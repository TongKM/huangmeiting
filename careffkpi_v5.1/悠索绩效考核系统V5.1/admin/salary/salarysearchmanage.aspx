﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="salarysearchmanage.aspx.cs" Inherits="BasicWeb.admin.salary.salarysearchmanage" %>


<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>员工工资查看</title>
    <asp:Literal ID="litregscript" runat="server" Text=""></asp:Literal>
    <link href="../style/default/style.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="/admin/js/jquery/jquery.js"></script>
    <%--<script language="javascript" type="text/javascript">
        var selectid = "#txt应发合计,#txt医保个人"
        var chkL = [];
        chkL[0] = ['txt应发合计', '应发合计', 17, '应发合计不能为空！', '应发合计不能为空！'];
        chkL[1] = ['txt医保个人', '排序', 17, '排序必须输入正整数！', '排序必须输入正整数！'];
    </script>--%>
    <%=sHTMLJS %>
    <script language="javascript" src="/admin/js/SystemAdminWebForm.js" type="text/jscript"></script>



    <script language="javascript" src="/admin/js/System.js" type="text/javascript"></script>

    <script language="javascript" src="/admin/My97DatePicker/WdatePicker.js" type="text/jscript"></script>
<script  language="javascript" src="/editor/xheditor.js" type="text/javascript"></script>
</head>
<body onload="win_onLoad();">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div id="divLoading">
                <img src="/admin/style/default/images/loading.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div id="tbMain">
        <div class="divTbg">
            <div class="divTbgL">
            </div>
            <div class="divTbgInfo">
                <div class="divTbgTitle">
                    员工工资查看</div>
            </div>
        </div>
        <div class="divTbgF">
        </div>
        <div class="divTNavBG" id="topURL">
            <ul id="topURLul">
                <li class="divURLed" id="div1">
                    <div>
                        <img src="../style/default/images/mini_icons_046.gif" width="10" height="10" />
                        基本信息</div>
                </li>
            </ul>
        </div>
        <div id="mypanel">
            <div class="divInfoContext">
                <div class="divInfo">
                    <div class="divInfoblack">
                        <div class="divInfoTopList">
                            <div class="listLeft">
                                <div class="navtitle">
                                    员工工资查看</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="divInfo">
                    <table width="100%" border="0" cellpadding="0" cellspacing="1" class="tablist" id="mainlist">
                    <%--<tr class="tablisttr">
                    <td class="tablisttitle" width="100px">&nbsp;</td><td class="tabmanagertr">&nbsp;</td>
                    <td class="tablisttitle" width="100px">&nbsp;</td><td class="tabmanagertr">&nbsp;</td>
                    </tr>--%>
                    <%=sHTML%>
                    </table>
                
                    <div class="divSave">
                        <asp:Button ID="btnSubmit" runat="server" Text="保存(S)" CssClass="SkyButtonFocus" Enabled="false" Visible="false"
                            OnClick="btnSubmit_Click" />
                        <asp:Button ID="btnExit" runat="server" Text="取消(C)" CssClass="SkyButtonFocus" OnClick="btnSubmit_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
