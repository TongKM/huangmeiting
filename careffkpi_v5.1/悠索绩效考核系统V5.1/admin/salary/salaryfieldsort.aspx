﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="salaryfieldsort.aspx.cs" Inherits="BasicWeb.admin.salary.salaryfieldsort" %>


<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>项目显示排序</title>
    <asp:Literal ID="litregscript" runat="server" Text=""></asp:Literal>
     <link href="../style/default/style.css" rel="stylesheet" type="text/css" />
     
    <script src="res/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="res/jquery-ui.js" type="text/javascript"></script>
    
    <script language="javascript" src="/admin/js/System.js" type="text/javascript"></script>
    <script language="javascript" src="/admin/My97DatePicker/WdatePicker.js" type="text/jscript"></script>
  

   
<style>
#sortable {
	list-style-type: none;
	margin: 0;
	padding: 0;
	width: 100%;
}
#sortable li {
	padding-left: 1.5em;
	font-size: 12px;
	height: 20px;
	border: 1px solid #CCCCCC;
	background-color: #FFFFFF;
	cursor: move;
	margin-top: 0;
	margin-right: 3px;
	margin-bottom: 3px;
	margin-left: 3px;
	padding-top: 0.4em;
	padding-right: 0.4em;
	padding-bottom: 0.4em;
	line-height: 20px;
	background-image: url(movebg.png);
	background-repeat: repeat-x;
	font-weight: bold;
	color: #000000;
}
#sortable li span {
	position: absolute;
	margin-left: -1.3em;
}
</style>
<script>  $(function() {    $( "#sortable" ).sortable();    $( "#sortable" ).disableSelection();  });  </script>
<script language="javascript" type="text/javascript">
       $(document).ready(function() {
             $("#btnSubmit").click(function() {
			 $("#txtShowID").val("");
               $("#sortable li span").each(function(index) {
        			var sid = ($(this).attr("id"))
					///alert(sid);
					var ss=$("#txtShowID").val()+sid+",";
					$("#txtShowID").val(ss);
			});
			
           });
       });
 
    </script>



</head>
<body onload="win_onLoad();">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div id="divLoading">
                <img src="/admin/style/default/images/loading.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div id="tbMain">
        <div class="divTbg">
            <div class="divTbgL">
            </div>
            <div class="divTbgInfo">
                <div class="divTbgTitle">
                    项目显示排序</div>
            </div>
        </div>
        <div class="divTbgF">
        </div>
        <div class="divTNavBG" id="topURL">
            <ul id="topURLul">
                <li class="divURLed" id="div1">
                    <div>
                        <img src="../style/default/images/mini_icons_046.gif" width="10" height="10" />
                        基本信息</div>
                </li>
            </ul>
        </div>
        <div id="mypanel">
            <div class="divInfoContext">
                <div class="divInfo">
                    <div class="divInfoblack">
                        <div class="divInfoTopList">
                            <div class="listLeft">
                                <div class="navtitle">
                                    项目显示排序</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="divInfo">
                    <table width="100%" border="0" cellpadding="0" cellspacing="1" class="tablist" id="mainlist">
                    <tr class="tablisttr"  style="display:none">
                    <td width="100" class="tablisttitle">工资表名</td>
<td class="tabmanagertr"><asp:DropDownList runat="server" ID="drpSTableName" ToolTip="工资表名">
                                                        <asp:ListItem Value="STableName" Text="codeno"></asp:ListItem>
                                                    </asp:DropDownList>
                                                    
                                                    
                                                    <asp:TextBox ID="txtShowID" runat="server"></asp:TextBox>
                                                    </td>

                    </tr>

                    <tr class="tablisttr">
                    <td width="100" class="tablisttitle">项目排序</td>
              <td class="tabmanagertr">
              <div id="divinfo" style="padding-top:10px; width:300px">
              <ul id="sortable">
                 <%=sHTML %>
                </ul>
                </div>

                        </td>
                    </tr>

                    </table>
      <div class="divSave">
                        <asp:Button ID="btnSubmit" runat="server" Text="保存(S)" CssClass="SkyButtonFocus"
                            OnClick="btnSubmit_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
