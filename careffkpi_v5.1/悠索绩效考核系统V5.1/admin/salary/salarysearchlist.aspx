﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="salarysearchlist.aspx.cs" Inherits="BasicWeb.admin.salary.salarysearchlist" %>


<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>员工工资查看</title>
    <link href="../style/default/style.css" rel="stylesheet" type="text/css" />

    <script language="javascript" src="/admin/js/System.js" type="text/javascript"></script>

</head>
<body>
    <form id="Form1" method="post" runat="server">
    <div class="divTbg">
        <div class="divTbgL">
        </div>
        <div class="divTbgInfo">
            <div class="divTbgTitle">
                员工工资查看</div>
        </div>
    </div>
    <div class="divTbgF">
    </div>
    <div class="divSearchBoth">
        <div class="divInfo">
            <div class="divInfoblack">
                <div class="divInfoTopList">
                    <div class="listLeft">
                        <div class="navsearchtitle">
                            员工工资搜索</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="divInfo">
            <div class="divSearch">
                <div id="searchBox" class="clearsearch">
                    <p style="display:none">
                        <label for="drpSTableName">
                            工资表名：</label>
                        <asp:DropDownList runat="server" ID="drpSTableName">
                            <asp:ListItem Value="STableName" Text="codeno"></asp:ListItem>
                        </asp:DropDownList>
                    </p>
                    <p>
                        <label for="txtYear">
                            查询年份：</label>
                        <asp:TextBox runat="server" ID="txtYear"></asp:TextBox>
                    </p>
                    <p>
                        <label for="txtMonth">
                            查询月份：</label>
                        <asp:TextBox runat="server" ID="txtMonth"></asp:TextBox>
                    </p>
                    <p>
                        <label for="txtUserID">
                            用户编号：</label>
                        <asp:TextBox runat="server" ID="txtUserID"></asp:TextBox>
                    </p>
                    <p>
                        <label for="txtUserName">
                            用户名称：</label>
                        <asp:TextBox runat="server" ID="txtUserName"></asp:TextBox>
                    </p>
                      
                
                    <p id="searchSubmit">
                        <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="确认搜索" CssClass="SkyButtonFocus" />
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="divInfo">
        <div class="divInfoblack">
            <div class="divInfoTopList">
                <div class="listLeft">
                    <div class="navtitle">
                        员工工资记录列表</div>
                </div>
                <div class="listRight">
                    <ul>
                        <li class="navstyle1">
                            <input type="checkbox" name="chkselect" id="chkselect" onclick="skySelectCheckList();" />
                            <label for="chkselect">
                                全选</label>
                        </li>
                        <li class="navstyle2">
                            <asp:LinkButton ID="LnkBAdd" runat="server" OnClick="SkyLnkBAMD_Click" Enabled="false">添加</asp:LinkButton>
                        </li>
                        <li class="navstyle3">
                            <asp:LinkButton ID="LnkBEdit" runat="server" OnClick="SkyLnkBAMD_Click"  Enabled="false">编辑</asp:LinkButton>
                        </li>
                        <li class="navstyle4">
                            <asp:LinkButton ID="LnkBDelete" runat="server" OnClick="SkyLnkBAMD_Click" Enabled="false">删除</asp:LinkButton>
                        </li>
                        <li class="navstyle5">
                            <asp:LinkButton ID="LnkBExcel" runat="server" OnClick="SkyLnkBAMD_Click">导出</asp:LinkButton>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="divInfo">
        <div class="divInfoContext">
            <div id="printInfo">
                 <asp:GridView ID="rptInfo" runat="server" AutoGenerateColumns="False" BorderStyle="None"
                        CellPadding="0" CellSpacing="1" GridLines="None" Width="100%" CssClass="tablist"
                        HorizontalAlign="Center">
                        <RowStyle CssClass="tablisttr" HorizontalAlign="Center" />
                        <HeaderStyle HorizontalAlign="Center" CssClass="tablisttitle"></HeaderStyle>
                        <Columns>
                            
                        </Columns>
                    </asp:GridView>
            </div>
        </div>
    </div>
    <div class="divInfo">
        <webdiyer:AspNetPager ID="Pager" runat="server" AlwaysShow="True" OnPageChanged="Pager_PageChanged"
                        ShowCustomInfoSection="Left" ShowPageIndexBox="Always" ShowPageIndex="False"
                        CustomInfoHTML="共 <B>%PageCount%</B> 页，当前为第 <B>%CurrentPageIndex%</B> 页，每页 <B>%PageSize%</B> 条"
                        SubmitButtonText="" FirstPageText="首页" LastPageText="尾页" LayoutType="Table" NextPageText="后一页"
                        PrevPageText="前一页" ShowMoreButtons="False" TextAfterPageIndexBox="页" TextBeforePageIndexBox="转到"
                        CustomInfoTextAlign="Left" HorizontalAlign="Right">
                    </webdiyer:AspNetPager>
    </div>
    <div class="divFootInfo">
    </div>
    </form>
</body>
</html>
