﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="salaryfieldlist.aspx.cs" Inherits="BasicWeb.admin.salary.salaryfieldlist" %>


<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>工资项目管理</title>
    
    
    <link href="../style/default/style.css" rel="stylesheet" type="text/css" />

    <script language="javascript" src="/admin/js/System.js" type="text/javascript"></script>
    <script language="javascript" src="/admin/My97DatePicker/WdatePicker.js" type="text/jscript"></script>

</head>
<body>
    <form id="Form1" method="post" runat="server">
    <div class="divTbg">
        <div class="divTbgL">
        </div>
        <div class="divTbgInfo">
            <div class="divTbgTitle">
                工资项目管理</div>
        </div>
    </div>
    <div class="divTbgF">
    </div>
    <div class="divSearchBoth">
        <div class="divInfo">
            <div class="divInfoblack">
                <div class="divInfoTopList">
                    <div class="listLeft">
                        <div class="navsearchtitle">
                            工资项目搜索</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="divInfo">
            <div class="divSearch">
                <div id="searchBox" class="clearsearch">
                   
                    

                    <p style="display:none">
                        <label for="drpSTableName">
                            工资表名：</label>
                        <asp:DropDownList runat="server" ID="drpSTableName">
                            <asp:ListItem Value="STableName" Text="code"></asp:ListItem>
                        </asp:DropDownList>
                    </p>
                    <p>
                        <label for="txtSFiledName">
                            项目名称：</label>
                        <asp:TextBox runat="server" ID="txtSFiledName"></asp:TextBox>
                    </p>
                     
                    <p>
                        <label for="drpIsSystem">
                            是否系统：</label>
                        <asp:DropDownList runat="server" ID="drpIsSystem">
                            <asp:ListItem Value="SysIsUsed" Text="code"></asp:ListItem>
                        </asp:DropDownList>
                    </p>
                    <p id="searchSubmit">
                        <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="确认搜索" CssClass="SkyButtonFocus" />
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="divInfo">
        <div class="divInfoblack">
            <div class="divInfoTopList">
                <div class="listLeft">
                    <div class="navtitle">
                        工资项目列表</div>
                </div>
                <div class="listRight">
                    <ul>
                        <li class="navstyle1">
                            <input type="checkbox" name="chkselect" id="chkselect" onclick="skySelectCheckList();" />
                            <label for="chkselect">
                                全选</label>
                        </li>
                        <li class="navstyle2">
                            <asp:LinkButton ID="LnkBAdd" runat="server" OnClick="SkyLnkBAMD_Click">添加</asp:LinkButton>
                        </li>
                        <li class="navstyle3">
                            <asp:LinkButton ID="LnkBEdit" runat="server" OnClick="SkyLnkBAMD_Click">编辑</asp:LinkButton>
                        </li>
                        <li class="navstyle4">
                            <asp:LinkButton ID="LnkBDelete" runat="server" OnClick="SkyLnkBAMD_Click" Enabled="false">删除</asp:LinkButton>
                        </li>
                        <li class="navstyle5">
                            <asp:LinkButton ID="LnkBExcel" runat="server" OnClick="SkyLnkBAMD_Click">导出</asp:LinkButton>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="divInfo">
        <div class="divInfoContext">
            <div id="printInfo">
                <asp:Repeater ID="rptInfo" runat="server">
                    <HeaderTemplate>
                        <table width="100%" border="0" cellpadding="0" cellspacing="1" class="tablist">
                            <tr class="tablisttitle">
                                <td>
                                    序号
                                </td>
                                <td>
                                    <%=cHTMLFx%>
                                </td>
                                <%--<td align="center">
                                    工资表名
                                </td>--%>
                                <td align="center">
                                    项目名称
                                </td>
                                
                                <td align="center">
                                    显示顺序
                                </td>
                                
                                
                                <td align="center">
                                    是否系统
                                </td>
                                
                                <td align="center">
                                    计算方式
                                </td>
                                <td align="center">
                                    列表显示
                                </td>
                                
                                <td align="center">
                                    建立时间
                                </td>
                                <td align="center">
                                    修改
                                </td>   
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr class="tablisttr">
                            <td>
                                <%#Eval("ID") %>
                            </td>
                            <td>
                                <asp:CheckBox runat="server" ID="chkselect" ToolTip='<%#Eval("ID") %>' Enabled='<%# Eval("IsSystem").ToString() == "0"?false:true%>'/>
                            </td>
                           <%-- <td align="center">
                                <%#Eval("STableNameName")%>
                            </td>--%>
                             
                            <td align="center">
                                <%#Eval("SFiledName")%>
                            </td>
                          
                            <td align="center">
                                <%#Eval("ShowID")%>


                            </td>
                            <td align="center">
                                <%#Eval("IsSystemName")%>
                            </td>
                            
                            <td align="center">
                                <%#Eval("SFileOprName")%>
                            </td>
                            <td align="center">
                                <%#Eval("SFileListName")%>
                            </td>
                            
                            <td align="center">
                                <%#UsToolsDateFormat(Eval("CreateDate"))%>
                            </td>
                            <td>
                                <%#cHTMLUrl(pDefault.sEditUrl + "?id=" + Eval("ID").ToString(), "[修改]")%>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>
    <div class="divInfo">
         <webdiyer:AspNetPager ID="Pager" runat="server" AlwaysShow="True" OnPageChanged="Pager_PageChanged"
                        ShowCustomInfoSection="Left" ShowPageIndexBox="Always" ShowPageIndex="False"
                        CustomInfoHTML="共 <B>%PageCount%</B> 页，当前为第 <B>%CurrentPageIndex%</B> 页，每页 <B>%PageSize%</B> 条"
                        SubmitButtonText="" FirstPageText="首页" LastPageText="尾页" LayoutType="Table" NextPageText="后一页"
                        PrevPageText="前一页" ShowMoreButtons="False" TextAfterPageIndexBox="页" TextBeforePageIndexBox="转到"
                        CustomInfoTextAlign="Left" HorizontalAlign="Right">
                    </webdiyer:AspNetPager>
    </div>
    
    <div class="divInfo">
        <span class="usHelpMessage"> * 注意事项： 
        <a class="rptlink" href="javascript:void(0);" onclick="UsSysOpen('salaryfieldsort.aspx?tid=SkySalaryTeacher');">字段的显示排序请点击这里。 </a></span>  
    </div>
    
    <div class="divFootInfo">
    </div>
    </form>
</body>
</html>
