﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="salaryfieldmanage.aspx.cs" Inherits="BasicWeb.admin.salary.salaryfieldmanage" %>


<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>工资项目管理</title>
    <asp:Literal ID="litregscript" runat="server" Text=""></asp:Literal>


    <link href="../style/default/style.css" rel="stylesheet" type="text/css" />

    <script language="javascript" src="/admin/js/System.js" type="text/javascript"></script>
    <script language="javascript" src="/admin/My97DatePicker/WdatePicker.js" type="text/jscript"></script>
    <script  language="javascript" src="/editor/xheditor.js" type="text/javascript"></script>


</head>
<body onload="win_onLoad();">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div id="divLoading">
                <img src="/admin/style/default/images/loading.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div id="tbMain">
        <div class="divTbg">
            <div class="divTbgL">
            </div>
            <div class="divTbgInfo">
                <div class="divTbgTitle">
                    工资项目管理</div>
            </div>
        </div>
        <div class="divTbgF">
        </div>
        <div class="divTNavBG" id="topURL">
            <ul id="topURLul">
                <li class="divURLed" id="div1">
                    <div>
                        <img src="../style/default/images/mini_icons_046.gif" width="10" height="10" />
                        基本信息</div>
                </li>
            </ul>
        </div>
        <div id="mypanel">
            <div class="divInfoContext">
                <div class="divInfo">
                    <div class="divInfoblack">
                        <div class="divInfoTopList">
                            <div class="listLeft">
                                <div class="navtitle">
                                    工资项目管理</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="divInfo">
                    <table width="100%" border="0" cellpadding="0" cellspacing="1" class="tablist" id="mainlist">
<tr class="tablisttr"  style="display:none">
<td class="tablisttitle">工资表名</td><td class="tabmanagertr"><asp:DropDownList runat="server" ID="drpSTableName" ToolTip="工资表名">
                                    <asp:ListItem Value="STableName" Text="codeno"></asp:ListItem>
                                </asp:DropDownList></td>
<td class="tablisttitle">是否系统</td><td class="tabmanagertr"><asp:DropDownList runat="server" ID="drpIsSystem" ToolTip="是否系统">
                                    <asp:ListItem Value="SysIsUsed" Text="codeno"></asp:ListItem>
                                </asp:DropDownList></td>
</tr>
<tr class="tablisttr">
<td class="tablisttitle">项目名称</td><td class="tabmanagertr"><asp:TextBox ID="txtSFiledName" runat="server" ToolTip="项目名称" Text="" CssClass="18"></asp:TextBox></td>
<td class="tablisttitle">项目类型</td><td class="tabmanagertr"><asp:DropDownList runat="server" ID="drpSFiledType" ToolTip="项目类型">
                                    <asp:ListItem Value="SFiledType" Text="codeno"></asp:ListItem>
                                </asp:DropDownList></td>
</tr>
<tr class="tablisttr">
<td class="tablisttitle">显示排序</td><td class="tabmanagertr"><asp:TextBox ID="txtShowID" runat="server" ToolTip="显示排序" Text="1"  CssClass="2"></asp:TextBox></td>
<td class="tablisttitle">计算方式</td><td class="tabmanagertr">
    <asp:DropDownList runat="server" ID="drpSFileOpr" ToolTip="计算方式">
                                    <asp:ListItem Value="SFileOpr" Text="codeno"></asp:ListItem>
                                </asp:DropDownList></td>
</tr>

<tr class="tablisttr">
<td class="tablisttitle">列表显示</td><td class="tabmanagertr">
    <asp:DropDownList runat="server" ID="drpSFileList" ToolTip="列表显示">
                                    <asp:ListItem Value="SysIsUsed" Text="codeno"></asp:ListItem>
                                </asp:DropDownList></td>
<td class="tablisttitle">&nbsp;</td><td class="tabmanagertr">
    &nbsp;</td>
</tr>

<tr class="tablisttr">
<td class="tablisttitle">项目说明</td>
<td class="tabmanagertr" colspan="3">
<asp:TextBox ID="txtSFiledInfo" runat="server" ToolTip="项目说明" Width="100%" TextMode="MultiLine" Height="100px" class="xheditor {upLinkUrl:s_upLinkUrl,upLinkExt:s_upLinkExt,upImgUrl:s_upLinkUrl,upImgExt:s_upImgExt,upFlashUrl:s_upLinkUrl,upFlashExt:s_upFlashExt,upMediaUrl:s_upLinkUrl,upMediaExt:s_upMediaExt}"></asp:TextBox>
</td>
</tr>

</table>
                    <div class="divSave">
                        <asp:Button ID="btnSubmit" runat="server" Text="保存(S)" CssClass="SkyButtonFocus"
                            OnClick="btnSubmit_Click" />
                        <asp:Button ID="btnDelete" runat="server" Text="删除(D)" CssClass="SkyButtonFocus" OnClick="btnSubmit_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
