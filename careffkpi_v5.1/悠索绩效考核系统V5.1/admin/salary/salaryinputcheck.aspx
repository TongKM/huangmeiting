﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="salaryinputcheck.aspx.cs" Inherits="BasicWeb.admin.salary.salaryinputcheck" %>


<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>工资导入确认</title>
   <link href="../style/default/style.css" rel="stylesheet" type="text/css" />


    
       <script language="javascript" src="/admin/js/System.js" type="text/javascript"></script>
   
    
    
    <style type="text/css">
        .SalaryError{ color:Red; background-color:#cccccc;}
    </style>

</head>
<body onload="win_onLoad();">
    <form id="Form1" method="post" runat="server">
        <div id="tbMain">
    <div class="divTbg">
        <div class="divTbgL">
        </div>
        <div class="divTbgInfo">
            <div class="divTbgTitle">
                工资导入确认</div>
        </div>
    </div>
    <div class="divTbgF">
    </div>
    <div class="divInfo">
        <div class="divInfoblack">
            <div class="divInfoTopList">
                <div class="listLeft">
                    <div class="navtitle">
                        工资导入数据</div>
                </div>
                <div class="listRight">
                </div>
            </div>
        </div>
    </div>
    <div class="divInfo">
        <div class="divInfoContext">
            <div id="printInfo" style="height:500px;overflow:auto;">
            
            
            <div style="font: 2px; line-height: 2px;">
                        &nbsp;</div>
                    <asp:GridView ID="rptInfo" runat="server" AutoGenerateColumns="False" BorderStyle="None"
                        CellPadding="0" CellSpacing="1" GridLines="None" Width="100%" CssClass="tablist"
                        HorizontalAlign="Center">
                        <RowStyle CssClass="tablisttr" HorizontalAlign="Center" />
                        <HeaderStyle HorizontalAlign="Center" CssClass="tablisttitle"></HeaderStyle>
                        <Columns>
                            
                        </Columns>
                    </asp:GridView>
            
            
            
                
            </div>
        </div>
    </div>
    <div class="divInfo">
        <div class="divSave">
                    <asp:Button ID="btnSubmit" runat="server" Text="保存(S)" CssClass="SkyButtonFocus"
                            OnClick="btnSubmit_Click" /></div>
               
    </div>
    <div class="divInfo">
        <span class="usHelpMessage"> * 注意事项： 有特殊标记的数据，代表数据不符合格式，您需要修改EXCEL文件重新上传后导入。如果个别数据值错误，您可以导入后通过手工修改单条工资记录。点击保存即可导入数据库。</span>  
    </div>
    <div class="divFootInfo">
    </div>
    </div>
    </form>
</body>
</html>
