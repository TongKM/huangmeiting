﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="salaryinput.aspx.cs" Inherits="BasicWeb.admin.salary.salaryinput" %>


<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>工资导入</title>
    <asp:Literal ID="litregscript" runat="server" Text=""></asp:Literal>
  <link href="../style/default/style.css" rel="stylesheet" type="text/css" />
 <script type="text/javascript" src="/admin/js/jquery/jquery.js"></script>

    <script language="javascript" src="/admin/js/System.js" type="text/javascript"></script>
    <script language="javascript" src="/admin/My97DatePicker/WdatePicker.js" type="text/jscript"></script>
    <script  language="javascript" src="/editor/xheditor.js" type="text/javascript"></script>
</head>
<body onload="win_onLoad();">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div id="divLoading">
                <img src="/admin/style/default/images/loading.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div id="tbMain">
        <div class="divTbg">
            <div class="divTbgL">
            </div>
            <div class="divTbgInfo">
                <div class="divTbgTitle">
                    工资导入</div>
            </div>
        </div>
        <div class="divTbgF">
        </div>
        <div class="divTNavBG" id="topURL">
            <ul id="topURLul">
                <li class="divURLed" id="div1">
                    <div>
                        <img src="../style/default/images/mini_icons_046.gif" width="10" height="10" />
                        基本信息</div>
                </li>
            </ul>
        </div>
        <div id="mypanel">
            <div class="divInfoContext">
                <div class="divInfo">
                    <div class="divInfoblack">
                        <div class="divInfoTopList">
                            <div class="listLeft">
                                <div class="navtitle">
                                    工资导入</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="divInfo">
                    <table width="100%" border="0" cellpadding="0" cellspacing="1" class="tablist" id="mainlist">
<tr class="tablisttr" style="display:none">
<td class="tablisttitle" width="100">工资表名</td><td class="tabmanagertr"><asp:DropDownList runat="server" ID="drpSTableName" ToolTip="工资表名">
                                    <asp:ListItem Value="STableName" Text="codeno"></asp:ListItem>
                                </asp:DropDownList></td>
</tr>
<tr class="tablisttr">
<td class="tablisttitle">选择文件</td><td class="tabmanagertr">
<asp:TextBox ID="txtFilePath" runat="server"   Width="80%"></asp:TextBox>
                                <a href="#" onclick="SkyOpen('txtFilePath');">[选择]</a>
                                <a href="#" onclick="SkyView('txtFilePath');">[查看]</a>                       
</td>
</tr>

<tr class="tablisttr">
<td class="tablisttitle">导入说明</td>
<td class="tabmanagertr">
<asp:TextBox ID="txtImpInfo" runat="server" ToolTip="导入说明" Width="100%" TextMode="MultiLine" Height="100px" class="xheditor {upLinkUrl:s_upLinkUrl,upLinkExt:s_upLinkExt,upImgUrl:s_upLinkUrl,upImgExt:s_upImgExt,upFlashUrl:s_upLinkUrl,upFlashExt:s_upFlashExt,upMediaUrl:s_upLinkUrl,upMediaExt:s_upMediaExt}"></asp:TextBox>
</td>
</tr>

<tr class="tablisttr">
<td class="tablisttitle">模板下载</td>
<td class="tabmanagertr">
                                
    <asp:LinkButton ID="lnkDownload" runat="server" onclick="lnkDownload_Click">[下载最新工资信息表模板]</asp:LinkButton>      
    <br /><span class="fontred">
    * 注意事项：<br />
1.如果工资项目有变动，请重新下载最新的的工资信息表模板.<br />
2.下载模板后，将数据输入或复制到数据表中，执行“文件-另存为-选择存放位置-选择保存类型（Microsoft Excel 工作簿）-保存”<br />
按以上操作方法即可生成标准格式工资信息表，如果操作不当可能会引起不必要的错误！敬请注意！ </span>                
</td>
</tr>

</table>
                    <div class="divSave">
                        <asp:Button ID="btnSubmit" runat="server" Text="保存(S)" CssClass="SkyButtonFocus"
                            OnClick="btnSubmit_Click" />
                        <asp:Button ID="btnExit" runat="server" Text="取消(C)" CssClass="SkyButtonFocus" OnClick="btnSubmit_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
