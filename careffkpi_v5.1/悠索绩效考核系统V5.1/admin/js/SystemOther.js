﻿//################################################
//代码功能：打开模式窗口
//编写时间：2013-03-12
//################################################
function UsOpenDialogTree(paraurl, src,txtValue) {
    var url = paraurl;
    var res;
    res = showModalDialog(url, 'careff2013', 'dialogWidth: 300px; dialogHeight: 500px; center: yes; resizable: no; scroll: no; status: no;');
    if (res == undefined) {
        window.alert("系统提示您：您没有选择相关需要的信息！");
    }
    else {
        try {
            //alert(res);
            var strs = res.split("#")
            var m = $id(src)
            //m.title = strs[0];
            m.value = strs[1];
            var m1 = $id(txtValue)
            m1.value = strs[0];
        }
        catch (e) {
            alert(e);
        }
    }
}


function UsOpenURLInfo(webURL) {
    //var webURL = "evaluationDetail.aspx?id=" + ID;
    var win = window.open(webURL, '2013', "height=500, width=750, top=0, left=0,toolbar=no, menubar=no, scrollbars=yes, resizable=yes, location=no, status=no")
    win.focus();
}


function UsView(src) {
    var m = $id(src)
    if (m.value != "") {
        window.open(m.value);
    }
    else {
        alert("暂无数据，不能查看！");
    }
}
function UsOpenDialog(txt) {
    //alert(txt);
    var webURL =  "/admin/document/documentuploadself.aspx"
    var picname = window.showModalDialog(webURL, window, "dialogWidth:550px;status:no;dialogHeight:150px");
    //alert(picname);
    if (picname != undefined) $id(txt).value = picname;
}

function UsClear(txt) {
    $id(txt).value = "";
}

function UsCopyRight() {
    var k = window.showModalDialog("/admin/ussoft.aspx", window, 'dialogWidth:500px;status:no;dialogHeight:300px');
}

//防止非法打开
function UsURLNoSrc() {
    try {
        var srcURL = window.opener.location;
        var doURL = document.domain.toString();

        var strObj = new String(srcURL);
        if (strObj.indexOf(doURL) < 0) {
            window.location.href = "/";
         }
    }
    catch (err) {
        //在此处理错误
        //window.close();
        //alert(err);
        window.location.href = "/";
    }


}

function skySelectCheckList() {

    var slist;
    slist = $id("chkselect");
    var t;
    var nName;
    t = slist.checked;
    var dom = document.getElementsByTagName("*");
    var el = event.srcElement ? event.srcElement : event.target;
    for (i = 0; i < dom.length; i++) {
        if (dom[i].tagName == "INPUT" && dom[i].type.toLowerCase() == "checkbox") {
            nName = dom[i].id;
            dom[i].checked = t;
            if (nName.indexOf("chkselect") != -1) {
                if (dom[i].disabled == "")
                    dom[i].checked = t;
            }
            if (nName.indexOf("chkIsMakeup") != -1) {
                if (dom[i].disabled == "")
                    dom[i].checked = t;
            }

        }
    }
}