﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="userdetails.aspx.cs" Inherits="BasicWeb.admin.analysis.userdetails" %>



<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>

<%@ Register src="userdetailinfo.ascx" tagname="userdetailinfo" tagprefix="uc1" %>
<%@ Register src="userdetailnav.ascx" tagname="userdetailnav" tagprefix="uc3" %>

<%@ Register src="../gdi/gdichart.ascx" tagname="gdichart" tagprefix="uc2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>指标小类-[<%=sUserName%>]人员评价历史</title>
    <link href="../style/default/style.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery/jquery.js" type="text/javascript"></script>
    <script language="javascript" src="/admin/js/System.js" type="text/javascript"></script>
    <script language="javascript" src="/admin/My97DatePicker/WdatePicker.js" type="text/jscript"></script>
    <script  language="javascript" src="/editor/xheditor.js" type="text/javascript" ></script>
</head>
<body onload="win_onLoad();">
    <form id="Form1" method="post" runat="server"><div id="tbMain">
    <div class="divTbg">
      <div class="divTbgL"></div>
      <div class="divTbgInfo">
        <div class="divTbgTitle">人员评价</div>
      </div>
    </div>
    <div class="divTbgF"></div>
    <div class="divSearchBoth">
        <div class="divInfo">
            <div class="divInfoblack">
                <div class="divInfoTopList">
                    <div class="listLeft">
                        <div class="navsearchtitle">
                            人员评价详细</div>
                    </div>
                    
                    <uc3:userdetailnav ID="userdetailnav1" runat="server" />
                </div>
            </div>
        </div>
        <div class="divInfo">
            <div class="divSearch1">
                <div id="searchBox1" class="clearsearch1">
                
                <uc1:userdetailinfo ID="userdetailinfo1" runat="server" />
                <table width="100%" border="0" cellpadding="0" cellspacing="1" class="tablist" onMouseOver="changeto()"  onmouseout="changeback()">
    <tr class="tablisttr">
      <td align="center"  width="100" class="tablisttitle"> 时间段名称</td>
      <td align="center" width="40%" class="tablisttr"><%=sDateName%></td>
      <td align="center"  width="100" class="tablisttitle">评估时间</td>
      <td align="center" class="tablisttr"><%=UsToolsDateFormat(eDep.dCreateDate)%></td>
    </tr>
    <tr class="tablisttr">
      <td align="center" class="tablisttitle"> 文件上传</td>
      <td align="center" class="tablisttr">暂未上传</td>
      <td align="center" class="tablisttitle">评价总分</td>
      <td align="center" class="tablisttr">  <%=siEValue.ToString() %></td>
    </tr>
    <tr class="tablisttr">
      <td align="center" class="tablisttitle"> 评价留言</td>
      <td colspan="3" align="center">
          <asp:TextBox ID="txtInfo" runat="server" TextMode="MultiLine" Width="100%"  Height="80px"></asp:TextBox>
        </td>
    </tr>
   
  </table>
                   
              </div>
            </div>
        </div>
    </div>
    <div class="divInfo">
        <div class="divInfoblack">
            <div class="divInfoTopList">
                <div class="listLeft">
                    <div class="navtitle">
                        指标详细信息展示</div>
                </div>
                <div class="listRight">
                    <ul>
                         <li class="navstyle1">
                            <input type="checkbox" name="chkselect" id="chkselect" onclick="skySelectCheckList();"/>
                            <label for="chkselect">全选</label> </li>
                        
                        <li class="navstylelist">
                            <asp:LinkButton ID="LnkBList" runat="server" OnClick="SkyLnkBAMD_Click">列表</asp:LinkButton>
                        </li>
                        <li class="navstylereport">
                            <asp:LinkButton ID="LnkBReport" runat="server" OnClick="SkyLnkBAMD_Click">图表</asp:LinkButton>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="divInfo">
        <div class="divInfoContext">
            <asp:Repeater runat="server" ID="rptInfo">
                <HeaderTemplate>
                    <table width="100%" border="0" cellpadding="0" cellspacing="1" class="tablist" onmouseover="changeto()"  onmouseout="changeback()">
                        <tr class="tablisttitle">
                           <td width="4%">
                                序号
                            </td>
                            
                            <td>
                                指标大类
                            </td>
                            <td>
                                指标小类 
                            </td>
                            <td>
                                指标分数
                            </td>
                           
                            <td>
                                子项目个数
                            </td>
                            
                            
                           
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr class="tablisttr">
                        <td>
                           
                            <asp:Literal runat="server" ID="LiteralID" Text=' <%#Eval("E1") %>'></asp:Literal>
                      
                        </td>
                       
                         <td>
                            <%#Eval("E1Name")%>
                        </td>
                        <td>
                            <%#Eval("E2Name")%>
                        </td>
                        
                        <td>
                            <%#Eval("EValueSum")%>
                        </td>
                       
                        <td>
                            <%#Eval("ACount")%>
                        </td>
                        
                       
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                 <tr class="tablisttitle">
                           <td width="4%">
                                
                            </td>
                          
                           <td>
                                
                            </td>
                            
                            
                             <td>
                                
                            </td>
                       
                            <td>
                                <% =siEPower.ToString()%>
                            </td>
                                
                            <td>
                                <%=siEValue.ToString() %>
                            </td>
                            
                           
                        </tr>
                    </table></FooterTemplate>
            </asp:Repeater>
        </div>
        <div class="divInfoContext">
            <uc2:gdichart ID="gdichart1" runat="server" Visible="false"/>
        </div>
    </div>
    <div class="divInfo" style="display:none;">
        <webdiyer:aspnetpager id="Pager" runat="server" AlwaysShow="True" OnPageChanged="Pager_PageChanged"
         ShowCustomInfoSection="Left" ShowPageIndexBox="Always" showpageindex="False"
               CustomInfoHTML="共 <B>%PageCount%</B> 页，当前为第 <B>%CurrentPageIndex%</B> 页，每页 <B>%PageSize%</B> 条" 
               SubmitButtonText="" FirstPageText="首页" LastPageText="尾页" 
               LayoutType="Table" NextPageText="后一页" PrevPageText="前一页" ShowMoreButtons="False" 
               TextAfterPageIndexBox="页" TextBeforePageIndexBox="转到" 
               CustomInfoTextAlign="Left" HorizontalAlign="Right"></webdiyer:aspnetpager>
    </div>
    </div>
    </form>
</body>
</html>
