﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="departmentsuminfo.ascx.cs" Inherits="BasicWeb.admin.analysis.departmentsuminfo" %>


<div class="analysis_table_department">
  <table width="100%" border="0" cellpadding="0" cellspacing="1" class="tablist">
    <tr>
      <td colspan="2" align="center" class="tabmanagertr" style="height:192px"><table border="0" cellpadding="0" cellspacing="0" class="showuserbg">
        <tr>
          <td align="center"><img src='<%=entity.sUserPic%>' class='showuser' onerror="javascript:this.src='/upload/user/no.jpg'"/></td>
        </tr>
      </table></td>
    </tr>
    <tr class="tablisttr">
      
      <td class="tablisttitlenowidthtitle" colspan="2" align="center"><%=entity.sDeptName%></td>
    </tr>
    
    <tr class="tablisttr">
      <td align="center" class="tablisttitlenowidth">部门编号</td>
      <td class="tabmanagertr"><%=entity.sDeptID%></td>
    </tr>
    
    <tr class="tablisttr">
      <td align="center" class="tablisttitlenowidth">部门类型</td>
      <td class="tabmanagertr"><%=entity.sDeptType%></td>
    </tr>
    <tr class="tablisttr">
      <td align="center" class="tablisttitlenowidth">部门负责人</td>
      <td class="tabmanagertr"><%=entity.sDUser%></td>
    </tr>
    <tr class="tablisttr">
      <td align="center" class="tablisttitlenowidth">部门电话</td>
      <td class="tabmanagertr"><%=entity.sDTel%></td>
    </tr>
    <tr class="tablisttr">
      <td align="center" class="tablisttitlenowidth">部门状态</td>
      <td class="tabmanagertr"><%=entity.sDState%></td>
    </tr>
  </table></div>

