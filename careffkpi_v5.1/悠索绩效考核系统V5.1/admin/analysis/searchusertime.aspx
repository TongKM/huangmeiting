﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="searchusertime.aspx.cs" Inherits="BasicWeb.admin.analysis.searchusertime" %>
<%@ Register src="../gdi/gdichart.ascx" tagname="gdichart" tagprefix="uc2" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>评估次数查询</title>
    <link href="../style/default/style.css" rel="stylesheet" type="text/css" />

    <script language="javascript" src="/admin/js/System.js" type="text/javascript"></script>
    <script language="javascript" src="/admin/My97DatePicker/WdatePicker.js" type="text/jscript"></script>
</head>
<body>
    <form id="Form1" method="post" runat="server">
    <div class="divTbg">
      <div class="divTbgL"></div>
      <div class="divTbgInfo">
        <div class="divTbgTitle">评估次数查询</div>
      </div>
    </div>
    <div class="divTbgF"></div>
    <div class="divSearchBoth">
        <div class="divInfo">
            <div class="divInfoblack">
                <div class="divInfoTopList">
                    <div class="listLeft">
                        <div class="navsearchtitle">
                           人员搜索</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="divInfo">
            <div class="divSearch">
                <div id="searchBox" class="clearsearch">
                     <p>
                        <label for="drpDateID">
                            时间段：</label>
                        <asp:DropDownList runat="server" ID="drpDateID" ToolTip="时间段"></asp:DropDownList>
                    </p>
                    
                    
                     <p>
                        <label for="txtDeptName">
                            部门名称：</label>
                        <asp:TextBox ID="txtDeptName" runat="server" ToolTip="人员名称" Text=""></asp:TextBox>
                    </p>
                    
                   <p>
                        <label for="txtUserID">
                            人员编号：</label>
                        <asp:TextBox ID="txtUserID" runat="server" ToolTip="人员编号" Text=""></asp:TextBox>
                    </p>
                     <p>
                        <label for="txtUserName">
                            人员名称：</label>
                        <asp:TextBox ID="txtUserName" runat="server" ToolTip="人员名称" Text=""></asp:TextBox>
                    </p>
                    
                     <p>
                        <label for="drpPostID">
                            岗位类型：</label>
                        <asp:DropDownList runat="server" ID="drpPostID" ToolTip="隶属岗位"></asp:DropDownList>
                    </p>
                  
                    <p>
                    
                        <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="确认搜索" CssClass="SkyButtonFocus" />
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="divInfo">
        <div class="divInfoblack">
            <div class="divInfoTopList">
                <div class="listLeft">
                    <div class="navtitle">
                        评估次数详细列表</div>
                </div>
                <div class="listRight">
                    <ul>
                         <li class="navstylelist">
                            <asp:LinkButton ID="LnkBList" runat="server" OnClick="SkyLnkBAMD_Click">列表</asp:LinkButton>
                        </li>
                        <li class="navstylereport">
                            <asp:LinkButton ID="LnkBReport" runat="server" OnClick="SkyLnkBAMD_Click">图表</asp:LinkButton>
                        </li>
                        <li class="navstyle5">
                            <asp:LinkButton ID="LnkBExcel" runat="server" OnClick="SkyLnkBAMD_Click">导出</asp:LinkButton>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="divInfo">
        <div class="divInfoContext">
            <asp:Repeater runat="server" ID="rptInfo">
                <HeaderTemplate>
                    <table width="100%" border="0" cellpadding="0" cellspacing="1" class="tablist" onmouseover="changeto()"  onmouseout="changeback()">
                        <tr class="tablisttitle">
                           <td width="4%">
                                序号
                            </td>
                           
                            <td>
                                用户编号
                            </td>
                            <td>
                                用户名称
                            </td>
                            <td>
                                部门名称
                            </td>
                            
                            <td>
                                 部门总评价次
                            </td>
                           
                            <td>
                                 本段部门评价次
                            </td>

                            <td>
                                互评总评价次
                            </td>
                            
                            <td>
                                本段互评次数
                            </td>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr class="tablisttr">
                        <td>
                            <%#Eval("ID") %>
                        </td>
                       
                        <td>
                           <%#Eval("UserID")%>
                        </td>
                        <td>
                            <%#Eval("UserName")%>
                        </td>
                        <td>
                            <%#Eval("DeptName")%>
                        </td>
                        <td>
                            <span class="fColorBlue">
                           <%#Eval("DeptCount")%></span>
                        </td>
                        <td>
                        <span class="fColorBlue">
                            <%#Eval("DeptCountDate")%></span>
                        </td>
                     
                        <td>
                        <span class="fColorRed">
                           <%#Eval("UserCount")%></span>
                        </td>
                        
                        <td><span class="fColorRed">
                           <%#Eval("UserCountDate")%></span>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table></FooterTemplate>
            </asp:Repeater>
        </div>
         <div class="divInfoContext">
            <uc2:gdichart ID="gdichart1" runat="server" Visible="false"/>
        </div>
    </div>
    <div class="divInfo">
        <webdiyer:aspnetpager id="Pager" runat="server" AlwaysShow="True" OnPageChanged="Pager_PageChanged"
         ShowCustomInfoSection="Left" ShowPageIndexBox="Always" showpageindex="False"
               CustomInfoHTML="共 <B>%PageCount%</B> 页，当前为第 <B>%CurrentPageIndex%</B> 页，每页 <B>%PageSize%</B> 条" 
               SubmitButtonText="" FirstPageText="首页" LastPageText="尾页" 
               LayoutType="Table" NextPageText="后一页" PrevPageText="前一页" ShowMoreButtons="False" 
               TextAfterPageIndexBox="页" TextBeforePageIndexBox="转到" 
               CustomInfoTextAlign="Left" HorizontalAlign="Right"></webdiyer:aspnetpager>
    </div>
    </form>
</body>
</html>
