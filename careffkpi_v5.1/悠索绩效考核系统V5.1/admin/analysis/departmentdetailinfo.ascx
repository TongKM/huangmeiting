﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="departmentdetailinfo.ascx.cs" Inherits="BasicWeb.admin.analysis.departmentdetailinfo" %>
<link href="../style/default/style.css" rel="stylesheet" type="text/css" />
<table width="100%" border="0" cellpadding="0" cellspacing="1" class="tablist" onMouseOver="changeto()"  onmouseout="changeback()">
  <tr class="tablisttr">
    <td align="center"  width="100" class="tablisttitle"> 部门编号</td>
    <td align="center" width="40%" class="tablisttr"><%=entity.sDeptID%></td>
    <td align="center"  width="100" class="tablisttitle">部门名称</td>
    <td align="center" class="tablisttr"><%=entity.sDeptName%></td>
  </tr>
  <tr class="tablisttr">
    <td align="center" class="tablisttitle">部门负责人</td>
    <td align="center" class="tablisttr"><%=entity.sDUser%></td>
    <td align="center" class="tablisttitle">部门电话</td>
    <td align="center" class="tablisttr"><%=entity.sDTel%></td>
  </tr>
</table>
