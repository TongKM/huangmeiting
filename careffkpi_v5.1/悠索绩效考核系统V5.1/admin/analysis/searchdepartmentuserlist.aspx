﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="searchdepartmentuserlist.aspx.cs" Inherits="BasicWeb.admin.analysis.searchdepartmentuserlist" %>

<%@ Register src="../gdi/gdichart.ascx" tagname="gdichart" tagprefix="uc2" %>
<%@ Register src="departmentsuminfo.ascx" tagname="departmentsuminfo" tagprefix="uc1" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>[<% =sDeptName.ToString() %>]评价成绩</title>
    <link href="../style/default/style.css" rel="stylesheet" type="text/css" />

    <script language="javascript" src="/admin/js/System.js" type="text/javascript"></script>
    <script language="javascript" src="/admin/My97DatePicker/WdatePicker.js" type="text/jscript"></script>
</head>
<body onload="win_onLoadSize(1000);">
    <form id="Form1" method="post" runat="server"><div id="tbMain">
    <div class="divTbg">
      <div class="divTbgL"></div>
      <div class="divTbgInfo">
        <div class="divTbgTitle">[<% =sDeptName.ToString() %>]评价成绩</div>
      </div>
    </div>
    <div class="divTbgF"></div>
    
    <div class="divInfo">
        <div class="divInfoblack">
            <div class="divInfoTopList">
                <div class="listLeft">
                    <div class="navtitle">
                        <%=sPageListTitle%></div>
                </div>
                <div class="listRight">
                    <ul>
                        
                        <li class="navstyle5">
                            <asp:LinkButton ID="LnkBExcel" runat="server" OnClick="SkyLnkBAMD_Click" >导出</asp:LinkButton>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="divInfo">
        <div class="divInfoContext">
            <asp:Repeater runat="server" ID="rptInfo">
                <HeaderTemplate>
                    <table width="100%" border="0" cellpadding="0" cellspacing="1" class="tablist" onmouseover="changeto()"  onmouseout="changeback()">
                        <tr class="tablisttitle">
                           <td width="4%">
                                序号
                            </td>
                            <td width="4%">
                                <%=cHTMLFx%>
                            </td>
                            <td>
                                时间段名称
                            </td>
                            <td>
                                被评价部门
                            </td>
                             <td>
                                套包名称
                            </td>
                            <td>
                                单项分
                            </td>
                            <td>
                                合计分
                            </td>
                            <td>
                                指标数
                            </td>
                             <td>
                                评价人
                            </td>

                            <td>
                                建立时间
                            </td>
                            
                            <td>
                                操作
                            </td>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr class="tablisttr">
                        <td>
                            <%#Eval("ID") %>
                        </td>
                        <td>
                            <asp:CheckBox  runat="server" ID="chkselect" ToolTip='<%#Eval("ID") %>'/>
                        </td>
                        <td>
                            <%#Eval("DateName")%>
                        </td>
                        <td>
                            <%#Eval("DeptIDName")%>
                        </td>
                        <td>
                            <%#Eval("PackName")%>
                        </td>
                        <td>
                           <%#Eval("EValueSplit")%>
                        </td>
                        <td><span class="fColorRed">
                           <%#Eval("EValue")%></span>
                        </td>
                         <td>
                           <%#Eval("ACount")%>
                        </td>
                         <td>
                           <%#Eval("UserName")%>
                        </td>
                        <td>
                           <%#UsToolsDateFormat(Eval("CreateDate"))%>
                        </td>
                        
                        <td>
                            <%#cHTMLUrlRand("../analysis/departmentdetaild.aspx?id=" + Eval("ID").ToString(), "查看")%>
                            
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                
                <tr class="tablisttitle">
                           <td width="4%">
                                
                            </td>
                            <td width="4%">
                               
                            </td>
                            <td>
                                
                            </td>
                            <td>
                                
                            </td>
                             <td>
                                
                            </td>
                            <td>
                                
                            </td>
                            <td>
                                 <%=Single1.ToString("F2")%>
                            </td>
                            <td>
                                 <%=Single2.ToString("")%>
                            </td>
                             <td>
                                
                            </td>

                            <td>
                                
                            </td>
                            
                            <td>
                                
                            </td>
                        </tr>
                
                
                    </table></FooterTemplate>
            </asp:Repeater>
        </div>
         <div class="divInfoContext">
         
         
             <table width="100%" border="0" cellspacing="0" cellpadding="0" class="analysis_table_chart">
              <tr>
                <td valign="top">
                    <uc1:departmentsuminfo ID="departmentsuminfo1" runat="server" />
                </td>
                <td class="analysis_table_chart_gdi"><uc2:gdichart ID="gdichart1" runat="server"/></td>
              </tr>
            </table>
         
         
           
        </div>
    </div>
    <div class="divInfo">
        <webdiyer:aspnetpager id="Pager" runat="server" AlwaysShow="True" OnPageChanged="Pager_PageChanged"
         ShowCustomInfoSection="Left" ShowPageIndexBox="Always" showpageindex="False"
               CustomInfoHTML="共 <B>%PageCount%</B> 页，当前为第 <B>%CurrentPageIndex%</B> 页，每页 <B>%PageSize%</B> 条" 
               SubmitButtonText="" FirstPageText="首页" LastPageText="尾页" 
               LayoutType="Table" NextPageText="后一页" PrevPageText="前一页" ShowMoreButtons="False" 
               TextAfterPageIndexBox="页" TextBeforePageIndexBox="转到" 
               CustomInfoTextAlign="Left" HorizontalAlign="Right"></webdiyer:aspnetpager>
    </div>
    </div>
    </form>
</body>
</html>
