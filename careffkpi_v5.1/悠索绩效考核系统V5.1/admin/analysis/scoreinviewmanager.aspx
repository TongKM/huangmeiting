﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="scoreinviewmanager.aspx.cs" Inherits="BasicWeb.admin.analysis.scoreinviewmanager" %>


<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>评估成绩入库</title>
    <asp:Literal ID="litregscript" runat="server" Text=""></asp:Literal>
    <link href="../style/default/style.css" rel="stylesheet" type="text/css" />

    <script language="javascript" src="/admin/js/System.js" type="text/javascript"></script>

    <script language="javascript" src="/admin/My97DatePicker/WdatePicker.js" type="text/jscript"></script>

    <script language="javascript" src="/editor/xheditor.js" type="text/javascript"></script>

</head>
<body onload="win_onLoad();">
    <form id="form1" runat="server">  
    <div id="tbMain">
        <div class="divTbg">
            <div class="divTbgL">
            </div>
            <div class="divTbgInfo">
                <div class="divTbgTitle">
                    评估成绩入库管理</div>
            </div>
        </div>
        <div class="divTbgF">
        </div>
        <div class="divTNavBG" id="topURL">
            <ul id="topURLul">
                <li class="divURLed" id="div1">
                    <div>
                        <img src="../style/default/images/mini_icons_046.gif" width="10" height="10" />
                        基本信息</div>
                </li>
            </ul>
        </div>
        <div id="mypanel">
            <div class="divInfoContext">
                <div class="divInfo">
                    <div class="divInfoblack">
                        <div class="divInfoTopList">
                            <div class="listLeft">
                                <div class="navtitle">
                                    评估成绩入库</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="divInfo">
                    <table width="100%" border="0" cellpadding="0" cellspacing="1" class="tablist" id="mainlist">
                        <tr class="tablisttr">
                            <td class="tablisttitle">
                                入库类型
                            </td>
                            <td class="tabmanagertr">
                                <asp:DropDownList runat="server" ID="drpLType" ToolTip="入库类型">
                                    <asp:ListItem Value="PAllType" Text="codeno"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td class="tablisttitle">
                                时间段
                            </td>
                            <td class="tabmanagertr">
                                <asp:DropDownList runat="server" ID="drpDateID" ToolTip="时间段">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="tablisttr">
                            <td class="tablisttitle">
                                部门类型
                            </td>
                            <td class="tabmanagertr">
                                <asp:DropDownList runat="server" ID="drpDeptType" ToolTip="部门类型">
                                    <asp:ListItem Value="DeptType" Text="code"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td class="tablisttitle">
                                岗位类型
                            </td>
                            <td class="tabmanagertr">
                                <asp:DropDownList runat="server" ID="drpPostID" ToolTip="隶属岗位">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="tablisttr">
                            <td class="tablisttitle">
                                成绩预览<br/>
                                <asp:LinkButton ID="lnkbExcelOK" runat="server" onclick="lnkbExcel_Click">导出Excel</asp:LinkButton>
                            </td>
                            <td class="tabmanagertr" colspan="3">
                                <div class="divInfoContext" style="height:300px; overflow:auto;">
                                        <asp:GridView ID="grvInfo" runat="server" BorderStyle="None" CellPadding="0" CellSpacing="1"
                                            GridLines="None" Width="100%" CssClass="tablist" HorizontalAlign="Center" AutoGenerateColumns="True">
                                            <RowStyle CssClass="tablisttr" HorizontalAlign="Center" />
                                            <HeaderStyle CssClass="tablisttitle" HorizontalAlign="Center"></HeaderStyle>
                                            <%--<Columns>
                                <asp:BoundField DataField="DeptID" HeaderText="部门编号" />
                                <asp:BoundField DataField="DeptName" HeaderText="部门名称" />
                            </Columns>--%>
                           
                                        </asp:GridView>
                                        <asp:Literal runat="server" ID="litText"></asp:Literal>
                                    </div>
                            </td>
                        </tr>
                        <tr class="tablisttr">
                            <td class="tablisttitle">
                                入库标题
                            </td>
                            <td class="tabmanagertr">
                                <asp:TextBox ID="txtLTitle" runat="server" ToolTip="入库标题" Text="" CssClass="1"></asp:TextBox>
                            </td>
                            <td class="tablisttitle">
                                记录数量
                            </td>
                            <td class="tabmanagertr">
                                <asp:TextBox ID="txtLNumber" runat="server" ToolTip="记录数量" Text="" CssClass="2"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="tablisttr">
                            <td class="tablisttitle">
                                入库说明
                            </td>
                            <td class="tabmanagertr" colspan="3">
                                <asp:TextBox ID="txtLInfo" runat="server" ToolTip="入库说明" Text="" Width="100%" TextMode="MultiLine"
                                    Height="100px" class="xheditor {upLinkUrl:s_upLinkUrl,upLinkExt:s_upLinkExt,upImgUrl:s_upLinkUrl,upImgExt:s_upImgExt,upFlashUrl:s_upLinkUrl,upFlashExt:s_upFlashExt,upMediaUrl:s_upLinkUrl,upMediaExt:s_upMediaExt}"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                    <div class="divSave">
                        <asp:Button ID="btnSubmit" runat="server" Text="保存(S)" CssClass="SkyButtonFocus" Enabled="false"
                            OnClick="btnSubmit_Click" />
                        <asp:Button ID="btnExit" runat="server" Text="取消(C)" CssClass="SkyButtonFocus" OnClick="btnSubmit_Click" />
                    </div>
                </div>
        </div>
    </div>
    </div>
    </form>
</body>
</html>
