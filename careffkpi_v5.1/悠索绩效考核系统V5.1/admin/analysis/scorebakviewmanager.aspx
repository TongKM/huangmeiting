﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="scorebakviewmanager.aspx.cs" Inherits="BasicWeb.admin.analysis.scorebakviewmanager" %>


<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>评估成绩</title>
    <asp:Literal ID="litregscript" runat="server" Text=""></asp:Literal>
    <link href="../style/default/style.css" rel="stylesheet" type="text/css" />

    <script language="javascript" src="/admin/js/System.js" type="text/javascript"></script>
 <script language="javascript" src="/admin/My97DatePicker/WdatePicker.js" type="text/jscript"></script>
 <script  language="javascript" src="/editor/xheditor.js" type="text/javascript" ></script>

</head>
<body onload="win_onLoad();">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div id="divLoading">
                <img src="/admin/style/default/images/loading.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div id="tbMain">
        <div class="divTbg">
            <div class="divTbgL">
            </div>
            <div class="divTbgInfo">
                <div class="divTbgTitle">
                    评估成绩管理</div>
            </div>
        </div>
        <div class="divTbgF">
        </div>
        <div class="divTNavBG" id="topURL">
            <ul id="topURLul">
                <li class="divURLed" id="div1">
                    <div>
                        <img src="../style/default/images/mini_icons_046.gif" width="10" height="10" />
                        基本信息</div>
                </li>
            </ul>
        </div>
        <div id="mypanel">
            <div class="divInfoContext">
                <div class="divInfo">
                    <div class="divInfoblack">
                        <div class="divInfoTopList">
                            <div class="listLeft">
                                <div class="navtitle">
                                    评估成绩</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="divInfo">
                    <table width="100%" border="0" cellpadding="0" cellspacing="1" class="tablist" id="mainlist">
<tr class="tablisttr">

<td class="tablisttitle">入库类型</td>
<td class="tabmanagertr"><asp:DropDownList runat="server" ID="drpLType" ToolTip="入库类型"  Enabled="false">
                                    <asp:ListItem Value="PAllType" Text="code"></asp:ListItem>
                                </asp:DropDownList></td>
<td class="tablisttitle">时间段编号</td>
<td class="tabmanagertr"><asp:DropDownList runat="server" ID="drpDateID" ToolTip="时间段" Enabled="false"></asp:DropDownList></td>
</tr>

<tr class="tablisttr" <%=sUserStyle %>>
<td class="tablisttitle">用户编号</td><td class="tabmanagertr"><asp:TextBox ID="txtUserID" runat="server" ToolTip="用户编号" Text=""  Enabled="false"></asp:TextBox></td>
<td class="tablisttitle">用户名称</td><td class="tabmanagertr"><asp:TextBox ID="txtUserName" runat="server" ToolTip="用户名称" Text=""></asp:TextBox></td>

</tr>
<tr class="tablisttr">
<td class="tablisttitle">部门编号</td><td class="tabmanagertr"><asp:TextBox ID="txtDeptID" runat="server" ToolTip="部门编号" Text=""  Enabled="false"></asp:TextBox></td>
<td class="tablisttitle">部门名称</td><td class="tabmanagertr"><asp:TextBox ID="txtDeptName" runat="server" ToolTip="部门名称" Text=""  ></asp:TextBox></td>

</tr>

<tr class="tablisttr">
<td class="tablisttitle">评价人员数量</td><td class="tabmanagertr"><asp:TextBox ID="txtEUserCount" runat="server" ToolTip="评价人员数量" Text="" CssClass="2"></asp:TextBox></td>
<td class="tablisttitle">评价指标数</td><td class="tabmanagertr"><asp:TextBox ID="txtECount" runat="server" ToolTip="评价指标数" Text=""  CssClass="2"></asp:TextBox></td>
</tr>
<tr class="tablisttr">
<td class="tablisttitle">评价总分</td><td class="tabmanagertr"><asp:TextBox ID="txtEValueALL" runat="server" ToolTip="评价总分" Text="" CssClass="17"></asp:TextBox></td>
<td class="tablisttitle">评价最终分</td><td class="tabmanagertr"><asp:TextBox ID="txtEValue" runat="server" ToolTip="评价最终分" Text="" CssClass="17"></asp:TextBox></td>
</tr>
</table>
                    <div class="divSave">
                        <asp:Button ID="btnSubmit" runat="server" Text="保存(S)" 
                            CssClass="SkyButtonFocus" onclick="btnSubmit_Click" />
                        <asp:Button ID="btnExit" runat="server" Text="取消(C)" CssClass="SkyButtonFocus" 
                            onclick="btnSubmit_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </form>
</body>
</html>