﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="usersuminfo.ascx.cs" Inherits="BasicWeb.admin.analysis.usersuminfo" %>

<div class="analysis_table_department">
  <table width="100%" border="0" cellpadding="0" cellspacing="1" class="tablist">
    <tr>
      <td colspan="2" align="center" class="tabmanagertr" style="height:192px"><table border="0" cellpadding="0" cellspacing="0" class="showuserbg">
        <tr>
          <td align="center"><img src='<%=entity.sUserPic%>' class='showuser' onerror="javascript:this.src='/upload/user/no.jpg'"/></td>
        </tr>
      </table></td>
    </tr>
    <tr class="tablisttr">
      
      <td class="tablisttitlenowidthtitle" colspan="2" align="center"><%=entity.sUserName%></td>
    </tr>
    
    <tr class="tablisttr">
      <td align="center" class="tablisttitlenowidth">用户编号</td>
      <td class="tabmanagertr"><%=entity.sUserID%></td>
    </tr>
    
    <tr class="tablisttr">
      <td align="center" class="tablisttitlenowidth">用户类型</td>
      <td class="tabmanagertr"><%=entity.sRoleID%></td>
    </tr>
    <tr class="tablisttr">
      <td align="center" class="tablisttitlenowidth">用户部门</td>
      <td class="tabmanagertr"><%=entity.sDeptID%></td>
    </tr>
    <tr class="tablisttr">
      <td align="center" class="tablisttitlenowidth">用户岗位</td>
      <td class="tabmanagertr"><%=entity.sPostID%></td>
    </tr>
    <tr class="tablisttr">
      <td align="center" class="tablisttitlenowidth">用户状态</td>
      <td class="tabmanagertr"><%=entity.sUserState%></td>
    </tr>
  </table></div>

