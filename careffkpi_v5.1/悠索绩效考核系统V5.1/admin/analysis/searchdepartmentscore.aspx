﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="searchdepartmentscore.aspx.cs" Inherits="BasicWeb.admin.analysis.searchdepartmentscore" %>
<%@ Register src="../gdi/gdichart.ascx" tagname="gdichart" tagprefix="uc2" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>部门成绩</title>
    <link href="../style/default/style.css" rel="stylesheet" type="text/css" />

    <script language="javascript" src="/admin/js/System.js" type="text/javascript"></script>
    <script language="javascript" src="/admin/My97DatePicker/WdatePicker.js" type="text/jscript"></script>
</head>
<body>
    <form id="Form1" method="post" runat="server">
    <div class="divTbg">
      <div class="divTbgL"></div>
      <div class="divTbgInfo">
        <div class="divTbgTitle">部门成绩</div>
      </div>
    </div>
    <div class="divTbgF"></div>
    <div class="divSearchBoth">
        <div class="divInfo">
            <div class="divInfoblack">
                <div class="divInfoTopList">
                    <div class="listLeft">
                        <div class="navsearchtitle">
                           部门成绩搜索</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="divInfo">
            <div class="divSearch">
                <div id="searchBox" class="clearsearch">
                     <p>
                        <label for="drpDateID">
                            时间段：</label>
                        <asp:DropDownList runat="server" ID="drpDateID" ToolTip="时间段"></asp:DropDownList>
                    </p>
                    
                    <p>
                        <label for="txtDeptID">
                            部门编号：</label>
                        <asp:TextBox ID="txtDeptID" runat="server" ToolTip="部门编号" Text=""></asp:TextBox>
                    </p>
                     <p>
                        <label for="txtDeptName">
                            部门名称：</label>
                        <asp:TextBox ID="txtDeptName" runat="server" ToolTip="部门名称" Text=""></asp:TextBox>
                    </p>
                    
                   <p>
                        <label for="drpITypeState">
                            部门类型：</label>
                       <asp:DropDownList runat="server" ID="drpDeptType" ToolTip="部门类型">
                                    <asp:ListItem Value="DeptType" Text="code"></asp:ListItem>
                                </asp:DropDownList>
                   </p>
                  
                    <p>
                    
                        <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="确认搜索" CssClass="SkyButtonFocus" />
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="divInfo">
        <div class="divInfoblack">
            <div class="divInfoTopList">
                <div class="listLeft">
                    <div class="navtitle">
                        部门成绩详细列表</div>
                </div>
                <div class="listRight">
                    <ul>
                         <li class="navstylelist">
                            <asp:LinkButton ID="LnkBList" runat="server" OnClick="SkyLnkBAMD_Click">列表</asp:LinkButton>
                        </li>
                        <li class="navstylereport">
                            <asp:LinkButton ID="LnkBReport" runat="server" OnClick="SkyLnkBAMD_Click">图表</asp:LinkButton>
                        </li>
                        <li class="navstyle5">
                            <asp:LinkButton ID="LnkBExcel" runat="server" OnClick="SkyLnkBAMD_Click">导出</asp:LinkButton>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="divInfo">
        <div class="divInfoContext"> 
            <asp:GridView ID="grvInfo" runat="server" BorderStyle="None" CellPadding="0" 
                CellSpacing="1" GridLines="None" 
            Width="100%" CssClass="tablist" HorizontalAlign="Center" 
                AutoGenerateColumns="True" >
            <RowStyle CssClass="tablisttr" HorizontalAlign="Center" />
            <HeaderStyle  CssClass="tablisttitle" HorizontalAlign="Center"></HeaderStyle>
                <%--<Columns>
                    <asp:BoundField DataField="DeptID" HeaderText="部门编号" />
                    <asp:BoundField DataField="DeptName" HeaderText="部门名称" />
                </Columns>--%>
           </asp:GridView>
        </div>
         <div class="divInfoContext">
            <uc2:gdichart ID="gdichart1" runat="server" Visible="false"/>
        </div>
    </div>
    <div class="divInfo">
        <webdiyer:aspnetpager id="Pager" runat="server" AlwaysShow="True" OnPageChanged="Pager_PageChanged"
         ShowCustomInfoSection="Left" ShowPageIndexBox="Always" showpageindex="False"
               CustomInfoHTML="共 <B>%PageCount%</B> 页，当前为第 <B>%CurrentPageIndex%</B> 页，每页 <B>%PageSize%</B> 条" 
               SubmitButtonText="" FirstPageText="首页" LastPageText="尾页" 
               LayoutType="Table" NextPageText="后一页" PrevPageText="前一页" ShowMoreButtons="False" 
               TextAfterPageIndexBox="页" TextBeforePageIndexBox="转到" 
               CustomInfoTextAlign="Left" HorizontalAlign="Right"></webdiyer:aspnetpager>
    </div>
    </form>
</body>
</html>
