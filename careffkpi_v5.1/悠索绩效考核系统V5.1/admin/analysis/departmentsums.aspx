﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="departmentsums.aspx.cs" Inherits="BasicWeb.admin.analysis.departmentsums" %>


<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>

<%@ Register src="../gdi/gdichart.ascx" tagname="gdichart" tagprefix="uc2" %>
<%@ Register src="departmentsumnav.ascx" tagname="departmentsumnav" tagprefix="uc3" %>
<%@ Register src="departmentsuminfo.ascx" tagname="departmentsuminfo" tagprefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>[<% =sDeptName.ToString() %>]指标小类统计分析</title>
    <link href="../style/default/style.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery/jquery.js" type="text/javascript"></script>
    <script language="javascript" src="/admin/js/System.js" type="text/javascript"></script>
    <script language="javascript" src="/admin/My97DatePicker/WdatePicker.js" type="text/jscript"></script>
    <script  language="javascript" src="/editor/xheditor.js" type="text/javascript" ></script>
</head>
<body onload="win_onLoadSize(1000);">
    <form id="Form1" method="post" runat="server"><div id="tbMain">
    <div class="divTbg">
      <div class="divTbgL"></div>
      <div class="divTbgInfo">
        <div class="divTbgTitle">[<% =sDeptName.ToString() %>]指标小类统计分析</div>
      </div>
    </div>
    <div class="divTbgF"></div>
    
    <div class="divInfo">
        <div class="divInfoblack">
            <div class="divInfoTopList">
                <div class="listLeft">
                    <div class="navtitle">
                        <%=sPageListTitle%></div>
                </div>
                <div class="listRight">
                    <uc3:departmentsumnav ID="departmentsumnav1" runat="server" />
                </div>
            </div>
        </div>
    </div>
    <div class="divInfo">
        <div class="divInfoContext">
            <asp:Repeater runat="server" ID="rptInfo">
                <HeaderTemplate>
                    <table width="100%" border="0" cellpadding="0" cellspacing="1" class="tablist" onmouseover="changeto()"  onmouseout="changeback()">
                        <tr class="tablisttitle">
                                   
                            <td>
                                部门编号
                            </td>
                            <td>
                                部门名称
                            </td>
                            <td>
                                大类名称
                            </td>
                            <td>
                                小类编号
                            </td>
                            <td>
                                小类名称
                            </td>
                            <td>
                                小类分值
                            </td>
                            <td>
                                子项条数
                            </td>
                             <td>
                                评价次数
                            </td>
                             <td>
                                按次分解
                            </td>
                            
                           
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr class="tablisttr">
                                               
                        <td>
                            <%#Eval("DEPTID")%>
                        </td>
                        <td>
                            <%#Eval("DeptName")%>
                        </td>
                         <td>
                            <%#Eval("ITypeName")%>
                        </td>
                         <td>
                            <%#Eval("E2")%>
                        </td>
                        <td>
                            <%#Eval("ITypeName2")%>
                        </td>
                        
                         <td><span class="fColorRed">
                            <%#Eval("EVALUE")%></span>
                        </td>
                        <td><span class="fColorBlue">
                            <%#Eval("ECount")%></span>
                        </td>
                         <td><span class="fColorBlue">
                            <%#Eval("BCount")%></span>
                        </td>
                        <td>
                             
                             
                              <%#cHTMLUrlLink("departmentsumsfj.aspx?id=" + sDeptID.ToString() + "&dateid=" + sDateID.ToString() + "&key=" + Eval("E2").ToString(), "按次分解")%>
                        </td>
                        
                    
                        
                       
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                 <tr class="tablisttitle">
                                   
                            <td>
                                
                            </td>
                            <td>
                                
                            </td>

                            <td>
                                
                            </td>
                            <td>
                                
                            </td>
                            <td>
                                
                            </td>
                            <td>
                                <%=Single1.ToString("F2")%>
                            </td>
                            <td>
                                <%=Single2.ToString() %>
                            </td>
                             <td>
                                <%=Single3.ToString() %>
                            </td>
                             <td>
                                
                            </td>
                            
                           
                        </tr>
                    </table></FooterTemplate>
            </asp:Repeater>
        </div>
        <div class="divInfoContext">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="analysis_table_chart">
          <tr>
            <td valign="top">
                <uc1:departmentsuminfo ID="departmentsuminfo1" runat="server" />
            </td>
            <td class="analysis_table_chart_gdi"><uc2:gdichart ID="gdichart1" runat="server"/></td>
          </tr>
        </table>
        </div>
    </div>
    <div class="divInfo" style="display:none;">
        <webdiyer:aspnetpager id="Pager" runat="server" AlwaysShow="True" OnPageChanged="Pager_PageChanged"
         ShowCustomInfoSection="Left" ShowPageIndexBox="Always" showpageindex="False"
               CustomInfoHTML="共 <B>%PageCount%</B> 页，当前为第 <B>%CurrentPageIndex%</B> 页，每页 <B>%PageSize%</B> 条" 
               SubmitButtonText="" FirstPageText="首页" LastPageText="尾页" 
               LayoutType="Table" NextPageText="后一页" PrevPageText="前一页" ShowMoreButtons="False" 
               TextAfterPageIndexBox="页" TextBeforePageIndexBox="转到" 
               CustomInfoTextAlign="Left" HorizontalAlign="Right"></webdiyer:aspnetpager>
    </div>
    </div>
    </form>
</body>
</html>