﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="scorebaklist.aspx.cs" Inherits="BasicWeb.admin.analysis.scorebaklist" %>


<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>评估成绩查询</title>
    
    
    <link href="../style/default/style.css" rel="stylesheet" type="text/css" />

    <script language="javascript" src="/admin/js/System.js" type="text/javascript"></script>
    <script language="javascript" src="/admin/My97DatePicker/WdatePicker.js" type="text/jscript"></script>

</head>
<body>
    <form id="Form1" method="post" runat="server">
    <div class="divTbg">
        <div class="divTbgL">
        </div>
        <div class="divTbgInfo">
            <div class="divTbgTitle">
                评估成绩查询</div>
        </div>
    </div>
    <div class="divTbgF">
    </div>
    <div class="divSearchBoth">
        <div class="divInfo">
            <div class="divInfoblack">
                <div class="divInfoTopList">
                    <div class="listLeft">
                        <div class="navsearchtitle">
                            评估成绩查询</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="divInfo">
            <div class="divSearch">
                <div id="searchBox" class="clearsearch">
                   
                    
                    <p>
                        <label for="drpLType">
                            入库类型：</label>
                        <asp:DropDownList runat="server" ID="drpLType">
                            <asp:ListItem Value="PAllType" Text="codeno"></asp:ListItem>
                        </asp:DropDownList>
                    </p>
                    <p>
                        <label for="drpDateID">
                            时间段：</label>
                        <asp:DropDownList runat="server" ID="drpDateID" ToolTip="时间段"></asp:DropDownList>
                    </p>
                    <p>
                        <label for="txtDeptID">
                            部门名称：</label>
                        <asp:TextBox runat="server" ID="txtDeptID"></asp:TextBox>
                    </p>
                    <p>
                        <label for="txtUserID">
                            人员名称：</label>
                        <asp:TextBox runat="server" ID="txtUserID"></asp:TextBox>
                    </p>
                    <p>
                        <label for="txtPID">
                            批次编号：</label>
                        <asp:TextBox runat="server" ID="txtPID"></asp:TextBox>
                    </p>
                     
                    
                    <p id="searchSubmit">
                        <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="确认搜索" CssClass="SkyButtonFocus" />
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="divInfo">
        <div class="divInfoblack">
            <div class="divInfoTopList">
                <div class="listLeft">
                    <div class="navtitle">
                        评估成绩查询列表</div>
                </div>
                <div class="listRight">
                    <ul>
                        <li class="navstyle1">
                            <input type="checkbox" name="chkselect" id="chkselect" onclick="skySelectCheckList();" />
                            <label for="chkselect">
                                全选</label>
                        </li>
                        <li class="navstyle2">
                            <asp:LinkButton ID="LnkBAdd" runat="server" OnClick="SkyLnkBAMD_Click" Enabled="false">添加</asp:LinkButton>
                        </li>
                        <li class="navstyle3">
                            <asp:LinkButton ID="LnkBEdit" runat="server" OnClick="SkyLnkBAMD_Click">编辑</asp:LinkButton>
                        </li>
                        <li class="navstyle4">
                            <asp:LinkButton ID="LnkBDelete" runat="server" OnClick="SkyLnkBAMD_Click">删除</asp:LinkButton>
                        </li>
                        <li class="navstyle5">
                            <asp:LinkButton ID="LnkBExcel" runat="server" OnClick="SkyLnkBAMD_Click">导出</asp:LinkButton>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="divInfo">
        <div class="divInfoContext">
            <div id="printInfo">
                <asp:Repeater ID="rptInfo" runat="server">
                    <HeaderTemplate>
                        <table width="100%" border="0" cellpadding="0" cellspacing="1" class="tablist">
                            <tr class="tablisttitle">
                                <td>
                                    序号
                                </td>
                                <td>
                                    <%=cHTMLFx%>
                                </td>
                                <%--<td align="center">
                                    工资表名
                                </td>--%>
                                <td align="center">
                                    入库类型
                                </td>
                                
                                <td align="center">
                                    导入批次
                                </td>
                                <td align="center">
                                    时间段编号
                                </td>
                                
                                
                                <td align="center" <%=sUserStyle %>>
                                    用户编号
                                </td>
                                
                                <td align="center" <%=sUserStyle %>>
                                    用户名称
                                </td>
                                <td align="center">
                                    部门编号
                                </td>
                                
                                <td align="center">
                                    部门名称
                                </td>
                                
                                
                                
                                <td align="center">
                                    评价总分
                                </td>
                                <td align="center">
                                    人次
                                </td>
                                <td align="center">
                                    指标个数
                                </td>
                                <td align="center">
                                    最终分
                                </td>
                                <td align="center">
                                    入库时间
                                </td>
                                <td align="center">
                                    操作                                  
                                </td>                             
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr class="tablisttr">
                            <td>
                                <%#Eval("ID") %>
                            </td>
                            <td>
                                <asp:CheckBox runat="server" ID="chkselect" ToolTip='<%#Eval("ID") %>'/>
                            </td>
                           <%-- <td align="center">
                                <%#Eval("STableNameName")%>
                            </td>--%>
                             
                            <td align="center">
                                <%#Eval("LTypeName")%>
                            </td>
                             <td align="center">
                                <%#Eval("PID")%>
                            </td>
                          
                            <td align="center">
                                <%#Eval("DateName")%>


                            </td>
                            <td align="center" <%=sUserStyle %>>
                                <%#Eval("UserID")%>
                            </td>
                            
                            <td align="center"  <%=sUserStyle %>>
                                <%#Eval("UserName")%>
                            </td>
                            <td align="center">
                                <%#Eval("DeptID")%>
                            </td>
                            
                            <td align="center">
                                <%#Eval("DeptName")%>
                            </td>
                            <td align="center">
                                <span class="fColorBlue">
                                <%#Eval("EValueALL")%></span>
                            </td>
                            <td align="center">
                                <%#Eval("EUserCount")%>
                            </td>
                            <td align="center">
                                <%#Eval("ECount")%>
                            </td>
                            <td align="center" title='<%#Eval("EInfo")%>'>
                                <span class="fColorRed">
                                <%#Eval("EValue")%></span>
                            </td>
                            
                            <td align="center">
                                <%#UsToolsDateFormat(Eval("CreateDate"))%>
                            </td>
                            <td>
                                
                                
                                   
                                
                                    <%#cHTMLUrl("scorebakviewmanager.aspx?typeid=1&id=" + Eval("ID").ToString(), "[指标]")%>
                                    <%#cHTMLUrl("scorebakviewmanager.aspx?typeid=2&id=" + Eval("ID").ToString(), "[人员]")%>
                                
                                <span style='display:<%=bIsAdministrator==false?"none":""%>'>
                                    <%#cHTMLUrl(pDefault.sEditUrl + "?id=" + Eval("ID").ToString(), "[修改]")%>
                                </span>
                            </td>
                            
          

                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>
     <div class="divInfo">
        <span class="usHelpMessage"> * 注意事项： 评估成绩需要结束一个时间段后，管理员发布后才能查询；成绩是历史入库数据，以后修改不影响此处的显示；
        </span>  
    </div>
    
    <div class="divInfo">
         <webdiyer:AspNetPager ID="Pager" runat="server" AlwaysShow="True" OnPageChanged="Pager_PageChanged"
                        ShowCustomInfoSection="Left" ShowPageIndexBox="Always" ShowPageIndex="False"
                        CustomInfoHTML="共 <B>%PageCount%</B> 页，当前为第 <B>%CurrentPageIndex%</B> 页，每页 <B>%PageSize%</B> 条"
                        SubmitButtonText="" FirstPageText="首页" LastPageText="尾页" LayoutType="Table" NextPageText="后一页"
                        PrevPageText="前一页" ShowMoreButtons="False" TextAfterPageIndexBox="页" TextBeforePageIndexBox="转到"
                        CustomInfoTextAlign="Left" HorizontalAlign="Right">
                    </webdiyer:AspNetPager>
    </div>
    
    
    
    <div class="divFootInfo">
    </div>
    </form>
</body>
</html>
