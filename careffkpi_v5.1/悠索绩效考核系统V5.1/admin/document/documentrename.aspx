﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="documentrename.aspx.cs" Inherits="BasicWeb.admin.document.documentrename" %>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>文件更名</title>

		<link href="../style/default/style.css" rel="stylesheet" type="text/css" />

    <script language="javascript" src="/admin/js/System.js" type="text/javascript"></script>

			<script language="javascript" src="js/0451skyMyDocument.js"></script>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server"><div id="tbMain">
          <div class="divTbg">
            <div class="divTbgL"></div>
            <div class="divTbgInfo">
              <div class="divTbgTitle"><a href="#" onClick="meoprthis();">
																<div id="mydocumentid" style="COLOR:#000000" title="关闭左侧目录菜单！">← 文件更名</div>
															</a></div>
            </div>
          </div>
          <div class="divTbgF"></div>
          <div id="mypanel">
            <div class="divInfo" id="ddiv1">
              
              <div class="divInfoContext">
                <table width="100%" border="0" align="center" cellPadding="5" cellSpacing="1" class="tablist">
<tr class="MeNewTDLine">
											<td width="100" align="center" class="tablisttitle">原文件名</td>
											<td align="left" bgcolor="#FFFFFF" class="tablisttrleft">&nbsp;
												<asp:textbox id="TextBox1" runat="server" ReadOnly="True" CssClass="textblur" onfocus="this.className='textfocus'"
													onblur="this.className='textblur'"></asp:textbox>
												<asp:literal id="Literal1" runat="server"></asp:literal></td>
										</tr>
										<tr class="MeNewTDLine">
											<td align="center" class="tablisttitle">新文件名</td>
											<td bgcolor="#FFFFFF" class="tablisttrleft">&nbsp;
												<asp:textbox id="TextBox2" runat="server" MaxLength="20" CssClass="textblur" onfocus="this.className='textfocus'"
													onblur="this.className='textblur'"></asp:textbox>
												<asp:literal id="Literal2" runat="server"></asp:literal>
												(只需要输入文件的名称，不需要输入扩展名)</td>
										</tr>
									</table>
              </div>
            </div>
          </div>
          <div class="divSave"><asp:button id="Button1" runat="server" Text="确定更名" class="SkyButtonFocus" OnClick="Button1_Click"></asp:button></div>
        </div>
			<asp:Literal id="Literal3" runat="server"></asp:Literal>
			<asp:Literal id="LiteralCss" runat="server"></asp:Literal></form>
	</body>
</HTML>
