
//系统定义的特殊字符集
var specialChars = /[!@#\$%\^&\*\(\)\{\}\[\]<>_\+\|~`-]|[=\/\\\?;:,！·#￥%……—*（）——、《》，。？'"]/g;
var keyArray = new Array();
/******************************************************
*以下是对字符串对象（String）的扩展函数，任何String对象都可
*使用这些函数，例如：
var str = " dslf dsf  sfd  ";
alert(str.trim());  //显示这样的字符串"dslf dsf  sfd"
alert(str.deleteSpace());  //显示这样的字符串"dslfdsfsfd"
******************************************************/
 
/*
        function:在字符串左边补指定的字符串
        parameter：
                countLen:结果字符串的长度
                addStr:要附加的字符串
        return:处理后的字符串
*/
String.prototype.lpad = function(countLen,addStr)
{
        // 如果countLen不是数字，不处理返回
        if(isNaN(countLen))return this;

        // 初始字符串长度大于指定的长度，则不需处理
        if(initStr.length >= countLen)return this;

        var initStr = this;        // 初始字符串
        var tempStr = new String();        // 临时字符串

        for(;;)
        {
                tempStr += addStr;
                if(tempStr.length >= countLen - initStr.length)
                {
                        tempStr = tempStr.substring(0,countLen - initStr.length);
                        break;
                }
        }
        return tempStr + initStr;
}


/*
        function:在字符串右边补指定的字符串
        parameter：
                countLen:结果字符串的长度
                addStr:要附加的字符串
        return:处理后的字符串
*/
String.prototype.rpad = function(countLen,addStr)
{
        // 如果countLen不是数字，不处理返回
        if(isNaN(countLen))return this;

        // 初始字符串长度大于指定的长度，则不处理返回
        if(initStr.length >= countLen)return this;

        var initStr = this;        // 初始字符串

        for(;;)
        {
                initStr += addStr;
                if(initStr.length >= countLen)
                {
                        initStr = initStr.substring(0,countLen);
                        break;
                }
        }
        return initStr;
}

/*
        function: 去掉字符串中所有的空格
        return: 处理后的字符串
*/
String.prototype.atrim = function()
{
    // 用正则表达式将右边空格用空字符串替代。
    return this.replace(/(\s+)|(　+)/g, "");
}

// 增加一个名为 trim 的函数作为
// String 构造函数的原型对象的一个方法。
String.prototype.trim = function()
{
    // 用正则表达式将前后空格用空字符串替代。
    return this.replace(/(^\s+)|(\s+$)|(^　+)|(　+$)/g, "");
}

/*
        function:去掉字符串左边的空格
        return:处理后的字符串
*/
String.prototype.ltrim = function()
{
        return this.replace(/(^\s+)|(^　+)/g,"");
}


/*
        function:去掉字符串右边的空格
        return:处理后的字符串
*/
String.prototype.rtrim = function()
{
        return this.replace(/(\s+$)|(　+$)/g,"");
}


/*
        function:获得字符串的字节数
        return:字节数
        example:"test测试".getByte值为8
*/
String.prototype.getByte = function()
{
        var intCount = 0;
        for(var i = 0;i < this.length;i ++)
        {
            // Ascii码大于255是双字节的字符
            var ascii = this.charCodeAt(i).toString(16);
            var byteNum = ascii.length / 2.0;
            intCount += (byteNum + (ascii.length % 2) / 2);
        }
        return intCount;
}

/*
        function: 指定字符集半角字符全部转变为对应的全角字符
        parameter:
                dbcStr: 要转换的半角字符集合
        return: 转换后的字符串
*/
String.prototype.dbcToSbc = function(dbcStr)
{
        var resultStr = this;

        for(var i = 0;i < this.length;i ++)
        {
                switch(dbcStr.charAt(i))
                {
                        case ",":
                                resultStr = resultStr.replace(/\,/g,"，"); 
                                break;
                        case "!":
                                resultStr = resultStr.replace(/\!/g,"！"); 
                                break;
                        case "#":
                                resultStr = resultStr.replace(/\#/g,"＃"); 
                                break;
                        case "|":
                                resultStr = resultStr.replace(/\|/g,"|"); 
                                break;
                        case ".":
                                resultStr = resultStr.replace(/\./g,"。"); 
                                break;
                        case "?":
                                resultStr = resultStr.replace(/\?/g,"？"); 
                                break;
                        case ";":
                                resultStr = resultStr.replace(/\;/g,"；"); 
                                break;
                }
        }
        return resultStr;
}

//获取字符串按字节数指定的字串
String.prototype.bytesubstr = function(index1,index2)
{
        var resultStr = "";
        var byteCount = 0;
        for(var i = index1;i < index2;i ++)
        {
                if(i > this.length)break;
                if(this.charCodeAt(i) > 255)byteCount += 2;
                else byteCount += 1;
                if(byteCount > (index2 - index1))break;
                resultStr += this.charAt(i);
        }
        return resultStr;
}

//判断字符串是否是数字字符串，若是则返回true，否则返回false
String.prototype.isNumber = function() {
	return (this.isInt() || this.isFloat());
}
//判断字符串是否是浮点数字符串，若是则返回true，否则返回false
String.prototype.isFloat = function() {
	return /^(?:-?|\+?)\d*\.\d+$/g.test(this);
}
//判断字符串是否是整数字符串，若是则返回true，否则返回false
String.prototype.isInt = function() {
	return /^(?:-?|\+?)\d+$/g.test(this);
}
//判断字符串是否是正数字符串，若是正数则返回true，否则返回false
String.prototype.isPlus = function() {
	return this.isPlusInt() || this.isPlusFloat();
}
//判断字符串是否是正浮点数字符串，若是正数则返回true，否则返回false
String.prototype.isPlusFloat = function() {
	return /^\+?\d*\.\d+$/g.test(this);
}
//判断字符串是否是正整数字符串，若是正数则返回true，否则返回false
String.prototype.isPlusInt = function() {
	return /^\+?\d+$/g.test(this);
}
//判断字符串是否是负数字符串，若是正数则返回true，否则返回false
String.prototype.isMinus = function() {
	return this.isMinusInt() || this.isMinusFloat();
}
//判断字符串是否是负浮点数字符串，若是正数则返回true，否则返回false
String.prototype.isMinusFloat = function() {
	return /^-\d*\.\d+$/g.test(this);
}
//判断字符串是否是负整数字符串，若是正数则返回true，否则返回false
String.prototype.isMinusInt = function() {
	return /^-\d+$/g.test(this);
}

//判断字符串是否只包含单词字符，若是则返回true，否则返回false
String.prototype.isLeastCharSet = function() {
	return !(/[^A-Za-z0-9_]/g.test(this));
}
//判断字符串是否是Email字符串，若是则返回true，否则返回false
String.prototype.isEmail = function() {
	return /^\w+@.+\.\w+$/g.test(this);
}
//判断字符串是否是邮政编码字符串，若是则返回true，否则返回false
String.prototype.isZip = function() {
	return /^\d{6}$/g.test(this);
}
//判断字符串是否是固定电话号码字符串，若是则返回true，否则返回false
String.prototype.isFixedTelephone = function() {
	return /^(\d{2,4}-)?((\(\d{3,5}\))|(\d{3,5}-))?\d{3,18}(-\d+)?$/g.test(this);
}
//判断字符串是否是手机电话号码字符串，若是则返回true，否则返回false
String.prototype.isMobileTelephone = function() {
	return /^13\d{9}$/g.test(this);
}
//判断字符串是否是电话号码字符串，若是则返回true，否则返回false
String.prototype.isTelephone = function() {
	return this.isMobileTelephone() || this.isFixedTelephone();
}
//判断字符串是否是日期字符串，若是则返回true，否则返回false
String.prototype.isDate = function() {
	return /^\d{1,4}-(?:(?:(?:0?[1,3,5,7,8]|1[0,2])-(?:0?[1-9]|(?:1|2)[0-9]|3[0-1]))|(?:(?:0?[4,6,9]|11)-(?:0?[1-9]|(?:1|2)[0-9]|30))|(?:(?:0?2)-(?:0?[1-9]|(?:1|2)[0-9])))$/g.test(this);
}
//判断字符串是否是时间字符串，若是则返回true，否则返回false
String.prototype.isTime = function() {
	return /^(?:(?:0?|1)[0-9]|2[0-3]):(?:(?:0?|[1-5])[0-9]):(?:(?:0?|[1-5])[0-9]).(?:\d{1,3})$/g.test(this);
}
//判断字符串是否是日期时间字符串，若是则返回true，否则返回false
String.prototype.isDateTime = function() {
	return /^\d{1,4}-(?:(?:(?:0?[1,3,5,7,8]|1[0,2])-(?:0?[1-9]|(?:1|2)[0-9]|3[0-1]))|(?:(?:0?[4,6,9]|11)-(?:0?[1-9]|(?:1|2)[0-9]|30))|(?:(?:0?2)-(?:0?[1-9]|(?:1|2)[0-9]))) +(?:(?:0?|1)[0-9]|2[0-3]):(?:(?:0?|[1-5])[0-9]):(?:(?:0?|[1-5])[0-9]).(?:\d{1,3})$/g.test(this);
}
//比较日期字符串，若相等则返回0，否则返回当前日期字符串和目标字符串之间相差的毫秒数，若其中一个字符串不符合日期或日期时间格式，则返回null
String.prototype.compareDate = function(target) {
	var thisDate = this.toDate();
	var targetDate = target.toDate();
	if (thisDate == null || targetDate == null) {
		return null;
	}
	else {
		return thisDate.getTime() - targetDate.getTime();
	}
}
//判断日期字符串指定的时期是否是当前日期，若是则返回true，否则返回false
String.prototype.isToday = function() {
	return this.trim().split(' ')[0].compareDate(getSysDate()) == 0;
}
//判断日期字符串指定的时期是否是当前日期之前，若是则返回true，否则返回false
String.prototype.isBeforeDate = function(baseDate) {
	if (baseDate == null) {
		baseDate = getSysDate();
	}
	return this.trim().split(' ')[0].compareDate(baseDate.trim().split(' ')[0]) < 0;
}
//判断日期字符串指定的时期是否是当前日期之后，若是则返回true，否则返回false
String.prototype.isAfterDate = function(baseDate) {
	if (baseDate == null) {
		baseDate = getSysDate();
	}
	return this.trim().split(' ')[0].compareDate(baseDate.trim().split(' ')[0]) > 0;
}

//判断日期时间字符串指定的时期是否是指定日期时间之前，若是则返回true，否则返回false
String.prototype.isBeforeDateTime = function(baseDateTime) {
	if (baseDateTime == null) {
		baseDateTime = getSysDateTime();
	}
	return this.trim().compareDate(baseDateTime.trim()) < 0;
}
//判断日期时间字符串指定的时期是否是指定日期时间之后，若是则返回true，否则返回false
String.prototype.isAfterDateTime = function(baseDateTime) {
	if (baseDateTime == null) {
		baseDateTime = getSysDateTime();
	}
	return this.trim().compareDate(baseDateTime.trim()) > 0;
}



//判断字符串中是否含有特殊字符，若有则返回true，否则返回false
String.prototype.hasSpecialChar = function() {
	specialChars.test('');
	return specialChars.test(this);
}
//删除字符串中的空格
String.prototype.deleteSpace = function() {
	return this.replace(/( +)|(　+)/g, '');
}
//删除字符串中指定的字符串
String.prototype.remove = function(str) {
	if (str == null || str == '') {
		return this;
	}
	return this.replace(str.toRegExp('g'), '');
}
//将字符串中包含的find字符串替换成target字符串，返回替换后的结果字符串
String.prototype.replaceByString = function(find, target) {
	return this.replace(find.toRegExp('g'), target);
}
//将字符串转换成相应的正则表达式
String.prototype.toRegExp = function(regType) {
	if (regType == null || regType.trim() == '') {
		regType = 'g';
	}
	var find = ['\\\\', '\\$', '\\(', '\\)', '\\*', '\\+', '\\.', '\\[', '\\]', '\\?', '\\^', '\\{', '\\}', '\\|', '\\/'];
	var str = this;
	for (var i = 0; i < find.length; i++) {
		str = str.replace(new RegExp(find[i], 'g'), find[i]);
	}
	return new RegExp(str, regType);
}
//将字符串转换成Date对象，要求字符串必须符合日期或日期时间格式，否则返回null
String.prototype.toDate = function() {
	if (this.isDate()) {
		var data = this.split('-');
		return new Date(parseInt(data[0].replace(/^0+/g, '')), parseInt(data[1].replace(/^0+/g, '')) - 1, parseInt(data[2].replace(/^0+/g, '')));
	}
	else if (this.isDateTime()) {
		var data = this.split(' ');
		var date = data[0].split('-');
		var time = data[1].split(".")[0].split(':');
		return new Date(parseInt(date[0].replace(/^0+/g, '')), parseInt(date[1].replace(/^0+/g, '')) - 1, parseInt(date[2].replace(/^0+/g, '')), 
			parseInt(time[0].replace(/^0+/g, '')), parseInt(time[1].replace(/^0+/g, '')), parseInt(time[2].replace(/^0+/g, '')));
	}
	else {
		return null;
	}
}
//将字符串按HTML需要的编码方式编码
String.prototype.encodeHtml = function() {
	var strToCode = this.replace(/</g,"&lt;");
	strToCode = strToCode.replace(/>/g,"&gt;");
	return strToCode;
}
//将字符串按HTML需要的编码方式解码
String.prototype.decodeHtml = function() {
	var strToCode = this.replace(/&lt;/g,"<");
	strToCode = strToCode.replace(/&gt;/g,">");
	return strToCode;
}
/*********************************************
*字符串对象（String）扩展函数结束
*********************************************/

/*********************************************
*以下是各种独立的全局公用函数，程序可直接调用
*********************************************/
//表单提交函数
function pageshow(formI, action, check, target) {
	var canDisable = (window.event != null && window.event.srcElement != null && window.event.srcElement.tagName.toLowerCase() == "button");
	if (canDisable) {
		window.event.srcElement.disabled = true;
	}
	if (check != true || checkValidity(formI)) {
		document.forms.item(formI).action = action;
		if (target != null && target.trim() != "") {
			document.forms.item(formI).target = target;
		}
		document.forms.item(formI).submit();
	}
	else {
		if (canDisable) {
			window.event.srcElement.disabled = false;
		}
	}
}


//列表页面删除函数
function remove(formI, action, name,disMsg) {

	if (hasSelected(name)) {
	   	var argv = remove.arguments;
    	var argc = remove.arguments.length;
    	var msg = (argc > 3) ? argv[3] : "确定要删除选中的记录吗？";
		if (confirm(msg)) {
			pageshow(formI, action);
		}
	}
	else {
		alert("请选择记录！");
	}
}


//记录修改或详细信息函数
function detail(formI, action, name) {
	if (hasSelectedOne(name)) {
		pageshow(formI, action);
	}
	else {
		alert("请选择一条记录进行此操作！");
	}
}

//对指定域设置默认的日期时间，
//elName为要设置的域的名称，
//offset为与当前时间的差值（毫秒数），在当前日期时间以前为负数，在当前日期时间以后为正数，默认为0
function setDefaultDateTime(elName, offset){
	if (offset == null) {
		offset = 0;
	}
	var mydate = new Date();
	mydate.setTime(mydate.getTime() + offset);
	document.all(elName).value = mydate.getFullYear() + "-" + (mydate.getMonth()+1) + "-" + mydate.getDate()
		 + " " + mydate.getHours() + ":" + mydate.getMinutes() + ":" + mydate.getSeconds() + "." + mydate.getMilliseconds();
}

//对指定域设置默认的日期，
//elName为要设置的域的名称，
//offset为与当前日期的差值（天数），在当前日期以前为负数，在当前日期以后为正数，默认为0
function setDefaultDate(elName, offset){
	if (offset == null) {
		offset = 0;
	}
	var mydate = new Date();
	mydate.setTime(mydate.getTime() + offset * 86400000);
	document.all(elName).value = mydate.getFullYear() + "-" + (mydate.getMonth()+1) + "-" + mydate.getDate();
}

//对指定域设置默认的时间，
//elName为要设置的域的名称，
//offset为与当前时间的差值（毫秒数），在当前时间以前为负数，在当前时间以后为正数，默认为0
function setDefaultTime(elName, offset){
	if (offset == null) {
		offset = 0;
	}
	var mydate = new Date();
	mydate.setTime(mydate.getTime() + offset);
	document.all(elName).value = mydate.getHours() + ":" + mydate.getMinutes() + ":" + mydate.getSeconds() + "." + mydate.getMilliseconds();
}

//询问函数
function confirmMsg(formI, action, disMsg) {


		if (confirm(disMsg)) {
			pageshow(formI, action);
		}

}
//对指定表单中的输入域进行合法性检查
function checkValidity(formI) {
	var form = document.forms.item(formI);
	var els = form.tags("input");
	for (var i = 0; i < els.length; i++) {
		var notNull = els[i].getAttribute("notnull");
		if (notNull != null) {
			if (els[i].value == null || els[i].value.trim() == "") {
				alert(notNull);
				try {
					els[i].focus();
				}
				catch (e) {}
				return false;
			}
		}
		if (!checkType(els[i])) {
			return false;
		}
	}
	var els = form.tags("textarea");
	for (var i = 0; i < els.length; i++) {
		var notNull = els[i].getAttribute("notnull");
		if (notNull != null) {
			if (els[i].value == null || els[i].value.trim() == "") {
				alert(notNull);
				try {
					els[i].focus();
				}
				catch (e) {}
				return false;
			}
		}
		if (!checkType(els[i])) {
			return false;
		}
	}
	var els = form.tags("select");
	for (var i = 0; i < els.length; i++) {
		var notNull = els[i].getAttribute("notnull");
		if (notNull != null) {
			if (els[i].options[els[i].selectedIndex].value == null || els[i].options[els[i].selectedIndex].value.trim() == "") {
				alert(notNull);
				try {
					els[i].focus();
				}
				catch (e) {}
				return false;
			}
		}
	}
	return true;
}
//弹出对话框确定响应函数
function dialogOk(checkname) {
	var els = document.getElementsByName(checkname);
	var rtnValue = new Array();
	var rtnIndex = 0;
	for (var i = 0; i < els.length; i++) {
		if (els[i].checked) {
			rtnValue[rtnIndex++] = els[i].value;
		}
	}
	if (rtnIndex == 0) {
		alert("对不起，您还没有选择任何选项！");
		return;
	}
	window.returnValue = rtnValue.join(',');
	
	window.close();
}
//弹出对话框取消响应函数
function dialogCancel() {
	window.returnValue = null;
	window.close();
}
//全选函数，将页面中所有名称为name的复选框的选择状态改为select状态
function selectAll(name, select){
	var selElements = document.getElementsByName(name);
	for (var i = 0; i < selElements.length; i++) {
		if (selElements[i].tagName.toLowerCase() == "input") {
			if (selElements[i].type.toLowerCase() == "checkbox") {
				selElements[i].checked = select;
			}
		}
	}
}
//全选函数，将指定表单中所有名称为name的复选框的选择状态改为select状态
function selectAllInForm(formI, name, select){
	var selElements = document.forms.item(formI).tags("input");
	for (var i = 0; i < selElements.length; i++) {
		if (selElements[i].name == name) {
			if (selElements[i].type.toLowerCase() == "checkbox") {
				selElements[i].checked = select;
			}
		}
	}
}
//判断用户是否在页面上选择指定名称的复选框
function hasSelected(name) {
	var selElements = document.getElementsByName(name);
	for (var i = 0; i < selElements.length; i++) {
		if (selElements[i].tagName.toLowerCase() == "input") {
			if (selElements[i].type.toLowerCase() == "checkbox" || selElements[i].type.toLowerCase() == "radio") {
				if (selElements[i].checked) {
					return true;
				}
			}
		}
	}
	return false;
}
//判断用户是否在页面上选择了一个且仅有一个指定名称的复选框
function hasSelectedOne(name) {
	var selElements = document.getElementsByName(name);
	var selected = false;
	for (var i = 0; i < selElements.length; i++) {
		if (selElements[i].tagName.toLowerCase() == "input") {
			if (selElements[i].type.toLowerCase() == "checkbox" || selElements[i].type.toLowerCase() == "radio") {
				if (selElements[i].checked) {
					if (selected == false) {
						selected = true;
					}
					else {
						return false;
					}
				}
			}
		}
	}
	return selected;
}
//获取浏览器的版本号
function getAppVer() {
	var sVer = navigator.appVersion;
	var nVer = sVer.indexOf("MSIE");
	var appVer = "";
	if (nVer > 0) {
		appVer = "M" + sVer.substring(nVer + 5, nVer + 9);
	}
	else {
		appVer = "N" + sVer.substring(0, 4);
	}
	if (appVer.charAt(4) == " ") {
		appVer = appVer.substring(0, 4) + "0";
	}
	return appVer;
}
//检查指定的年份是否闰年
function checkLeapYear(year) {
	if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0)) {
		return true;
	}
	return false;
}
//获取系统日期字符串
function getSysDate() {
	var today  = new Date();
	var nYear  = today.getFullYear();
	var nMonth = today.getMonth() + 1;
	var nDay   = today.getDate();
	var sToday = "";
	sToday += (nYear < 1000) ? "" + (1900 + nYear) : nYear;
	sToday += "-";
	sToday += (nMonth < 10) ? "0" + nMonth : nMonth;
	sToday += "-"
	sToday += (nDay < 10) ? "0" + nDay : nDay;
	return sToday;
}
//获取系统日期时间字符串
function getSysDateTime() {
	var today  = new Date();
	var nYear  = today.getFullYear();
	var nMonth = today.getMonth() + 1;
	var nDay   = today.getDate();
	var nHours = today.getHours();
	var nMinutes = today.getMinutes();
	var nSeconds = today.getSeconds();
	var nMilliSeconds = today.getMilliseconds();
	var sToday = "";
	sToday += (nYear < 1000) ? "" + (1900 + nYear) : nYear;
	sToday += "-";
	sToday += (nMonth < 10) ? "0" + nMonth : nMonth;
	sToday += "-"
	sToday += (nDay < 10) ? "0" + nDay : nDay;
	sToday += " ";
	sToday += (nHours < 10) ? "0" + nHours : nHours;
	sToday += ":"
	sToday += (nMinutes < 10) ? "0" + nMinutes : nMinutes;
	sToday += ":"
	sToday += (nSeconds < 10) ? "0" + nSeconds : nSeconds;
	if (nMilliSeconds < 10) {
		sToday += "00" + nMilliSeconds;
	}
	else if (nMilliSeconds < 100) {
		sToday += "0" + nMilliSeconds;
	}
	else {
		sToday += nMilliSeconds;
	}
	return sToday;
}

/************************************************
*全局公用函数结束
************************************************/

/************************************************
*以下是操作cookie的函数
************************************************/
// returns the decoded value of a cookie
function getCookieVal(offset) {
	var endstr = document.cookie.indexOf(";", offset);
	if (endstr == -1)
		endstr = document.cookie.length;
	return unescape(document.cookie.substring(offset, endstr));
}

// returns the value of the specified cookie, or null
function getCookie(name) {
	var arg = name + "=";
	var alen = arg.length;
	var clen = document.cookie.length;
	var i = 0;
	while (i < clen) {
		var j = i + alen;
		if (document.cookie.substring(i, j) == arg)
			return getCookieVal(j);
		i = document.cookie.indexOf(" ", i) + 1;
		if (i == 0) 
			break;
	}
	return "";
}

// creates or updates a cookie
function setCookie(name, value) {
	var argv = setCookie.arguments;
	var argc = setCookie.arguments.length;
	var expires = (argc > 2) ? argv[2] : null;
	var path = (argc > 3) ? argv[3] : null;
	var domain = (argc > 4) ? argv[4] : null;
	var secure = (argc > 5) ? argv[5] : false;
	var cookie = name + "=" + escape(value) +
 	  	((expires == null) ? "" : ("; expires=" + expires.toGMTString())) +
		((path == null) ? "" : ("; path=" + path)) +
		((domain == null) ? "" : ("; domain=" + domain)) +
		((secure == true) ? "; secure" : "");
	document.cookie = cookie;
	//alert(cookie + "\n\n" + document.cookie);
}
// deletes a cookie
function deleteCookie(name) {
	var exp = new Date();
	exp.setTime(exp.getTime() - 1);
	var cval = getCookie(name);
	if (cval != null)
		document.cookie = name + "=" + cval + "; expires=" + exp.toGMTString();
}
/************************************************
*操作cookie的函数结束
************************************************/

/***********************************************
*各种页面响应函数
************************************************/
//屏蔽右键菜单
function closeContextMenu(myevent) {
	var tagName = myevent.srcElement.tagName.toLowerCase();
	if (tagName != "input" && tagName != "textarrea" && tagName != "select") {
		return false;
	}
}

//屏蔽Shift键加鼠标左键在新窗口中打开
function closeNewWindow(myevent) {
	return (myevent.shiftKey == false);
}

//页面加载时屏蔽非文字编辑的backspace键和F5(刷新)、F11(全屏显示)、Ctrl+N(在新窗口打开)键、快捷键
//对设置了checktype的文本域进行有效性检查
function checkKeyDown(myevent) {
	var tagName = myevent.srcElement.tagName.toLowerCase();
	//alert(myevent.keyCode);
	if (tagName == "input" 
		&& myevent.srcElement.type == "text" 
		&& myevent.keyCode >= 48
		&& myevent.keyCode <= 222) {
		var checktype = myevent.srcElement.getAttribute("checktype");
		if (checktype != null && checktype.trim() != "" && checktype.trim().toLowerCase() != "none") {
			switch (checktype) {
				case "number", "float" : 
					if (!(myevent.keyCode >= 48 && myevent.keyCode <= 57 && myevent.shiftKey == false)
						&& !(myevent.keyCode >= 96 && myevent.keyCode <= 105 && myevent.shiftKey == false)
						&& !((myevent.keyCode == 189 || myevent.keyCode == 109) && myevent.shiftKey == false && myevent.srcElement.value.indexOf("-") < 0 && myevent.srcElement.value.indexOf("+") < 0)
						&& !(myevent.keyCode == 187 && myevent.shiftKey == true && myevent.srcElement.value.indexOf("+") < 0 && myevent.srcElement.value.indexOf("-") < 0)
						&& !(myevent.keyCode == 107 && myevent.shiftKey == false && myevent.srcElement.value.indexOf("+") < 0 && myevent.srcElement.value.indexOf("-") < 0)
						&& !((myevent.keyCode == 190 || myevent.keyCode == 110) && myevent.shiftKey == false && myevent.srcElement.value.indexOf(".") < 0)) {
						myevent.keyCode = 0;
						myevent.returnValue = 0;
					}
					break;
				case "int" : 
					if (!(myevent.keyCode >= 48 && myevent.keyCode <= 57 && myevent.shiftKey == false)
						&& !(myevent.keyCode >= 96 && myevent.keyCode <= 105 && myevent.shiftKey == false)
						&& !((myevent.keyCode == 189 || myevent.keyCode == 109) && myevent.shiftKey == false && myevent.srcElement.value.indexOf("-") < 0 && myevent.srcElement.value.indexOf("+") < 0)
						&& !(myevent.keyCode == 187 && myevent.shiftKey == true && myevent.srcElement.value.indexOf("+") < 0 && myevent.srcElement.value.indexOf("-") < 0)
						&& !(myevent.keyCode == 107 && myevent.shiftKey == false && myevent.srcElement.value.indexOf("+") < 0 && myevent.srcElement.value.indexOf("-") < 0)) {
						myevent.keyCode = 0;
						myevent.returnValue = 0;
					}
					break;
				case "minus", "minusfloat" : 
					if (!(myevent.keyCode >= 48 && myevent.keyCode <= 57 && myevent.shiftKey == false)
						&& !(myevent.keyCode >= 96 && myevent.keyCode <= 105 && myevent.shiftKey == false)
						&& !((myevent.keyCode == 189 || myevent.keyCode == 109) && myevent.shiftKey == false && myevent.srcElement.value.indexOf("-") < 0)
						&& !((myevent.keyCode == 190 || myevent.keyCode == 110) && myevent.shiftKey == false && myevent.srcElement.value.indexOf(".") < 0)) {
						myevent.keyCode = 0;
						myevent.returnValue = 0;
					}
					break;
				case "plus", "plusfloat" : 
					if (!(myevent.keyCode >= 48 && myevent.keyCode <= 57 && myevent.shiftKey == false)
						&& !(myevent.keyCode >= 96 && myevent.keyCode <= 105 && myevent.shiftKey == false)
						&& !(myevent.keyCode == 187 && myevent.shiftKey == true && myevent.srcElement.value.indexOf("+") < 0)
						&& !(myevent.keyCode == 107 && myevent.shiftKey == false && myevent.srcElement.value.indexOf("+") < 0)
						&& !((myevent.keyCode == 190 || myevent.keyCode == 110) && myevent.shiftKey == false && myevent.srcElement.value.indexOf(".") < 0)) {
						myevent.keyCode = 0;
						myevent.returnValue = 0;
					}
					break;
				case "plusint" : 
					if (!(myevent.keyCode >= 48 && myevent.keyCode <= 57 && myevent.shiftKey == false)
						&& !(myevent.keyCode >= 96 && myevent.keyCode <= 105 && myevent.shiftKey == false)
						&& !(myevent.keyCode == 187 && myevent.shiftKey == true && myevent.srcElement.value.indexOf("+") < 0)
						&& !(myevent.keyCode == 107 && myevent.shiftKey == false && myevent.srcElement.value.indexOf("+") < 0)) {
						myevent.keyCode = 0;
						myevent.returnValue = 0;
					}
					break;
				case "minusint" : 
					if (!(myevent.keyCode >= 48 && myevent.keyCode <= 57 && myevent.shiftKey == false)
						&& !(myevent.keyCode >= 96 && myevent.keyCode <= 105 && myevent.shiftKey == false)
						&& !(myevent.keyCode == 189 && myevent.shiftKey == true && myevent.srcElement.value.indexOf("-") < 0)) {
						myevent.keyCode = 0;
						myevent.returnValue = 0;
					}
					break;
				case "wordchar" :
					if (!(myevent.keyCode >= 48 && myevent.keyCode <= 57 && myevent.shiftKey == false)
						&& !(myevent.keyCode >= 96 && myevent.keyCode <= 105 && myevent.shiftKey == false)
						&& !(myevent.keyCode >= 65 && myevent.keyCode <= 90)
						&& !(myevent.keyCode == 189 && myevent.shiftKey == true)) {
						myevent.keyCode = 0;
						myevent.returnValue = 0;
					}
					break;
				case "zip" : 
					if (!(myevent.keyCode >= 48 && myevent.keyCode <= 57 && myevent.shiftKey == false && myevent.srcElement.value.length < 6) && !(myevent.keyCode >= 96 && myevent.keyCode <= 105 && myevent.shiftKey == false && myevent.srcElement.value.length < 6)) {
						myevent.keyCode = 0;
						myevent.returnValue = 0;
					}
					break;
				case "mobile" : 
					if (!(myevent.keyCode >= 48 && myevent.keyCode <= 57 && myevent.shiftKey == false && myevent.srcElement.value.length < 11) && !(myevent.keyCode >= 96 && myevent.keyCode <= 105 && myevent.shiftKey == false && myevent.srcElement.value.length < 6)) {
						myevent.keyCode = 0;
						myevent.returnValue = 0;
					}
					break;
				case "telephone" : 
					if (!(myevent.keyCode >= 48 && myevent.keyCode <= 57 && myevent.shiftKey == false)
						&& !(myevent.keyCode >= 96 && myevent.keyCode <= 105 && myevent.shiftKey == false)
						&& !(myevent.keyCode == 48 && myevent.shiftKey == true && myevent.srcElement.value.indexOf(")") < 0)
						&& !(myevent.keyCode == 57 && myevent.shiftKey == true && myevent.srcElement.value.indexOf("(") < 0)
						&& !((myevent.keyCode == 189 || myevent.keyCode == 109) && myevent.shiftKey == false)) {
						myevent.keyCode = 0;
						myevent.returnValue = 0;
					}
					break;
				case "date" : 
					if (!(myevent.keyCode >= 48 && myevent.keyCode <= 57 && myevent.shiftKey == false)
						&& !(myevent.keyCode >= 96 && myevent.keyCode <= 105 && myevent.shiftKey == false)
						&& !((myevent.keyCode == 189 || myevent.keyCode == 109) && myevent.shiftKey == false)) {
						myevent.keyCode = 0;
						myevent.returnValue = 0;
					}
					break;
				case "time" : 
					if (!(myevent.keyCode >= 48 && myevent.keyCode <= 57 && myevent.shiftKey == false)
						&& !(myevent.keyCode >= 96 && myevent.keyCode <= 105 && myevent.shiftKey == false)
						&& !(myevent.keyCode == 186 && myevent.shiftKey == true)) {
						myevent.keyCode = 0;
						myevent.returnValue = 0;
					}
					break;
				case "datetime" : 
					if (!(myevent.keyCode >= 48 && myevent.keyCode <= 57 && myevent.shiftKey == false)
						&& !(myevent.keyCode >= 96 && myevent.keyCode <= 105 && myevent.shiftKey == false)
						&& !((myevent.keyCode == 189 || myevent.keyCode == 109) && myevent.shiftKey == false)
						&& !(myevent.keyCode == 32 && myevent.srcElement.value.indexOf(" ") < 0)
						&& !(myevent.keyCode == 186 && myevent.shiftKey == true)) {
						myevent.keyCode = 0;
						myevent.returnValue = 0;
					}
					break;
				case "nospecialchar" :
					//TODO:对不允许输入特殊字符的输入域进行键入时检查
					var special = specialChars.source;
					break;
				default :
					break;
			}
		}
	}
	
	if (myevent.keyCode == 8) {
		if (tagName != "input" && tagName != "textarea" && tagName != "select") {
			myevent.keyCode = 0;
			myevent.returnValue = 0;
			return;
		}
	}
	quickKey(myevent);
	if (myevent.keyCode == 116 || myevent.keyCode == 122) {
		myevent.keyCode = 0;
		myevent.returnValue = 0;
		return;
	}
	if (myevent.ctrlKey == true && myevent.keyCode == 78) {
		myevent.keyCode = 0;
		myevent.returnValue = 0;
		return;
	}
}
//得到快捷键进行处理
function quickKey(myevent){
	for (var i=0;i<keyArray.length;i++){
		var key =keyArray[i].quickKey;
		if (typeof(key) != "undefined"){
			if (isAscii(key,getFunctionKey(keyArray[i]))){
				if (typeof(keyArray[i].quickFunction) != "undefined" && "" != keyArray[i].quickFunction){
					var add = new Function("return " + keyArray[i].quickFunction);
					add();
				}else{
					keyArray[i].onclick();
				}
				myevent.keyCode = 0;
				myevent.returnValue = 0;
			}
		}
	}
}
//判断功能键
function getFunctionKey(myevent){
	var functionKey = myevent.functionKey;
	if (typeof(functionKey) != "undefined"){
		switch (functionKey.toLowerCase()) {
			case "alt" :
				return functionKey.toLowerCase();
			break;
			case "ctrl" :
				return functionKey.toLowerCase();
			break;
			case "shift" :
				return functionKey.toLowerCase();
			break;
			case "none" :
				return functionKey.toLowerCase();
			break;
			default :
			break;
		}
	}
	return "ctrl";
}
//判断按键
function isAscii(key,functionKey){
	switch (functionKey){
		case "ctrl":
			if (event.ctrlKey && key.toLowerCase().charCodeAt(0)==event.keyCode){
				return true;
			}
			if (event.ctrlKey && key.toUpperCase().charCodeAt(0)==event.keyCode){
				return true;
			}
		break;
		case "alt":
			if (event.altKey && key.toLowerCase().charCodeAt(0)==event.keyCode){
				return true;
			}
			if (event.altKey && key.toUpperCase().charCodeAt(0)==event.keyCode){
				return true;
			}
		break;
		case "none":
			if (!event.ctrlKey && !event.altKey && !event.shiftKey && key.toLowerCase().charCodeAt(0)==event.keyCode){
				return true;
			}
			if (!event.ctrlKey && !event.altKey && !event.shiftKey && key.toUpperCase().charCodeAt(0)==event.keyCode){
				return true;
			}
		break;
		case "shift":
			if (event.shiftKey && key.toLowerCase().charCodeAt(0)==event.keyCode){
				return true;
			}
			if (event.shiftKey && key.toUpperCase().charCodeAt(0)==event.keyCode){
				return true;
			}
		break;
		default :
		break;
	}
	return false;
}
//处理获取焦点事件，使本域处于全部选中状态
function initFocusIn(srcElement) {
    if (srcElement==null)
     return;
	var tagName = srcElement.tagName.toLowerCase();
	if ((tagName == "input" && (srcElement.type == "text" || srcElement.type == "password")) || tagName == "textarrea") {
		srcElement.select();
	}
}

//处理丢失焦点事件，并检查域中的值是否符合设定的数据验证类型
function checkType(srcElement) {
	try {
		var tagName = srcElement.tagName.toLowerCase();
		
		var location = srcElement.getAttribute("inputname");
		if (location == null || location == "") {
			location = "光标所在处";
		}
		if ((tagName == "input" && srcElement.type == "text")
			|| tagName == "textarea") {
			var len = srcElement.getAttribute("maxlength");
			if (len != null && !isNaN(parseInt(len)) && parseInt(len) > 0) {
				if (srcElement.value.getByte() > parseInt(len)) {
					alert(location + "输入的内容超过限定的最大长度！\n最大长度为：" + len + "字节\n输入内容的长度为：" + srcElement.value.getByte() + "字节！");
					srcElement.focus();
					return false;
				}
			}
		}
		if (tagName == "input" && srcElement.type == "text") {
			var checktype = srcElement.getAttribute("checktype");
			if (checktype == null 
				|| checktype.trim() == "" 
				|| checktype.trim().toLowerCase() == "none" 
				|| srcElement.value == null 
				|| srcElement.value == "") {
				return true;
			}
			switch (checktype.toLowerCase()) {
				case "number" : 
					if (!srcElement.value.isNumber()) {
						srcElement.focus();
						alert(location + "只能输入半角型数字！");
						srcElement.focus();
						return false;
					}
					break;
				case "int" : 
					if (!srcElement.value.isInt()) {
						srcElement.focus();
						alert(location + "只能输入半角型整数！");
						srcElement.focus();
						return false;
					}
					break;
				case "float" : 
					if (!srcElement.value.isFloat()) {
						srcElement.focus();
						alert(location + "只能输入半角型浮点数！");
						srcElement.focus();
						return false;
					}
					break;
				case "plus" : 
					if (!srcElement.value.isPlus()) {
						srcElement.focus();
						alert(location + "只能输入半角型正数！");
						srcElement.focus();
						return false;
					}
					break;
				case "plusint" : 
					if (!srcElement.value.isPlusInt()) {
						srcElement.focus();
						alert(location + "只能输入半角型正整数！");
						srcElement.focus();
						return false;
					}
					break;
				case "plusfloat" : 
					if (!srcElement.value.isPlusFloat()) {
						srcElement.focus();
						alert(location + "只能输入半角型正浮点数！");
						srcElement.focus();
						return false;
					}
					break;
				case "minus" : 
					if (!srcElement.value.isMinus()) {
						srcElement.focus();
						alert(location + "只能输入半角型负数！");
						srcElement.focus();
						return false;
					}
					break;
				case "minusint" : 
					if (!srcElement.value.isMinusInt()) {
						srcElement.focus();
						alert(location + "只能输入半角型负整数！");
						srcElement.focus();
						return false;
					}
					break;
				case "minusfloat" : 
					if (!srcElement.value.isMinusFloat()) {
						srcElement.focus();
						alert(location + "只能输入半角型负浮点数！");
						srcElement.focus();
						return false;
					}
					break;
				case "wordchar" :
					if (!srcElement.value.isLeastCharSet()) {
						srcElement.focus();
						alert(location + "只能输入半角型大小写字母、数字和下划线，不包括空格！");
						srcElement.focus();
						return false;
					}
					break;
				case "zip" :
					if (!srcElement.value.isZip()) {
						srcElement.focus();
						alert(location + "输入的字符串不符合邮政编码标准！");
						srcElement.focus();
						return false;
					}
					break;
				case "mobile" :
					if (!srcElement.value.isMobileTelephone()) {
						srcElement.focus();
						alert(location + "输入的字符串不符合手机号标准！\n标准的手机号格式为：13xxxxxxxxx");
						srcElement.focus();
						return false;
					}
					break;
				case "telephone" :
					if (!srcElement.value.isTelephone()) {
						srcElement.focus();
						alert(location + "输入的字符串不符合电话号码标准！标准格式为：(xxxx)xxxxxxxx或者xxxx-xxxxxxxx");
						srcElement.focus();
						return false;
					}
					break;
				case "fax" :
					if (!srcElement.value.isTelephone()) {
						srcElement.focus();
						alert(location + "输入的字符串不符合传真号码标准！标准格式为：(xxxx)xxxxxxxx或者xxxx-xxxxxxxx");
						srcElement.focus();
						return false;
					}
					break;
				case "email" :
					if (!srcElement.value.isEmail()) {
						srcElement.focus();
						alert(location + "输入的字符串不符合电子邮件标准！\n标准的电子邮件格式为：xx@xx.xx");
						srcElement.focus();
						return false;
					}
					break;
				case "date" :
					if (!srcElement.value.isDate()) {
						srcElement.focus();
						alert(location + "输入的字符串不符合日期格式标准！\n标准的日期格式为：2004-04-23");
						srcElement.focus();
						return false;
					}
					break;
				case "time" :
					if (!srcElement.value.isTime()) {
						srcElement.focus();
						alert(location + "输入的字符串不符合时间格式标准！\n标准的时间格式为：09:30:50");
						srcElement.focus();
						return false;
					}
					break;
				case "datetime" :
					if (!srcElement.value.isDateTime()) {
						srcElement.focus();
						alert(location + "输入的字符串不符合日期时间格式标准！\n标准的日期时间格式为：2004-04-23 09:30:50");
						srcElement.focus();
						return false;
					}
					break;
				case "nospecialchar" :
					if (srcElement.value.hasSpecialChar()) {
						srcElement.focus();
						alert(location + "输入的字符串中包含特殊字符！\n以下字符集为特殊字符集：\n" + specialChars.source);
						srcElement.focus();
						return false;
					}
					break;
				default :
					break;
			}
		}
		return true;
	}
	catch (e) {
		alert(e);
		return false;
	}
	return true;
}

//对页面上的各种输入域做初始化处理
function initControl(myevent) {
	var selEls = window.document.getElementsByTagName("select");
	for (var i = 0; i < selEls.length; i++) {
		/*if (selEls[i].className == 'inputselect') {
			selEls[i].outerHTML = '<div class="inputtext">' + selEls[i].outerHTML + '</div>';
		}
		else if (selEls[i].className == 'inputselectshort') {
			selEls[i].outerHTML = '<div class="inputtextshort">' + selEls[i].outerHTML + '</div>';
		}*/
		if (selEls[i].disabled) {
			selEls[i].className = selEls[i].className + "readonly";
		}
	}
	var writeEls = window.document.getElementsByTagName("input");
	for (var i = 0; i < writeEls.length; i++) {
		if (writeEls[i].readOnly || writeEls[i].disabled) {
			writeEls[i].className = writeEls[i].className + "readonly";
		}
		if (writeEls[i].type == "button"){
			keyLoad(writeEls[i]);
		}
	}
	var writeEls = window.document.getElementsByTagName("textarea");
	for (var i = 0; i < writeEls.length; i++) {
		if (writeEls[i].readOnly || writeEls[i].disabled) {
			writeEls[i].className = writeEls[i].className + "readonly";
		}
	}
	var writeEls = window.document.getElementsByTagName("button");
	for (var i = 0; i < writeEls.length; i++) {
		keyLoad(writeEls[i]);
	}
}
function keyLoad(els){
	keyArray[keyArray.length] = els;
}
function gwpEvent() {
	this.onclick = "";
	this.onkeydown = "";
	this.onfocusin = "";
	this.onfocusout = "";
	this.onload = "";
	this.run = function (exp) {
		if (exp != null && exp.trim() != "") {
			eval(exp);
		}
	};
}
var gwpEventor = new gwpEvent();

/*
//页面加载时屏蔽右键菜单
document.oncontextmenu = function() {
	return closeContextMenu(window.event);
};
//页面加载时屏蔽shift加左键点击链接时从新窗口打开
document.onclick = function() {
	gwpEventor.run(gwpEventor.onclick);
	return closeNewWindow(window.event);
};*/

document.onkeydown = function () {
	gwpEventor.run(gwpEventor.onkeydown);
	checkKeyDown(window.event);
};

document.onfocusin = function () {
	gwpEventor.run(gwpEventor.onfocusin);
	initFocusIn(window.event.srcElement);
};

document.onfocusout = function () {
	gwpEventor.run(gwpEventor.onfocusout);
	//checkType(window.event.srcElement);
};

window.onload = function() {
	initControl(window.event);
	gwpEventor.run(gwpEventor.onload);
};
function initTab(str,index){
if(str==null || str=="") return
var tabobj=document.all("tab")
if(tabobj==null) return;
var row = tabobj.rows[0];
var arr = str.split(",");
for (i = 0; i < arr.length; i++){
var otd = row.insertCell();
otd.innerText = arr[i];
if(index==i)
otd.className='taboncss';
else
otd.className='taboffcss';
}
doSelectTab(index);
}
function doTabClick(){
var obj=window.event.srcElement;
if(obj==null || obj.tagName!='TD') return;
var index=obj.cellIndex;
var tabobj=document.all("tab")
if(tabobj==null) return;
for(var i=0;i<tabobj.rows[0].cells.length;i++){
tabobj.rows[0].cells[i].className='taboffcss';}
tabobj.rows[0].cells[index].className='taboncss';
doSelectTab(index);
}
function doSelectTab(index){
var tabobj=document.all("tab")
if(tabobj==null) return;
var divAll=tabobj.nextSibling;
if(divAll==null) return;
for(var j=0;j<divAll.children.length;j++)
divAll.children[j].style.display='none';
divAll.children[index].style.display='block';
}