// menuClick.js

// newFunction
function gwpOpen(url) {
	//alert(url);
	window.parent.parent.frames("main").location = url;
}

function gwpTopOpen(url) {
	window.top.location.href = url;
}

var stickStatus = false;
function showLeftTree(divEl, show) {
	if (stickStatus == false) {
		if (show) {
			divEl.className = "divtreeshow";
		}
		else {
			if(window.event.clientX>140){
				divEl.className = "divtree";
			}
		}
	}
}

function stickLeftTree(divEl, stickImg, stickSrc, stickupSrc) {
	stickStatus = !stickStatus;
	setCookie("stickStatus", (stickStatus ? "true" : "false"), null, "/");
	if (stickStatus) {
		divEl.className = "divtreestick";
		stickImg.src = stickSrc;
		stickImg.alt = '活动菜单';
		document.getElementById("seprator").style.display = "none";
	}
	else {
		divEl.className = "divtreeshow";
		stickImg.src = stickupSrc;
		stickImg.alt = '固定菜单';
		document.getElementById("seprator").style.display = "";
	}
}
