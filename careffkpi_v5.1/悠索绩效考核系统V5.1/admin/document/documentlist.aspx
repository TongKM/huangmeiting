﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="documentlist.aspx.cs" Inherits="BasicWeb.admin.document.documentlist" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
<title>文件列表</title>

<script language="javascript" src="js/0451skyMyDocument.js"></script>
<link href="../style/default/style.css" rel="stylesheet" type="text/css" />
 <script language="javascript" src="/admin/js/System.js" type="text/javascript"></script>

</HEAD>
<body>
<form id="Form1" method="post" runat="server">
  <div id="tbMain">
    <div class="divTbg">
      <div class="divTbgL"></div>
      <div class="divTbgInfo">
        <div class="divTbgTitle"><a href="#" onClick="meoprthis();">
          <div id="mydocumentid" style="COLOR:#000000" title="关闭左侧目录菜单！">← 我的文档</div>
          </a></div>
      </div>
    </div>
    <div class="divTbgF"></div>
    <div id="mypanel">
      <div class="divInfo" id="ddiv1">
        <div class="divInfoContext">
          <table width="100%" border="0" align="center" cellpadding="5" cellspacing="1" class="tablist">
            <tr class="tablisttitle">
              <td width="100" height="25" align="center" valign="middle" class="tablisttitle">当前路径名称</td>
              <td align="left" valign="middle" bgcolor="#FFFFFF" class="tablisttrleft"><asp:TextBox ID="TxtPath" ReadOnly="true" runat="server" Width="300px" CssClass="textblur" onfocus="this.className='textfocus'"
																			onblur="this.className='textblur'"></asp:TextBox>              </td>
            </tr>
            <tr class="MeNewTDLine">
              <td align="center" valign="middle" bgcolor="#ffffff" class="tablisttitle">建立子目录：</td>
              <td align="left" valign="middle" bgcolor="#ffffff" class="tablisttrleft"><asp:TextBox ID="TxtCreate" runat="server" ToolTip="建立子目录的名称，3-16个字母数字下划线组合，不区分大小写！" Width="200px"
																			CssClass="textblur" onfocus="this.className='textfocus'" onblur="this.className='textblur'"></asp:TextBox>
                <asp:Button ID="BtnCreate" runat="server" Text="建立目录" class="SkyButtonFocus" OnClick="BtnCreate_Click"></asp:Button></td>
            </tr>
            <tr class="MeNewTDLine">
              <td align="center" valign="middle" bgcolor="#ffffff" class="tablisttitle">其它相关操作</td>
              <td align="left" valign="middle" bgcolor="#ffffff" class="tablisttrleft"><asp:Button ID="btnUpload" runat="server" Text="文件上传" ToolTip="多文件上传" class="SkyButtonFocus" OnClick="btnUpload_Click"></asp:Button>
                <asp:Button ID="Btndel" runat="server" Text="删除目录" class="SkyButtonFocus" OnClick="Btndel_Click"></asp:Button>
                <asp:TextBox ID="TxtFileType" runat="server" Width="120px" Visible="False"></asp:TextBox>              </td>
            </tr>
          </table>
        </div>
      </div>
    </div>
    
  </div>
  <div class="divInfo">
   <div class="divInfoblack">
                <div class="divInfoTopList">
                    <div class="listLeft">
                        <div class="navsearchtitle">
                            文件列表</div>
                    </div>
                </div>
            </div>
    <div class="divInfoContext">
      <div style="font: 2px; line-height: 2px;"> &nbsp;</div>
      <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" BorderStyle="None"
                                                                        CellPadding="0" CellSpacing="1" GridLines="None" Width="100%" CssClass="tablist" OnRowDataBound="GridView1_RowDataBound" OnRowCommand="GridView1_RowCommand" DataKeyNames="Fsrc">
        <RowStyle CssClass="tablisttr" HorizontalAlign="Center" />
        <HeaderStyle HorizontalAlign="Center" CssClass="tablisttitle"></HeaderStyle>
        <Columns>
        <asp:BoundField HeaderText="序号" />
        <asp:BoundField DataField="FName" HeaderText="名称" HtmlEncode="False" >
          <ItemStyle HorizontalAlign="Left" />
        </asp:BoundField>
        <asp:BoundField DataField="FType" HeaderText="类型" />
        <asp:BoundField DataField="FSize" HeaderText="大小" />
        <asp:BoundField DataField="Fdate" HeaderText="时间" />
        <asp:HyperLinkField DataNavigateUrlFields="Fsrc" DataNavigateUrlFormatString="{0}"
                                                                        HeaderText="查看" Text="[查看]" Target="_blank" />
        <asp:ButtonField HeaderText="更名" Text="[更名]" CommandName="ReName" />
        <asp:ButtonField HeaderText="应用" Text="[应用]" CommandName="Used" />
        <asp:ButtonField HeaderText="删除" Text="[删除]" CommandName="Dele" />
        </Columns>
      </asp:GridView>
      <div class="divInfoRight">
        <asp:Literal ID="Literalall" runat="server"></asp:Literal>
      </div>
    </div>
  </div>
  <asp:literal id="Literal1" runat="server"></asp:literal>
  <asp:Literal id="LiteralCss" runat="server"></asp:Literal>
</form>
</body>
</HTML>
