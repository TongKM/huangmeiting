﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="documentbigmul.aspx.cs" Inherits="BasicWeb.admin.document.documentbigmul" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>多文件上传</title>
<link href="../style/default/style.css" rel="stylesheet" type="text/css" />

    <script language="javascript" src="/admin/js/System.js" type="text/javascript"></script>

<script language="javascript" src="js/0451skyMyDocument.js"></script>
<asp:Literal ID="LiteralCss" runat="server"></asp:Literal>
</head>
<body>
<form id="Form1" method="post" enctype="multipart/form-data" runat="server">
  <div id="tbMain">
    <div class="divTbg">
      <div class="divTbgL"></div>
      <div class="divTbgInfo">
        <div class="divTbgTitle"><a href="#" onClick="meoprthis();">
          <div id="mydocumentid" style="color: #000000" title="关闭左侧目录菜单！"> ← 多文件上传</div>
          </a></div>
      </div>
    </div>
    <div class="divTbgF"></div>
    <div id="mypanel">
      <div class="divInfo" id="ddiv1">
        <div class="divInfoContext">
          <table class="tablist" cellspacing="1" cellpadding="5" width="100%" align="center"
                                                        border="0">
            <tr class="SkyTDLine">
              <td width="100" align="center" nowrap class="tablisttitle"> 文件选择 </td>
              <td width="100%" class="tablisttrleft"><table class="SkyTDLine" cellspacing="0" cellpadding="0" width="100%" border="0">
                  <tr class="MeNewTDLine">
                    <td class="tablisttrleft"> 请选择文件 </td>
                    <td class="tablisttrleft"><asp:FileUpload ID="File1" runat="server" />
                      <asp:CheckBox ID="ChkRName" runat="server" Text="自动更名" ToolTip="自动更改文件名称为时间名，如果不选择则不更改文件名，如果您上传的文件有中文，则也更改成时间文件名。如果存在文件，则不提示覆盖！"
                                                                    Checked="True"></asp:CheckBox>                    </td>
                  </tr>
                </table></td>
            </tr>
            <tr class="MeNewTDLine">
              <td align="center" nowrap class="tablisttitle"> 提示信息 </td>
              <td class="tablisttrleft">如果不自动更名，存在文件则自动更换. </td>
            </tr>
          </table>
        </div>
      </div>
    </div>
    <div class="divSave">
      <asp:Button ID="BtnUpBig" runat="server" Text="文件上传" ToolTip="确定文件上传" class="SkyButtonFocus" OnClick="BtnUpBig_Click"></asp:Button>
    </div>
  </div>
  <asp:Literal ID="LitScript" runat="server" EnableViewState="False"></asp:Literal>
</form>
</body>
</html>
