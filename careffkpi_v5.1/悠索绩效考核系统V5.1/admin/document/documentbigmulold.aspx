﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="documentbigmulold.aspx.cs" Inherits="BasicWeb.admin.document.documentbigmulold" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>多文件上传</title>
    <link href="../style/default/skin.css" rel="stylesheet" type="text/css" />

    <script language="javascript" src="js/0451skyMyDocument.js"></script>

    <asp:literal id="LiteralCss" runat="server"></asp:literal>
    
</head>
<body>
    <form id="Form1" method="post" enctype="multipart/form-data" runat="server">
        <table width="100%" height="100" border="0" cellpadding="0" cellspacing="0" class="tablemain">
            <tr>
                <td>
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tableT">
                        <tr>
                            <td class="tableTleft">&nbsp;
                                </td>
                            <td class="tableTcenter">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="46%" valign="middle">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="5%">
                                                        <div align="center"></div>
                                                    </td>
                                                    <td width="95%" class="tableTBig">
                                                        <a href="#" onClick="meoprthis();">
                                                            <div id="mydocumentid" style="color: #000000" title="关闭左侧目录菜单！">
                                                                ← 多文件上传</div>
                                                        </a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td width="54%">&nbsp;
                                            </td>
                                    </tr>
                                </table>
                            </td>
                            <td class="tableTright">&nbsp;
                                </td>
                        </tr>
                    </table>
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tableC">
                        <tr>
                            <td class="tableCleft">&nbsp;
                                </td>
                            <td class="tableCcenter">
                                <table class="Menu2Table" cellspacing="0" cellpadding="0" width="100%" border="0">
                                    <tr>
                                        <td class="Menu2Td">
                                            <table class="PallNow" cellspacing="1" cellpadding="0" width="100%" align="center"
                                                border="0">
                                                <tr>
                                                    <td class="SkyTDLine" align="left">
                                                        <a href="DocumentReName.aspx?path=" target="_self"><font color="#ff0000">〖返回文档〗</font></a></td>
                                                </tr>
                                            </table>
                                            <table class="PallNow" cellspacing="0" cellpadding="0" width="100%" align="center"
                                                border="0">
                                                <tr>
                                                    <td class="Pinfo">
                                                        <table class="PallNow" cellspacing="1" cellpadding="0" width="100%" align="center"
                                                            border="0">
                                                            <tr class="MeNewTDLine">
                                                                <td nowrap align="center" width="100">
                                                                    文件选择</td>
                                                                <td width="100%">
                                                                    <table class="SkyTDLine" cellspacing="0" cellpadding="0" width="100%" border="0">
                                                                        <tr class="MeNewTDLine">
                                                                            <td class="SkyTDLine">
                                                                                文件1</td>
                                                                            <td class="SkyTDLine">
                                                                                <input class="textblur" id="File1" onBlur="this.className='textblur'" title="文件上传接口选择"
                                                                                    style="width: 500px; height: 19px" onFocus="this.className='textfocus'" type="file"
                                                                                    size="20" name="File1" runat="server">
                                                                                <asp:TextBox ID="TxtName1" runat="server" Width="0px"></asp:TextBox></td>
                                                                        </tr>
                                                                        <tr class="MeNewTDLine">
                                                                            <td class="SkyTDLine">
                                                                                文件2</td>
                                                                            <td class="SkyTDLine">
                                                                                <input class="textblur" id="File2" onBlur="this.className='textblur'" title="文件上传接口选择"
                                                                                    style="width: 500px; height: 19px" onFocus="this.className='textfocus'" type="file"
                                                                                    size="20" name="File2" runat="server">
                                                                                <asp:TextBox ID="TxtName2" runat="server" Width="0px"></asp:TextBox></td>
                                                                        </tr>
                                                                        <tr class="MeNewTDLine">
                                                                            <td class="SkyTDLine">
                                                                                文件3</td>
                                                                            <td class="SkyTDLine">
                                                                                <input class="textblur" id="File3" onBlur="this.className='textblur'" title="文件上传接口选择"
                                                                                    style="width: 500px; height: 19px" onFocus="this.className='textfocus'" type="file"
                                                                                    size="20" name="3" runat="server">
                                                                                <asp:TextBox ID="TxtName3" runat="server" Width="0px"></asp:TextBox></td>
                                                                        </tr>
                                                                        <tr class="MeNewTDLine">
                                                                            <td class="SkyTDLine">
                                                                                文件4</td>
                                                                            <td class="SkyTDLine">
                                                                                <input class="textblur" id="File4" onBlur="this.className='textblur'" title="文件上传接口选择"
                                                                                    style="width: 500px; height: 19px" onFocus="this.className='textfocus'" type="file"
                                                                                    size="20" name="File4" runat="server">
                                                                                <asp:TextBox ID="TxtName4" runat="server" Width="0px"></asp:TextBox></td>
                                                                        </tr>
                                                                        <tr class="MeNewTDLine">
                                                                            <td class="SkyTDLine">
                                                                                文件5</td>
                                                                            <td class="SkyTDLine">
                                                                                <input class="textblur" id="File5" onBlur="this.className='textblur'" title="文件上传接口选择"
                                                                                    style="width: 500px; height: 19px" onFocus="this.className='textfocus'" type="file"
                                                                                    size="20" name="5" runat="server">
                                                                                <asp:TextBox ID="TxtName5" runat="server" Width="0px"></asp:TextBox></td>
                                                                        </tr>
                                                                        <tr class="MeNewTDLine">
                                                                            <td class="SkyTDLine">
                                                                                文件6</td>
                                                                            <td class="SkyTDLine">
                                                                                <input class="textblur" id="File6" onBlur="this.className='textblur'" title="文件上传接口选择"
                                                                                    style="width: 500px; height: 19px" onFocus="this.className='textfocus'" type="file"
                                                                                    size="20" name="File6" runat="server">
                                                                                <asp:TextBox ID="TxtName6" runat="server" Width="0px"></asp:TextBox></td>
                                                                        </tr>
                                                                        <tr class="MeNewTDLine">
                                                                            <td class="SkyTDLine">
                                                                                文件7</td>
                                                                            <td class="SkyTDLine">
                                                                                <input class="textblur" id="File7" onBlur="this.className='textblur'" title="文件上传接口选择"
                                                                                    style="width: 500px; height: 19px" onFocus="this.className='textfocus'" type="file"
                                                                                    size="20" name="File7" runat="server">
                                                                                <asp:TextBox ID="TxtName7" runat="server" Width="0px"></asp:TextBox></td>
                                                                        </tr>
                                                                        <tr class="MeNewTDLine">
                                                                            <td class="SkyTDLine">
                                                                                文件8</td>
                                                                            <td class="SkyTDLine">
                                                                                <input class="textblur" id="File8" onBlur="this.className='textblur'" title="文件上传接口选择"
                                                                                    style="width: 500px; height: 19px" onFocus="this.className='textfocus'" type="file"
                                                                                    size="20" name="File8" runat="server">
                                                                                <asp:TextBox ID="TxtName8" runat="server" Width="0px"></asp:TextBox></td>
                                                                        </tr>
                                                                        <tr class="MeNewTDLine">
                                                                            <td class="SkyTDLine">
                                                                                文件9</td>
                                                                            <td class="SkyTDLine">
                                                                                <input class="textblur" id="File9" onBlur="this.className='textblur'" title="文件上传接口选择"
                                                                                    style="width: 500px; height: 19px" onFocus="this.className='textfocus'" type="file"
                                                                                    size="20" name="File9" runat="server">
                                                                                <asp:TextBox ID="TxtName9" runat="server" Width="0px"></asp:TextBox></td>
                                                                        </tr>
                                                                        <tr class="MeNewTDLine">
                                                                            <td class="SkyTDLine">
                                                                                文件10</td>
                                                                            <td class="SkyTDLine">
                                                                                <input class="textblur" id="File10" onBlur="this.className='textblur'" title="文件上传接口选择"
                                                                                    style="width: 500px; height: 19px" onFocus="this.className='textfocus'" type="file"
                                                                                    size="20" name="File10" runat="server">
                                                                                <asp:TextBox ID="TxtName10" runat="server" Width="0px"></asp:TextBox></td>
                                                                        </tr>
                                                                    </table>
                                                                    <asp:CheckBox ID="ChkRName" runat="server" Text="自动更名" ToolTip="自动更改文件名称为时间名，如果不选择则不更改文件名，如果您上传的文件有中文，则也更改成时间文件名。如果存在文件，则不提示覆盖！" Checked="True" Enabled="False">
                                                                    </asp:CheckBox>
                                                                    <asp:Button ID="BtnUpBig" runat="server" Text="文件上传" ToolTip="确定文件上传" class="SkyButtonBlur"
                                                                        onMouseOut="this.className='SkyButtonBlur'" onMouseOver="this.className='SkyButtonFocus'"
                                                                        OnClick="BtnUpBig_Click" OnUnload="BtnUpBig_Unload"></asp:Button>
                                                                    <asp:TextBox ID="TxtName" runat="server" Width="0px"></asp:TextBox>
                                                                    一次可以选择10个文件上传，如果不到10个，可以只选几个</td>
                                                            </tr>
                                                            <tr class="MeNewTDLine">
                                                                <td align="center" nowrap>
                                                                    提示信息</td>
                                                                <td>
                                                                    <asp:ListBox ID="ListBox1" runat="server" Width="542px" Height="72px" CssClass="textblur">
                                                                    </asp:ListBox></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td class="tableCright">&nbsp;
                                </td>
                        </tr>
                    </table>
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tableF">
                        <tr>
                            <td class="tableFleft">&nbsp;
                                </td>
                            <td class="tableFcenter">&nbsp;
                                </td>
                            <td class="tableFright">&nbsp;
                                </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <asp:Literal ID="LitScript" runat="server" EnableViewState="False"></asp:Literal>
    </form>
</body>
</html>
