﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="documentleft.aspx.cs" Inherits="BasicWeb.admin.document.documentleft" %>

<html>
<head>
<title>目录导航</title>



<script language="javascript" src="js/0451skyMyDocument.js"></script>
<link href="../style/default/style.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="/admin/js/System.js" type="text/javascript"></script>
<link rel="StyleSheet" href="/admin/js/tree/dtree.css" type="text/css" />
<script type="text/javascript" src="/admin/js/tree/dtree.js"></script>


<script src="../js/jquery/jquery.js" type="text/javascript"></script>

 <script language="javascript" type="text/javascript" >
     function AutoHeight() {
         var he = document.body.clientHeight - 40;
         var s = document.getElementById("ScroLeft");
         s.style.height = he;
     }
     $(window).resize(function() {
         AutoHeight();
     });

     $(document).ready(function() {
         AutoHeight();
     });
</script>

<SCRIPT language="javascript">
	stickStatus = false;
	dTree.prototype.icon = {
		root				: 'images/dtree/base.gif',
		folder			: 'images/dtree/folder.gif',
		folderOpen	: 'images/dtree/folderopen.gif',
		node				: 'images/dtree/page.gif',
		empty				: 'images/dtree/empty.gif',
		line				: 'images/dtree/line.gif',
		join				: 'images/dtree/join.gif',
		joinBottom	: 'images/dtree/joinbottom.gif',
		plus				: 'images/dtree/plus.gif',
		plusBottom	: 'images/dtree/plusbottom.gif',
		minus				: 'images/dtree/minus.gif',
		minusBottom	: 'images/dtree/minusbottom.gif',
		nlPlus			: 'images/dtree/nolines_plus.gif',
		nlMinus			: 'images/dtree/nolines_minus.gif'
	};

				</SCRIPT>
</head>
<body>
<form id="Form1" method="post" runat="server">
  <div id="tbMain">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" height="100%">
    <tr>
      <td width="180" valign="top"><div class="divTbg">
          <div class="divTbgL"></div>
          <div class="divTbgInfo">
            <div class="divTbgTitle">目录列表</div>
          </div>
        </div>
        <div class="divTbgF"></div>
        <div class="divSearchBoth">
              <div class="divInfo">
         <div class="divAutoHeight" id="ScroLeft"><%=TreeHTML %></div>
            
            
          </div>
        </div></td>
      <td  valign="top" ><iframe src="" width="100%" height="100%" id="frameright" name="frameright" frameborder="0"
                    scrolling="auto"></iframe></td>
    </tr>
  </table>
  </div>
</form>

</body>
</html>
