﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ussoft.aspx.cs" Inherits="BasicWeb.admin.ussoft" %>


<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>软件信息</title>
    <asp:Literal ID="litregscript" runat="server" Text=""></asp:Literal>
    <link href="style/default/style.css" rel="stylesheet" type="text/css" />

    <script language="javascript" src="/admin/js/System.js" type="text/javascript"></script>


</head>
<body>
    <form id="form1" runat="server">
    
    <div id="tbMain">
        <div class="divTbg">
            <div class="divTbgL">
            </div>
            <div class="divTbgInfo">
                <div class="divTbgTitle">
                    软件信息</div>
            </div>
        </div>
        <div class="divTbgF">
        </div>
        
        <div id="mypanel">
            <div class="divInfoContext">
                
                <div class="divInfo">
                  

                        <table width="100%" border="0" cellpadding="0" cellspacing="1" class="tablist" id="mainlist" onMouseOver="changeto()" onmouseout="changeback()">
                        <tr class="tablisttr">
                            <td width="100" align="center" class="tablisttitle">
                                软件名称
                            </td>
                            <td class="tabmanagertr">
                                <asp:Literal ID="LiteralSoftName" runat="server" Text="悠索绩效考核系统"></asp:Literal>
                            </td>
                        </tr>
                        <tr class="tablisttr">
                            <td align="center" class="tablisttitle">
                                软件版本
                            </td>
                            <td class="tabmanagertr">
                                <asp:Literal ID="LiteralSoftAssembly" runat="server"></asp:Literal>
                            </td>
                        </tr>
                       <tr class="tablisttr">
                            <td align="center" class="tablisttitle">
                                软件版权
                            </td>
                            <td class="tabmanagertr">
                                <asp:Literal ID="LiteralCompany" runat="server" Text="黑龙江悠索工作室"></asp:Literal>
                                保留所有权利
                            </td>
                        </tr>
                       <tr class="tablisttr">
                            <td align="center" class="tablisttitle">
                                说明信息
                            </td>
                            <td class="tabmanagertr">
                                <asp:Literal ID="LiteralDescription" runat="server"></asp:Literal>
                            </td>
                        </tr>
                        <tr class="tablisttr">
                            <td align="center" class="tablisttitle">
                                授权类型
                            </td>
                            <td class="tabmanagertr">
                                <asp:Literal ID="LiteralUserType" runat="server"></asp:Literal>
                            </td>
                        </tr>
                        <tr class="tablisttr">
                            <td align="center" class="tablisttitle">
                                                                授权单位
                            </td>
                            <td class="tabmanagertr">
                                <asp:TextBox ID="txtWebName" runat="server" CssClass="1" Width="400px" Enabled="False"></asp:TextBox>
                            </td>
                        </tr>
                       <tr class="tablisttr">
                            <td align="center" class="tablisttitle">
                                <span class="SkyTDLine">在线人数</span>
                            </td>
                            <td class="tabmanagertr">
                                <asp:TextBox ID="txtWebOnline" runat="server" CssClass="1" Width="400px" Enabled="False"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                   
                   
                     <div class="divSave">

                            <input name="btnClose" type="button" value="关闭(C)" onclick="window.close()" class="SkyButtonFocus" />
                    
                    </div>
                   
                </div>
            </div>
        </div>
    </div>

    </form>
</body>
</html>
