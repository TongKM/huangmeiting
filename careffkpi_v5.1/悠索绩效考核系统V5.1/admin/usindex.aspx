﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="usindex.aspx.cs" Inherits="BasicWeb.admin.usindex" %>
<!DOCTYPE HTML>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>后台首页</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"><style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
.topnav{
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #D7E4EA;
	height: 25px;
	font-size: 13px;
	line-height: 25px;
	background-color: #EDF6FA;
	text-indent: 20px;
	font-family: "微软雅黑";

}
-->
</style>
</head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="tdheight">
  <tr>
    <td align="left" class="topnav">位置：系统 > 首页</td>
  </tr>
  <tr>
    <td align="left" valign="bottom">
    
    
    <%--发布的时候，除非是特殊用户，否则关闭此功能，或者修改功能--%>
    <% =getInfo()%>
<%--<script type="text/javascript" >
    window.onload = function() {
        $("#tdheight").height( $(document).height());
        var pop = new Pop("#",
			"#",
			"<a href='/admin/b2other/nnewsviewlist.aspx' target='_self'>您有【2】条未读公告消息，点击查看！</a><br><a href='/admin/b2other/nnewsviewlist.aspx' target='_self'>您有【2】条未读立项消息，点击查看！</a>");
    }
</script>--%>
<script type="text/javascript" src="/admin/js/pop/jquery.min.js"></script>
<script type="text/javascript" src="/admin/js/pop/pop.js"></script>
<div id="pop" style="display:none;">
	<style type="text/css">
	*{margin:0;padding:0;}
	#pop{background:#fff;width:260px;border:1px solid #e0e0e0;font-size:12px;position: fixed;right:10px;bottom:10px;}
	#popHead{line-height:32px;background:#f6f0f3;border-bottom:1px solid #e0e0e0;position:relative;font-size:12px;padding:0 0 0 10px;}
	#popHead h2{font-size:14px;color:#666;line-height:32px;height:32px;}
	#popHead #popClose{position:absolute;right:10px;top:1px;}
	#popHead a#popClose:hover{color:#f00;cursor:pointer;}
	#popContent{padding:5px 10px;}
	#popTitle a{line-height:24px;font-size:14px;font-family:'微软雅黑';color:#333;font-weight:bold;text-decoration:none;}
	#popTitle a:hover{color:#f60;}
	#popIntro{text-indent:0px;line-height:160%;margin:5px 0;color:#666;text-decoration: none;}
	#popMore{text-align:right;border-top:1px dotted #ccc;line-height:24px;margin:8px 0 0 0;}
	#popIntro a{color:#f60;}
	#popIntro a:hover{color:#f00;}
	</style>
	<div id="popHead">
	<a id="popClose" title="关闭">关闭</a>
	<h2>温馨提示</h2>
	</div>
	<div id="popContent">
	<dl>
		<dt id="popTitle" style="display:none"><a href="#" target="_self">这里是参数</a></dt>
		<dd id="popIntro">这里是内容简</dd>
	</dl>
	<p id="popMore" style="display:none"><a href="#/" target="_self">查看 »</a></p>
	</div>
</div>
    
    <img src="style/default/images/index.jpg"></td>
  </tr>
</table>
</body>
</html>
