﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="userplanmanagerwc.aspx.cs" Inherits="BasicWeb.admin.advanced.userplanmanagerwc" %>


<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>计划管理</title>
    <asp:Literal ID="litregscript" runat="server" Text=""></asp:Literal>
    <link href="../style/default/style.css" rel="stylesheet" type="text/css" />

    <script language="javascript" src="/admin/js/System.js" type="text/javascript"></script>
 <script language="javascript" src="/admin/My97DatePicker/WdatePicker.js" type="text/jscript"></script>
 <script  language="javascript" src="/editor/xheditor.js" type="text/javascript" ></script>
<script language="javascript" type="text/javascript">
    function opJsTreeUser(txt, txtValue) {
        var para = "/admin/basic/usertreeselect.aspx?para=2&list=" + $("#" + txtValue).val();
        UsOpenDialogTree(para, txt, txtValue);
    }

    function jscheckAll() {
        var sValue = $("#drpPlanState").val();
        if (sValue == "1") {
            return window.confirm("当您提交计划状态为[提交审核]状态，您将无法修改和调整审核状态，是否要继续吗？");
        }
        return true;
    }
 </script>

</head>
<body onload="win_onLoad();">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div id="divLoading">
                <img src="/admin/style/default/images/loading.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div id="tbMain">
        <div class="divTbg">
            <div class="divTbgL">
            </div>
            <div class="divTbgInfo">
                <div class="divTbgTitle">
                    计划管理</div>
            </div>
        </div>
        <div class="divTbgF">
        </div>
        <div class="divTNavBG" id="topURL">
            <ul id="topURLul">
                <li class="divURLed" id="div1">
                    <div>
                        <img src="../style/default/images/mini_icons_046.gif" width="10" height="10" />
                        基本信息</div>
                </li>
            </ul>
        </div>
        <div id="mypanel">
            <div class="divInfoContext">
                <div class="divInfo">
                    <div class="divInfoblack">
                        <div class="divInfoTopList">
                            <div class="listLeft">
                                <div class="navtitle">
                                    计划管理</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="divInfo">
                  <table width="100%" border="0" cellpadding="0" cellspacing="1" class="tablist" id="mainlist" onMouseOver="changeto()" onmouseout="changeback()">
<tr class="tablisttr">
<td class="tablisttitle">计划编号</td><td class="tabmanagertr"><asp:TextBox ID="txtPlanID" runat="server" ToolTip="计划编号" Text="" CssClass="1"></asp:TextBox></td>
<td class="tablisttitle">计划名称</td><td class="tabmanagertr"><asp:TextBox ID="txtPlanName" runat="server" ToolTip="计划名称" Text="" CssClass="1" Enabled="false"></asp:TextBox></td>
</tr>
<tr class="tablisttr">
<td class="tablisttitle">计划状态</td><td class="tabmanagertr">
    <asp:DropDownList runat="server" ID="drpPlanState" ToolTip="计划状态"  Enabled="false">
                                    <asp:ListItem Value="PlanState" Text="codeno"></asp:ListItem>
                                </asp:DropDownList></td>
<td class="tablisttitle">计划类型</td><td class="tabmanagertr">
    <asp:DropDownList runat="server" ID="drpPlanType" ToolTip="计划类型"  Enabled="false">
                                    <asp:ListItem Value="PlanType" Text="codeno"></asp:ListItem>
                                </asp:DropDownList></td>
</tr>
<tr class="tablisttr">
<td class="tablisttitle">计划部门</td><td class="tabmanagertr">
    <asp:TextBox ID="txtDeptID" runat="server" ToolTip="计划部门" Text="" 
        Enabled="False"></asp:TextBox></td>
<td class="tablisttitle">计划人员</td><td class="tabmanagertr">
    <asp:TextBox ID="txtUserID" runat="server" ToolTip="计划人员" Text="" 
        Enabled="False"></asp:TextBox></td>
</tr>
<tr class="tablisttr">
<td class="tablisttitle">计划信息</td><td class="tabmanagertr" colspan="3">

<asp:Literal runat="server" ID="litPinfo"></asp:Literal>


</td>
</tr>
<tr class="tablisttr">
<td class="tablisttitle">附件地址</td><td class="tabmanagertr"><asp:TextBox ID="txtPAddr" runat="server" ToolTip="附件地址" Text=""></asp:TextBox></td>
<td class="tablisttitle">时间段</td><td class="tabmanagertr"> <asp:DropDownList runat="server" ID="drpDateID" ToolTip="时间段" Enabled="false"></asp:DropDownList></td>
</tr>
<tr class="tablisttr">
<td class="tablisttitle">完成情况</td><td class="tabmanagertr" colspan="3"><asp:TextBox ID="txtPUserInfo" runat="server" ToolTip="个人说明" Text=""  Width="100%" TextMode="MultiLine" Height="200px" class="xheditor {upLinkUrl:s_upLinkUrl,upLinkExt:s_upLinkExt,upImgUrl:s_upLinkUrl,upImgExt:s_upImgExt,upFlashUrl:s_upLinkUrl,upFlashExt:s_upFlashExt,upMediaUrl:s_upLinkUrl,upMediaExt:s_upMediaExt}"></asp:TextBox></td>
</tr>
<tr class="tablisttr">
<td class="tablisttitle">审核人员</td><td class="tabmanagertr" colspan="3">
<asp:TextBox ID="txtPSHUser" runat="server" ToolTip="审核人员" Text="" CssClass="1"></asp:TextBox>
<div style="display:none"><asp:TextBox ID="txtPSHUserID" runat="server" ToolTip="审核人员" Text=""></asp:TextBox></div>


</td>
</tr>
</table>
                    <div class="divSave">
                        <asp:Button ID="btnSubmit" runat="server" Text="保存(S)" 
                            CssClass="SkyButtonFocus" onclick="btnSubmit_Click" />
                        <asp:Button ID="btnExit" runat="server" Text="取消(C)" CssClass="SkyButtonFocus" 
                            onclick="btnSubmit_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </form>
</body>
</html>
