﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="innovatepackview.aspx.cs"
    Inherits="BasicWeb.admin.advanced.innovatepackview" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><%=entity1.sPackName %>-指标套包效果预览</title>
    <link href="../style/default/style.css" rel="stylesheet" type="text/css" />

    <script language="javascript" src="/admin/js/System.js" type="text/javascript"></script>

    <script language="javascript" src="/admin/My97DatePicker/WdatePicker.js" type="text/jscript"></script>

</head>
<body onload="win_onLoad();">
    <form id="Form1" method="post" runat="server">
    <div id="tbMain">
        <div class="divTbg">
            <div class="divTbgL">
            </div>
            <div class="divTbgInfo">
                <div class="divTbgTitle">
                    <%=entity1.sPackName %>-指标套包效果预览</div>
            </div>
        </div>
        <div class="divTbgF">
        </div>
        <div class="divInfo">
            <div class="divInfoblack">
                <div class="divInfoTopList">
                    <div class="listLeft">
                        <div class="navtitle">
                            指标套包效果预览</div>
                    </div>
                    <div class="listRight">
                        <ul>
                            <li class="navstyle1">
                                <input type="checkbox" name="chkselect" id="chkselect" onclick="skySelectCheckList();" />
                                <label for="chkselect">
                                    全选</label>
                            </li>
                            <li class="navstyle2">
                                <asp:LinkButton ID="LnkBAdd" runat="server" OnClick="SkyLnkBAMD_Click" Enabled="false">添加</asp:LinkButton>
                            </li>
                            <li class="navstyle3">
                                <asp:LinkButton ID="LnkBEdit" runat="server" OnClick="SkyLnkBAMD_Click" Enabled="false">编辑</asp:LinkButton>
                            </li>
                            <li class="navstyle4">
                                <asp:LinkButton ID="LnkBDelete" runat="server" OnClick="SkyLnkBAMD_Click" Enabled="false">删除</asp:LinkButton>
                            </li>
                            <li class="navstyle5">
                                <asp:LinkButton ID="LnkBExcel" runat="server" OnClick="SkyLnkBAMD_Click">导出</asp:LinkButton>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="divInfo">
            <div class="divInfoContext">
                <asp:Repeater runat="server" ID="rptInfo">
                    <HeaderTemplate>
                        <table width="100%" border="0" cellpadding="0" cellspacing="1" class="tablist" onmouseover="changeto()"
                            onmouseout="changeback()">
                            <tr class="tablisttitle">
                                <td width="4%">
                                    序号
                                </td>
                                <td width="4%">
                                    <%=cHTMLFx%>
                                </td>
                                <td>
                                    指标大类
                                </td>
                                <td>
                                    指标小类
                                </td>
                                <td>
                                    指标名称
                                </td>
                                <td>
                                    显示顺序
                                </td>
                                
                                <td>
                                    最小分值
                                </td>
                                <td>
                                    最大分值
                                </td>
                                <td>
                                    分值比例
                                </td>
                                <td>
                                    显示效果
                                </td>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr class="tablisttr">
                            <td>
                                <%#Eval("ID") %>
                            </td>
                            <td>
                                <asp:CheckBox runat="server" ID="chkselect" ToolTip='<%#Eval("ID") %>' />
                            </td>
                            <td>
                                <%#Eval("BigIDName")%>
                            </td>
                            <td>
                                <%#Eval("SmallIDName")%>
                            </td>
                            <td>
                                <%#UsToolsStringLengthTitle(Eval("EvalName").ToString(), 15)%>
                            </td>
                            <td>
                                <%#Eval("ShowID")%>
                            </td>
                            
                            <td>
                                <%#Eval("EMinValue")%>
                            </td>
                            <td>
                                <%#Eval("EMaxValue")%>
                            </td>
                            <td>
                                <%#Eval("EPower")%>
                            </td>
                            <td>
                                <%#KPIShowDetail(Eval("ID"), Eval("EType"), Eval("EValue"), Eval("EMaxValue"))%>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        <tr class="tablisttitle">
                            <td width="4%">
                                
                            </td>
                            <td width="4%">
                                
                            </td>
                            <td>
                                
                            </td>
                            <td>
                                
                            </td>
                            <td>
                               
                            </td>
                            <td>
                               
                            </td>
                            <td>
                                
                            </td>
                            
                            <td>
                                <% =siEMaxValue.ToString()%>
                            </td>
                            <td>
                                 <% =siEPower.ToString()%>
                            </td>
                            <td>
                               
                            </td>
                        </tr>
                        </table></FooterTemplate>
                </asp:Repeater>
            </div>
        </div>
        <div class="divInfo">
            <webdiyer:AspNetPager ID="Pager" runat="server" AlwaysShow="True" OnPageChanged="Pager_PageChanged"
                ShowCustomInfoSection="Left" ShowPageIndexBox="Always" ShowPageIndex="False"
                CustomInfoHTML="共 <B>%PageCount%</B> 页，当前为第 <B>%CurrentPageIndex%</B> 页，每页 <B>%PageSize%</B> 条"
                SubmitButtonText="" FirstPageText="首页" LastPageText="尾页" LayoutType="Table" NextPageText="后一页"
                PrevPageText="前一页" ShowMoreButtons="False" TextAfterPageIndexBox="页" TextBeforePageIndexBox="转到"
                CustomInfoTextAlign="Left" HorizontalAlign="Right">
            </webdiyer:AspNetPager>
        </div>
    </div>
    </form>
</body>
</html>
