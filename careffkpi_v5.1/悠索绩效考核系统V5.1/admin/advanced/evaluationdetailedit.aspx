﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="evaluationdetailedit.aspx.cs" Inherits="BasicWeb.admin.advanced.evaluationdetailedit" %>


<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <base target="_self" />
    <title>指标值</title>
    <asp:Literal ID="litregscript" runat="server" Text=""></asp:Literal>
    <link href="../style/default/style.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
function $id(id) {
    return document.getElementById(id);
}

function CareffModalDialogEVL() {
    var s1, s2;
    s1 = "R";
    s2 = $id("txtEValue").value;
    var arg, url, res;

    //组装URL
    url = "evaluationdetailedit.aspx" + "?ShowType=" + s1 + "&DefaultValue=" + escape(s2);
    para = "dialogWidth:700px;status:no;dialogHeight:420px";
    res = ModalDialog(url, para);
    if (res != "") {
        $id("txtEValue").value = res;
    }
    return false;
}

//################################################
//代码功能：显示模式窗口
//################################################
function ModalDialog(url, para) {
    var returnValue;
    returnValue = window.showModalDialog(url, window, para);
    if (returnValue == undefined) {
        returnValue = window.returnValue;
    }
    if (returnValue != null) {
        return returnValue;
    } else {
        return "";
    }
}
</script>
<script type="text/javascript">
    //<![CDATA[
    window.onerror = function() { return true; }
    //]]>
</script> 

</head>
<body onload="win_onLoad();">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div id="divLoading">
                <img src="/admin/style/default/images/loading.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div id="tbMain">
        <div class="divTbg">
            <div class="divTbgL">
            </div>
            <div class="divTbgInfo">
                <div class="divTbgTitle">
                    指标值管理</div>
            </div>
        </div>
        <div class="divTbgF">
        </div>
        <div class="divTNavBG" id="topURL">
            <ul id="topURLul">
                <li class="divURLed" id="div1">
                    <div>
                        <img src="../style/default/images/mini_icons_046.gif" width="10" height="10" />
                        基本信息</div>
                </li>
            </ul>
        </div>
        <div id="mypanel">
            <div class="divInfoContext">
                <div class="divInfo">
                    <div class="divInfoblack">
                        <div class="divInfoTopList">
                            <div class="listLeft">
                                <div class="navtitle">
                                    指标值</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="divInfo">
                    <table width="100%" border="0" cellpadding="0" cellspacing="1" class="tablist" id="mainlist">
<tr class="tablisttr">
<td class="tablisttitle">指标1</td><td class="tabmanagertr">
    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
    </td>
<td class="tablisttitle">指标6</td><td class="tabmanagertr">
    <asp:TextBox ID="TextBox6" runat="server"></asp:TextBox>
    </td>
</tr>
<tr class="tablisttr">
<td class="tablisttitle">指标2</td><td class="tabmanagertr">
    <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
    </td>
<td class="tablisttitle">指标7</td><td class="tabmanagertr">
    <asp:TextBox ID="TextBox7" runat="server"></asp:TextBox>
    </td>
</tr>
<tr class="tablisttr">
<td class="tablisttitle">指标3</td><td class="tabmanagertr">
    <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
    </td>
<td class="tablisttitle">指标8</td><td class="tabmanagertr">
    <asp:TextBox ID="TextBox8" runat="server"></asp:TextBox>
    </td>
</tr>
<tr class="tablisttr">
<td class="tablisttitle">指标4</td><td class="tabmanagertr">
    <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
    </td>
<td class="tablisttitle">指标9</td><td class="tabmanagertr">
    <asp:TextBox ID="TextBox9" runat="server"></asp:TextBox>
    </td>
</tr>
<tr class="tablisttr">
<td class="tablisttitle">指标5</td><td class="tabmanagertr">
    <asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>
    </td>
<td class="tablisttitle">指标10</td><td class="tabmanagertr">
    <asp:TextBox ID="TextBox10" runat="server"></asp:TextBox>
    </td>
</tr>
</table>
                    <div class="divSave">
                        <asp:Button ID="btnSubmit" runat="server" Text="保存(S)" 
                            CssClass="SkyButtonFocus" onclick="btnSubmit_Click" />
                        <asp:Button ID="btnExit" runat="server" Text="取消(C)" CssClass="SkyButtonFocus" 
                            onclick="btnSubmit_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </form>
</body>
</html>
