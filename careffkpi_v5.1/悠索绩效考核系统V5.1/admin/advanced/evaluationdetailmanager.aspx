﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="evaluationdetailmanager.aspx.cs" Inherits="BasicWeb.admin.advanced.evaluationdetailmanager" %>


<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>指标库</title>
    <asp:Literal ID="litregscript" runat="server" Text=""></asp:Literal>
    <link href="../style/default/style.css" rel="stylesheet" type="text/css" />

    <script language="javascript" src="/admin/js/System.js" type="text/javascript"></script>
 <script language="javascript" src="/admin/My97DatePicker/WdatePicker.js" type="text/jscript"></script>
 <script  language="javascript" src="/editor/xheditor.js" type="text/javascript" ></script>
<style type="text/css">
#cblPostID input {
	width:13px;
	height:13px;!important;
	margin:0px;!important;
	padding:0px;!important;
	border: 0px none #009999;
}
</style>
</head>
<body onload="win_onLoad();">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div id="divLoading">
                <img src="/admin/style/default/images/loading.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div id="tbMain">
        <div class="divTbg">
            <div class="divTbgL">
            </div>
            <div class="divTbgInfo">
                <div class="divTbgTitle">
                    指标库管理</div>
            </div>
        </div>
        <div class="divTbgF">
        </div>
        <div class="divTNavBG" id="topURL">
            <ul id="topURLul">
                <li class="divURLed" id="div1">
                    <div>
                        <img src="../style/default/images/mini_icons_046.gif" width="10" height="10" />
                        基本信息</div>
                </li>
            </ul>
        </div>
        <div id="mypanel">
            <div class="divInfoContext">
                <div class="divInfo">
                    <div class="divInfoblack">
                        <div class="divInfoTopList">
                            <div class="listLeft">
                                <div class="navtitle">
                                    指标库</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="divInfo">
                   <table width="100%" border="0" cellpadding="0" cellspacing="1" class="tablist" id="mainlist" onMouseOver="changeto()" onmouseout="changeback()">
<tr class="tablisttr">
<td class="tablisttitle">指标编号</td><td class="tabmanagertr"><asp:TextBox ID="txtEvalID" runat="server" ToolTip="指标编号" Text="" CssClass="1"></asp:TextBox></td>
<td class="tablisttitle">指标名称</td><td class="tabmanagertr"><asp:TextBox ID="txtEvalName" runat="server" ToolTip="指标名称" Text="" CssClass="1"></asp:TextBox></td>
</tr>
<tr class="tablisttr">
<td class="tablisttitle">显示类型</td><td class="tabmanagertr"><asp:DropDownList runat="server" ID="drpEType" ToolTip="显示类型">
                                    <asp:ListItem Value="EType" Text="codeno"></asp:ListItem>
                                </asp:DropDownList></td>
<td class="tablisttitle">默认数据</td><td class="tabmanagertr"><asp:TextBox ID="txtEValue" runat="server" ToolTip="默认数据" Text="10" CssClass="1" style="width:120px;"></asp:TextBox>
<input type="button" name="btnEdit" value="编辑" onclick="return CareffModalDialogEVL();" id="btnEdit" class="SkyButtonFocus" />
</td>
</tr>
<tr class="tablisttr">
<td class="tablisttitle">显示顺序</td><td class="tabmanagertr"><asp:TextBox ID="txtShowID" runat="server" ToolTip="显示顺序" Text="0" CssClass="2"></asp:TextBox></td>
<td class="tablisttitle">指标权重</td><td class="tabmanagertr"><asp:TextBox ID="txtEPower" runat="server" ToolTip="指标权重" Text="100"  CssClass="2"></asp:TextBox></td>
</tr>
<tr class="tablisttr">
<td class="tablisttitle">最大分值</td><td class="tabmanagertr"><asp:TextBox ID="txtEMaxValue" runat="server" ToolTip="最大分值" Text="10"  CssClass="2"></asp:TextBox></td>
<td class="tablisttitle">最小分值</td><td class="tabmanagertr"><asp:TextBox ID="txtEMinValue" runat="server" ToolTip="最小分值" Text="0"  CssClass="2"></asp:TextBox></td>
</tr>
<tr class="tablisttr">
<td class="tablisttitle">指标大类</td><td class="tabmanagertr">
    <asp:DropDownList runat="server" ID="drpBigID" ToolTip="指标大类" 
        AutoPostBack="True" onselectedindexchanged="drpBigID_SelectedIndexChanged">
                                    
                                </asp:DropDownList></td>
<td class="tablisttitle">指标小类</td><td class="tabmanagertr"><asp:DropDownList runat="server" ID="drpSmallID" ToolTip="指标小类">
                                    
                                </asp:DropDownList></td>
</tr>
<tr class="tablisttr">
<td class="tablisttitle">指标类型</td><td class="tabmanagertr"><asp:DropDownList runat="server" ID="drpEIType" ToolTip="指标类型">
                                    <asp:ListItem Value="EIType" Text="codeno"></asp:ListItem>
                                </asp:DropDownList></td>
<td class="tablisttitle">满分分值</td><td class="tabmanagertr"><asp:TextBox ID="txtEIMax" runat="server" ToolTip="满分分值" Text="100"></asp:TextBox></td>
</tr>
<tr class="tablisttr">
<td class="tablisttitle">指标状态</td><td class="tabmanagertr"><asp:DropDownList runat="server" ID="drpEState" ToolTip="指标状态">
                                    <asp:ListItem Value="CodeState" Text="codeno"></asp:ListItem>
                                </asp:DropDownList></td>
<td class="tablisttitle">岗位使用</td><td class="tabmanagertr">如果全部岗位都用，也可以什么都不选择即可</td>
</tr>
<tr class="tablisttr">
<td class="tablisttitle">岗位使用</td><td class="tabmanagertr" colspan="3"><asp:CheckBoxList runat="server" ID="cblPostID" ToolTip="岗位使用" 
        RepeatColumns="4" RepeatDirection="Horizontal" Width="100%"></asp:CheckBoxList></td>
</tr>
<tr class="tablisttr">
<td class="tablisttitle">指标备注</td><td class="tabmanagertr" colspan="3"><asp:TextBox ID="txtEvalInfo" runat="server" ToolTip="指标备注" Text="" TextMode="MultiLine" Height="200px" Width="100%" class="xheditor {upLinkUrl:s_upLinkUrl,upLinkExt:s_upLinkExt,upImgUrl:s_upLinkUrl,upImgExt:s_upImgExt,upFlashUrl:s_upLinkUrl,upFlashExt:s_upFlashExt,upMediaUrl:s_upLinkUrl,upMediaExt:s_upMediaExt}"></asp:TextBox></td>
</tr>
</table>
                    <div class="divSave">
                        <asp:Button ID="btnSubmit" runat="server" Text="保存(S)" 
                            CssClass="SkyButtonFocus" onclick="btnSubmit_Click" />
                        <asp:Button ID="btnExit" runat="server" Text="取消(C)" CssClass="SkyButtonFocus" 
                            onclick="btnSubmit_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </form>
</body>
</html>
