﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="datemanager.aspx.cs" Inherits="BasicWeb.admin.advanced.datemanager" %>


<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>时间段</title>
    <asp:Literal ID="litregscript" runat="server" Text=""></asp:Literal>
    <link href="../style/default/style.css" rel="stylesheet" type="text/css" />

    <script language="javascript" src="/admin/js/System.js" type="text/javascript"></script>
 <script language="javascript" src="/admin/My97DatePicker/WdatePicker.js" type="text/jscript"></script>
 <script  language="javascript" src="/editor/xheditor.js" type="text/javascript" ></script>

</head>
<body onload="win_onLoad();">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div id="divLoading">
                <img src="/admin/style/default/images/loading.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div id="tbMain">
        <div class="divTbg">
            <div class="divTbgL">
            </div>
            <div class="divTbgInfo">
                <div class="divTbgTitle">
                    时间段管理</div>
            </div>
        </div>
        <div class="divTbgF">
        </div>
        <div class="divTNavBG" id="topURL">
            <ul id="topURLul">
                <li class="divURLed" id="div1">
                    <div>
                        <img src="../style/default/images/mini_icons_046.gif" width="10" height="10" />
                        基本信息</div>
                </li>
            </ul>
        </div>
        <div id="mypanel">
            <div class="divInfoContext">
                <div class="divInfo">
                    <div class="divInfoblack">
                        <div class="divInfoTopList">
                            <div class="listLeft">
                                <div class="navtitle">
                                    时间段</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="divInfo">
                    <table width="100%" border="0" cellpadding="0" cellspacing="1" class="tablist" id="mainlist" onMouseOver="changeto()" onmouseout="changeback()">
<tr class="tablisttr">
<td class="tablisttitle">时间段编号</td><td class="tabmanagertr"><asp:TextBox ID="txtDateID" runat="server" ToolTip="时间段编号" Text="" CssClass="1"></asp:TextBox></td>
<td class="tablisttitle">时间段名称</td><td class="tabmanagertr"><asp:TextBox ID="txtDateName" runat="server" ToolTip="时间段名称" Text=""  CssClass="1"></asp:TextBox></td>
</tr>
<tr class="tablisttr">
<td class="tablisttitle">时间段状态</td><td class="tabmanagertr"><asp:DropDownList runat="server" ID="drpDState" ToolTip="时间段状态">
                                    <asp:ListItem Value="CodeState" Text="codeno"></asp:ListItem>
                                </asp:DropDownList></td>
<td class="tablisttitle">&nbsp;</td><td class="tabmanagertr">&nbsp;</td>
</tr>
<tr class="tablisttr">
<td class="tablisttitle">评价开始时间</td><td class="tabmanagertr"><asp:TextBox ID="txtDStart" runat="server"  CssClass="1" ToolTip="评价开始时间" Text="" onfocus="var txtAContractEnd=$dp.$('txtDEnd');WdatePicker({onpicked:function(){txtDEnd.focus();},maxDate:'#F{$dp.$D(\'txtDEnd\')}',isShowClear:true,readOnly:true,dateFmt:'yyyy-MM-dd'})"></asp:TextBox></td>
<td class="tablisttitle">评价结束时间</td><td class="tabmanagertr"><asp:TextBox ID="txtDEnd" runat="server"  CssClass="1" ToolTip="评价结束时间" Text="" onfocus="WdatePicker({minDate:'#F{$dp.$D(\'txtDStart\')}',isShowClear:true,readOnly:true,dateFmt:'yyyy-MM-dd'});"></asp:TextBox></td>
</tr>
<tr class="tablisttr">
<td class="tablisttitle">自评开始时间</td><td class="tabmanagertr"><asp:TextBox ID="txtDSelfStart" runat="server" CssClass="1" ToolTip="自评开始时间" Text="" onfocus="var txtAContractEnd=$dp.$('txtDSelfEnd');WdatePicker({onpicked:function(){txtDSelfEnd.focus();},maxDate:'#F{$dp.$D(\'txtDSelfEnd\')}',isShowClear:true,readOnly:true,dateFmt:'yyyy-MM-dd'})"></asp:TextBox></td>
<td class="tablisttitle">自评结束时间</td><td class="tabmanagertr"><asp:TextBox ID="txtDSelfEnd" runat="server" CssClass="1" ToolTip="自评结束时间" Text="" onfocus="WdatePicker({minDate:'#F{$dp.$D(\'txtDSelfStart\')}',isShowClear:true,readOnly:true,dateFmt:'yyyy-MM-dd'});"></asp:TextBox></td>
</tr>
<tr class="tablisttr">
<td class="tablisttitle">计划开始时间</td><td class="tabmanagertr"><asp:TextBox ID="txtDPlanStart" runat="server" CssClass="1" ToolTip="计划开始时间" Text="" onfocus="var txtAContractEnd=$dp.$('txtDPlanEnd');WdatePicker({onpicked:function(){txtDPlanEnd.focus();},maxDate:'#F{$dp.$D(\'txtDPlanEnd\')}',isShowClear:true,readOnly:true,dateFmt:'yyyy-MM-dd'})"></asp:TextBox></td>
<td class="tablisttitle">计划结束时间</td><td class="tabmanagertr"><asp:TextBox ID="txtDPlanEnd" runat="server" CssClass="1" ToolTip="计划结束时间" Text="" onfocus="WdatePicker({minDate:'#F{$dp.$D(\'txtDPlanStart\')}',isShowClear:true,readOnly:true,dateFmt:'yyyy-MM-dd'});"></asp:TextBox></td>
</tr>
<tr class="tablisttr">
<td class="tablisttitle">备注说明</td><td class="tabmanagertr" colspan="3"><asp:TextBox ID="txtDateInfo" runat="server" ToolTip="备注说明" Text="" TextMode="MultiLine" Height="100px" Width="100%" class="xheditor {upLinkUrl:s_upLinkUrl,upLinkExt:s_upLinkExt,upImgUrl:s_upLinkUrl,upImgExt:s_upImgExt,upFlashUrl:s_upLinkUrl,upFlashExt:s_upFlashExt,upMediaUrl:s_upLinkUrl,upMediaExt:s_upMediaExt}"></asp:TextBox></td>
</tr>
</table>
                    <div class="divSave">
                        <asp:Button ID="btnSubmit" runat="server" Text="保存(S)" 
                            CssClass="SkyButtonFocus" onclick="btnSubmit_Click" />
                        <asp:Button ID="btnExit" runat="server" Text="取消(C)" CssClass="SkyButtonFocus" 
                            onclick="btnSubmit_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </form>
</body>
</html>
