﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="usermanagerself.aspx.cs" Inherits="BasicWeb.admin.basic.usermanagerself" %>


<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>用户</title>
    <asp:Literal ID="litregscript" runat="server" Text=""></asp:Literal>
    <link href="../style/default/style.css" rel="stylesheet" type="text/css" />

    <script language="javascript" src="/admin/js/System.js" type="text/javascript"></script>
    <script language="javascript" src="/admin/js/SystemTree.js" type="text/javascript"></script>
    <script  language="javascript" src="/editor/xheditor.js" type="text/javascript" ></script>


<style type="text/css">
.NTree input {
	width:12px;
	height:12px;!important;
	margin:0px;!important;
	padding:0px;!important;
	border: 0px none #009999;
}
</style>

<script language="javascript" type="text/javascript">
    function opJsTree(txt, txtValue) {
        var para = "/admin/basic/departmenttreeselect.aspx?para=3&list=" + $("#" + txtValue).val();
        //alert(para);
        //alert(para);
        UsOpenDialogTree(para, txt, txtValue);
        $id("btnchange").click();
    }

    function opJsTree2(txt, txtValue) {
        var para = "/admin/basic/departmenttreeselect.aspx?para=2&list=" + $("#" + txtValue).val();
        UsOpenDialogTree(para, txt, txtValue);
        //$id("btnchange").click();
    }


    function jscheckAll() {
        var sValue = $("#txtUserPwd").val();
        if (sValue != "") {
            var num = sValue.length;
            if (num < 3 || num > 16) {
                window.alert("密码长度不符合规则(3-16位)，请重新输入！");
                return false;
            }
            
        }
    }
    
 </script>

</head>
<body onload="win_onLoad();">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div id="divLoading">
                <img src="/admin/style/default/images/loading.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div id="tbMain">
        <div class="divTbg">
            <div class="divTbgL">
            </div>
            <div class="divTbgInfo">
                <div class="divTbgTitle">
                    用户管理</div>
            </div>
        </div>
        <div class="divTbgF">
        </div>
        <div class="divTNavBG" id="topURL">
            <ul id="topURLul">
                <li class="divURLed" id="div1">
                    <div>
                        <img src="../style/default/images/mini_icons_046.gif" width="10" height="10" />
                        基本信息</div>
                </li>
            </ul>
        </div>
        <div id="mypanel">
            <div class="divInfoContext">
                <div class="divInfo">
                    <div class="divInfoblack">
                        <div class="divInfoTopList">
                            <div class="listLeft">
                                <div class="navtitle">
                                    用户管理</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="divInfo">
                    <table width="100%" border="0" cellpadding="0" cellspacing="1" class="tablist" id="mainlist" onMouseOver="changeto()" onmouseout="changeback()">
<tr class="tablisttr">
<td class="tablisttitle">用户编号</td><td class="tabmanagertr"><asp:TextBox ID="txtUserID" runat="server" ToolTip="用户编号" Text="" CssClass="1"></asp:TextBox></td>
<td class="tablisttitle">用户名称</td><td class="tabmanagertr"><asp:TextBox ID="txtUserName" runat="server" ToolTip="用户名称" Text=""  CssClass="1"></asp:TextBox></td>
</tr>
<tr class="tablisttr">
<td class="tablisttitle">用户密码</td><td class="tabmanagertr"><asp:TextBox ID="txtUserPwd" runat="server" ToolTip="用户密码" Text="" ></asp:TextBox></td>
<td class="tablisttitle">用户角色</td><td class="tabmanagertr"><asp:DropDownList runat="server" ID="drpRoleID" CssClass="1" ToolTip="用户角色"  Enabled="false">
                            <asp:ListItem Value="LModel" Text="code"></asp:ListItem>
                        </asp:DropDownList>         </td>
</tr>
<tr class="tablisttr">
<td class="tablisttitle">用户电话</td><td class="tabmanagertr"><asp:TextBox ID="txtUserTel" runat="server" ToolTip="用户电话" Text=""></asp:TextBox>         </td>
<td class="tablisttitle">电子邮箱</td><td class="tabmanagertr"><asp:TextBox ID="txtUserEmail" runat="server" ToolTip="电子邮箱" Text=""></asp:TextBox></td>
</tr>
<tr class="tablisttr">
<td class="tablisttitle">隶属部门</td><td class="tabmanagertr">
<asp:TextBox ID="txtDeptName" runat="server" ToolTip="隶属部门" Text="" CssClass="1"></asp:TextBox>
<span runat="server" id="Span1" visible="false"><a href="javascript:opJsTree('txtDeptName','txtDeptID');">
    [选择]</a></span>

<div style="display:none"><asp:TextBox ID="txtDeptID" runat="server" ToolTip="部门编号" Text=""></asp:TextBox>


<asp:Button ID="btnchange" runat="server" Text="调整" 
                            CssClass="SkyButtonFocus" onclick="btnchange_Click" />


</div>
</td>
<td class="tablisttitle">隶属岗位</td><td class="tabmanagertr"><asp:DropDownList runat="server" ID="drpPostID" ToolTip="隶属岗位"  Enabled="false">

                                </asp:DropDownList></td>
</tr>
<tr class="tablisttr">
<td class="tablisttitle">用户性别</td><td class="tabmanagertr"><asp:DropDownList runat="server" ID="drpUserSex" ToolTip="用户性别" Enabled="false">
                                    <asp:ListItem Value="CodeSex" Text="codeno"></asp:ListItem>
                                </asp:DropDownList></td>
<td class="tablisttitle">用户照片</td><td class="tabmanagertr"><asp:TextBox ID="txtUserPic" runat="server" ToolTip="用户照片" Text=""></asp:TextBox>
<span runat="server" id="divClose">


 <a href="#"  onClick="UsOpenDialog('txtUserPic');" >[上传]</a>
  <a href="#"  onClick="UsView('txtUserPic');" >[查看]</a>
   <a href="#"  onClick="UsClear('txtUserPic');" >[删除]</a>
 





</span>

</td>
</tr>
<tr class="tablisttr">
<td class="tablisttitle">启用状态</td><td class="tabmanagertr"><asp:DropDownList runat="server" ID="drpUserState" ToolTip="启用状态">
                                    <asp:ListItem Value="UserState" Text="codeno"></asp:ListItem>
                                </asp:DropDownList></td>
<td class="tablisttitle">联系地址</td><td class="tabmanagertr"><asp:TextBox ID="txtUserAddr" runat="server" ToolTip="联系地址"></asp:TextBox></td>
</tr>

<tr class="tablisttr">
<td class="tablisttitle">可评人员部门</td><td class="tabmanagertr" colspan="3"><asp:TextBox ID="txtPUserDepartment" runat="server" ToolTip="可评人员部门" Text=""  Width="90%"  Enabled="false"></asp:TextBox>
<span runat="server" id="Span2"></span>
<div style="display:none"><asp:TextBox ID="txtPUserDepartmentID" runat="server" ToolTip="可评人员部门编号" 
        Text="" ></asp:TextBox></div>
</td>
</tr>

<tr class="tablisttr">
<td class="tablisttitle">用户说明</td><td class="tabmanagertr" colspan="3"><asp:TextBox ID="txtUserInfo" runat="server" ToolTip="用户说明" Text="" TextMode="MultiLine" Height="100px" Width="100%" class="xheditor {upLinkUrl:s_upLinkUrl,upLinkExt:s_upLinkExt,upImgUrl:s_upLinkUrl,upImgExt:s_upImgExt,upFlashUrl:s_upLinkUrl,upFlashExt:s_upFlashExt,upMediaUrl:s_upLinkUrl,upMediaExt:s_upMediaExt}"></asp:TextBox></td>
</tr>
</table>
                  
                  <div class="divSave">
                        <asp:Button ID="btnSubmit" runat="server" Text="保存(S)" 
                            CssClass="SkyButtonFocus" onclick="btnSubmit_Click" />
                        <asp:Button ID="btnExit" runat="server" Text="取消(C)" CssClass="SkyButtonFocus" 
                            onclick="btnSubmit_Click" />
                  </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </form>
</body>
</html>
