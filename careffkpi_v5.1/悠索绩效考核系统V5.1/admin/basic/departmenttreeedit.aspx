﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="departmenttreeedit.aspx.cs" Inherits="BasicWeb.admin.basic.departmenttreeedit" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>系统部门</title>
<link href="../style/default/style.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="/admin/js/System.js" type="text/javascript"></script>
<link rel="StyleSheet" href="/admin/js/tree/dtree.css" type="text/css" />
<script type="text/javascript" src="/admin/js/tree/dtree.js"></script>


 <script src="../js/jquery/jquery.js" type="text/javascript"></script>

 <script language="javascript" type="text/javascript" >
     function AutoHeight() {
         var he = document.body.clientHeight - 70;
         var s = document.getElementById("ScroLeft");
         s.style.height = he;
     }
     $(window).resize(function() {
         AutoHeight();
     });
     
     $(document).ready(function() {
        AutoHeight();
    });
</script>


</head>
<body>
<form id="Form1" method="post" runat="server">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" height="100%">
    <tr>
      <td width="180" valign="top"><div class="divTbg">
          <div class="divTbgL"></div>
          <div class="divTbgInfo">
            <div class="divTbgTitle">系统部门管理</div>
          </div>
        </div>
        <div class="divTbgF"></div>
        <div class="divSearchBoth">
          <div class="divInfo" align="center">
            <a href="departmentmanager.aspx" class="rptlink" target="frameright">增加部门</a>
          </div>
          <div class="divInfo">
            <div class="divAutoHeight" id="ScroLeft"><%=TreeHTML %></div>
            
            
          </div>
        </div></td>
      <td  valign="top" ><iframe src="" width="100%" height="100%" id="frameright" name="frameright" frameborder="0"
                    scrolling="auto"></iframe></td>
    </tr>
  </table>
</form>
</body>
</html>
