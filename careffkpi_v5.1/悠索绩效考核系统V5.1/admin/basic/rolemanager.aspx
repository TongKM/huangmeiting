﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="rolemanager.aspx.cs" Inherits="BasicWeb.admin.basic.rolemanager" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>系统角色</title>
<asp:Literal ID="litregscript" runat="server" Text=""></asp:Literal>
<link href="../style/default/style.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="/admin/js/System.js" type="text/javascript"></script>
<script language="javascript" src="/admin/js/SystemTree.js" type="text/javascript"></script>
<style type="text/css">
.NTree input {
	width:12px;
	height:12px;!important;
	margin:0px;!important;
	padding:0px;!important;
	border: 0px none #009999;
}
</style>
</head>
<body onload="win_onLoad();">
<form id="form1" runat="server">
  <asp:ScriptManager ID="ScriptManager1" runat="server"> </asp:ScriptManager>
  <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate> </ContentTemplate>
  </asp:UpdatePanel>
  <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
    <ProgressTemplate>
      <div id="divLoading"> <img src="/admin/style/default/images/loading.gif" /> </div>
    </ProgressTemplate>
  </asp:UpdateProgress>
  <div id="tbMain">
    <div class="divTbg">
      <div class="divTbgL"> </div>
      <div class="divTbgInfo">
        <div class="divTbgTitle"> 系统角色管理</div>
      </div>
    </div>
    <div class="divTbgF"> </div>
    <div class="divTNavBG" id="topURL">
      <ul id="topURLul">
        <li class="divURLed" id="div1">
          <div> <img src="../style/default/images/mini_icons_046.gif" width="10" height="10" /> 基本信息</div>
        </li>
      </ul>
    </div>
    <div id="mypanel">
      <div class="divInfoContext">
        <div class="divInfo">
          <div class="divInfoblack">
            <div class="divInfoTopList">
              <div class="listLeft">
                <div class="navtitle"> 系统角色</div>
              </div>
            </div>
          </div>
        </div>
        <div class="divInfo">
          <table width="100%" border="0" cellspacing="0" cellpadding="5">
            <tr>
              <td valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="1" class="tablist" id="mainlist" >
                  <tr class="tablisttr">
                    <td class="tablisttitle"> 角色编号</td>
                    <td class="tabmanagertr"><asp:TextBox ID="txtRoleID" runat="server" ToolTip="角色编号" Text="" CssClass="1"></asp:TextBox></td>
                  </tr>
                  <tr class="tablisttr">
                    <td class="tablisttitle"> 角色名称</td>
                    <td class="tabmanagertr"><asp:TextBox ID="txtRName" runat="server" CssClass="1" ToolTip="角色名称"></asp:TextBox>                    </td>
                  </tr>
                  <tr class="tablisttr">
                    <td class="tablisttitle"> 角色描述</td>
                    <td class="tabmanagertr"><asp:TextBox ID="txtRInfo" runat="server"></asp:TextBox>                    </td>
                  </tr>
                  <tr class="tablisttr">
                    <td class="tablisttitle">系统角色</td>
                    <td class="tabmanagertr"><asp:DropDownList runat="server" ID="drpRState" CssClass="1" tooltip="系统角色">
                        <asp:ListItem Value="RState" Text="codeno"></asp:ListItem>
                      </asp:DropDownList></td>
                  </tr>
                  <tr class="tablisttr">
                    <td class="tablisttitle">角色说明</td>
                    <td class="tabmanagertr">
                    <div style="height:200px;">
                    请在右侧选择该角色可以操作的菜单。</div></td>
                  </tr>
                </table></td>
              <td width="50%" valign="top">
              
              <div class="divSearch">
              <div style="width:200px; height:277px; overflow:auto;" class="NTree">
                  <asp:TreeView ID="TreeView1" runat="server" ExpandDepth="2" 
                          ImageSet="XPFileExplorer" ShowCheckBoxes="All" ShowLines="True"> </asp:TreeView>
                </div></div></td>
            </tr>
          </table>
          <div class="divSave">
            <asp:Button ID="btnSubmit" runat="server" Text="保存(S)" 
                            CssClass="SkyButtonFocus" onclick="btnSubmit_Click" />
            <asp:Button ID="btnExit" runat="server" Text="取消(C)" CssClass="SkyButtonFocus" 
                            onclick="btnSubmit_Click" />
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</form>
</body>
</html>
