﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="oprlogmanager.aspx.cs" Inherits="BasicWeb.admin.basic.oprlogmanager" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>操作日志</title>
    <asp:Literal ID="litregscript" runat="server" Text=""></asp:Literal>
    <link href="../style/default/style.css" rel="stylesheet" type="text/css" />

    <script language="javascript" src="/admin/js/System.js" type="text/javascript"></script>

</head>
<body onload="win_onLoad();">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div id="divLoading">
                <img src="/admin/style/default/images/loading.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div id="tbMain">
        <div class="divTbg">
            <div class="divTbgL">
            </div>
            <div class="divTbgInfo">
                <div class="divTbgTitle">
                    操作日志管理</div>
            </div>
        </div>
        <div class="divTbgF">
        </div>
        <div class="divTNavBG" id="topURL">
            <ul id="topURLul">
                <li class="divURLed" id="div1">
                    <div>
                        <img src="../style/default/images/mini_icons_046.gif" width="10" height="10" />
                        基本信息</div>
                </li>
            </ul>
        </div>
        <div id="mypanel">
            <div class="divInfoContext">
                <div class="divInfo">
                    <div class="divInfoblack">
                        <div class="divInfoTopList">
                            <div class="listLeft">
                                <div class="navtitle">
                                    操作日志</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="divInfo">
                    <table width="100%" border="0" cellpadding="0" cellspacing="1" class="tablist" id="mainlist" onMouseOver="changeto()" onmouseout="changeback()">
<tr class="tablisttr">
<td class="tablisttitle">系统编号</td><td class="tabmanagertr"><asp:TextBox ID="txtID" runat="server" ToolTip="系统编号"></asp:TextBox></td>
<td class="tablisttitle">日志标题</td><td class="tabmanagertr"><asp:TextBox ID="txtLName" runat="server" ToolTip="日志描述" Text=""></asp:TextBox></td>
</tr>
<tr class="tablisttr">
<td class="tablisttitle">操作模块</td><td class="tabmanagertr"><asp:DropDownList runat="server" ID="drpLModel" ToolTip="操作方法">
                                    <asp:ListItem Value="LModel" Text="code"></asp:ListItem>
                                </asp:DropDownList></td>
<td class="tablisttitle">操作方法</td><td class="tabmanagertr"><asp:DropDownList runat="server" ID="drpLType" ToolTip="操作方法">
                                    <asp:ListItem Value="LType" Text="code"></asp:ListItem>
                                </asp:DropDownList></td>

</tr>
<tr class="tablisttr">
<td class="tablisttitle">用户编号</td><td class="tabmanagertr"><asp:TextBox ID="txtCreateBy" runat="server" ToolTip="建立人的ID"></asp:TextBox></td>
<td class="tablisttitle">用户名称</td><td class="tabmanagertr"><asp:TextBox ID="txtUpdateby" runat="server" ToolTip="用户名称"></asp:TextBox></td>
</tr>
<tr class="tablisttr">
<td class="tablisttitle">操作IP</td><td class="tabmanagertr"><asp:TextBox ID="txtIP" runat="server" ToolTip="操作IP" Text=""></asp:TextBox></td>
<td class="tablisttitle">操作时间</td><td class="tabmanagertr"><asp:TextBox ID="txtCreateDate" runat="server" ToolTip="建立时间"></asp:TextBox></td>
</tr>
<tr class="tablisttr">
<td class="tablisttitle">详细内容</td><td class="tabmanagertr"><asp:TextBox ID="txtLInfo" runat="server" ToolTip="详细内容" Text="" TextMode="MultiLine" Height="100px"></asp:TextBox></td>
<td class="tablisttitle">操作语句</td><td class="tabmanagertr"><asp:TextBox ID="txtLSQL" runat="server" ToolTip="操作语句" Text=""  TextMode="MultiLine" Height="100px"></asp:TextBox></td>
</tr>
</table>
                    <div class="divSave">
                        <asp:Button ID="btnSubmit" runat="server" Text="保存(S)" 
                            CssClass="SkyButtonFocus" onclick="btnSubmit_Click" />
                        <asp:Button ID="btnExit" runat="server" Text="取消(C)" CssClass="SkyButtonFocus" 
                            onclick="btnSubmit_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </form>
</body>
</html>
