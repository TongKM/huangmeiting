﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="softregister.aspx.cs" Inherits="BasicWeb.admin.basic.softregister" %>


<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>软件注册</title>
    <asp:Literal ID="litregscript" runat="server" Text=""></asp:Literal>
    <link href="../style/default/style.css" rel="stylesheet" type="text/css" />

    <script language="javascript" src="/admin/js/System.js" type="text/javascript"></script>
<script  language="javascript" src="/editor/xheditor.js" type="text/javascript" ></script>

</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div id="divLoading">
                <img src="/admin/style/default/images/loading.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div id="tbMain">
        <div class="divTbg">
            <div class="divTbgL">
            </div>
            <div class="divTbgInfo">
                <div class="divTbgTitle">
                    软件注册</div>
            </div>
        </div>
        <div class="divTbgF">
        </div>
        
        <div id="mypanel">
            <div class="divInfoContext">
                <div class="divInfo">
                    <div class="divInfoblack">
                        <div class="divInfoTopList">
                            <div class="listLeft">
                                <div class="navtitle">
                                    软件注册</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="divInfo">
                   <table width="100%" border="0" cellpadding="0" cellspacing="1" class="tablist" id="mainlist" onMouseOver="changeto()" onmouseout="changeback()">
                        <tr class="tablisttr">
                            <td width="100" align="center" class="tablisttitle">
                                运行IP                            </td>
                            <td class="tabmanagertr">
                                <asp:TextBox ID="txtWebIP" runat="server" CssClass="1" ToolTip="运行IP" 
                                    Width="200px" ReadOnly="True"></asp:TextBox>                            </td>
                            <td width="100" align="center" class="tablisttitle">网卡地址</td>
                            <td class="tabmanagertr"><asp:TextBox ID="txtWebMac" runat="server" CssClass="1" ToolTip="网卡地址" 
                                    Width="200px" ReadOnly="True"></asp:TextBox></td>
                            <td width="100" align="center" class="tablisttitle">网站端口 </td>
                            <td class="tabmanagertr"><asp:TextBox ID="txtWebPort" runat="server" CssClass="1" ToolTip="网站端口" 
                                    Width="200px" ReadOnly="True"></asp:TextBox></td>
                        </tr>
                        <tr class="tablisttr">
                            <td align="center" class="tablisttitle" width="100">
                                网站地址                            </td>
                            <td class="tabmanagertr">
                                <asp:TextBox ID="txtWebUrl" runat="server" CssClass="1" ToolTip="网站地址" 
                                    Width="200px" ReadOnly="True"></asp:TextBox>                            </td>
                            <td align="center" class="tablisttitle">注册时间</td>
                            <td class="tabmanagertr"><asp:TextBox ID="txtWebRegdate" runat="server" CssClass="1" ToolTip="注册时间" 
                                    Width="200px" ReadOnly="True"></asp:TextBox></td>
                            <td width="100" align="center" class="tablisttitle">到期时间 </td>
                            <td class="tabmanagertr"><asp:TextBox ID="txtWebEnddate" runat="server" CssClass="1" ToolTip="到期时间" Width="200px"></asp:TextBox></td>
                        </tr>
                        <tr class="tablisttr">
                            <td align="center" class="tablisttitle" width="100">
                                在线人数                            </td>
                            <td class="tabmanagertr">
                                <asp:TextBox ID="txtWebOnline" runat="server" CssClass="2" ToolTip="在线人数" 
                                    Width="200px">1000</asp:TextBox>                            </td>
                            <td align="center" class="tablisttitle">软件名称 </td>
                            <td class="tabmanagertr"><asp:TextBox ID="txtWebName" runat="server" CssClass="1" 
                                    ToolTip="软件名称，可以显示XXXX教务管理系统" Width="200px">软件名称</asp:TextBox></td>
                            <td width="100" align="center" class="tablisttitle">软件版本 </td>
                            <td class="tabmanagertr"><asp:TextBox ID="txtWebAssemblyVersion" runat="server" CssClass="1" ToolTip="软件版本"
                                    Width="200px" ReadOnly="True"></asp:TextBox></td>
                        </tr>
                        <tr class="tablisttr">
                            <td align="center" class="tablisttitle" width="100">单位名称</td>
                            <td class="tabmanagertr"><asp:TextBox ID="txtWebUserName" runat="server" CssClass="1" 
                                    ToolTip="显示版权信息" Width="200px">单位名称</asp:TextBox></td>
                            <td align="center" class="tablisttitle">联系电话</td>
                            <td class="tabmanagertr"><asp:TextBox ID="txtWebTel" runat="server" CssClass="6" ToolTip="联系电话" 
                                    Width="200px">13936685915</asp:TextBox></td>
                            <td width="100" align="center" class="tablisttitle">&nbsp;</td>
                            <td class="tabmanagertr">&nbsp;</td>
                        </tr>
                        <tr class="tablisttr">
                            <td width="100" align="center" class="tablisttitle">
                                功能模块</td>
                            <td colspan="5">
                                <asp:CheckBoxList ID="chkFunction" runat="server" RepeatColumns="6" 
                                    RepeatDirection="Horizontal">
                                    <asp:ListItem>1</asp:ListItem>
                                    <asp:ListItem>2</asp:ListItem>
                                    <asp:ListItem>3</asp:ListItem>
                                </asp:CheckBoxList>                            </td>
                        </tr>
                    </table>
                    
                    
                     <div class="divSave">
                    
                  <asp:Button ID="btnSubmit" runat="server" Text="生成(S)" CssClass="SkyButtonFocus"
                OnClick="btnSubmit_Click" AccessKey="S" CommandName="save"></asp:Button>
                    
                    </div>
                    
                    
                    <div class="divInfoblack">
                        <div class="divInfoTopList">
                            <div class="listLeft">
                                <div class="navtitle">
                                    授权文件</div>
                            </div>
                        </div>
                   
                </div>
                    <table width="100%" border="0" cellpadding="0" cellspacing="1" class="tablist" id="Table1" onMouseOver="changeto()" onmouseout="changeback()">
                    <tr class="tablisttr">
                      <td align="center" class="tablisttitle">授权类型</td>
                      <td class="tabmanagertr">
                          <asp:Literal ID="LiteralGetAssembly" runat="server"></asp:Literal>
                        </td>
                    </tr>
                    <tr class="tablisttr">
                        <td width="100" align="center" class="tablisttitle">
                            临时文件                        </td>
                        <td class="tabmanagertr">
                            <asp:TextBox ID="txtLicTmp" runat="server" ToolTip="临时授权" Width="600px" 
                                TextMode="MultiLine" Height="80px"></asp:TextBox>                        </td>
                    </tr>
                    <tr class="tablisttr">
                        <td align="center" class="tablisttitle" width="100">
                            授权文件                        </td>
                        <td class="tabmanagertr">
                            <asp:TextBox ID="txtLicFinish" runat="server" ToolTip="正式授权" Width="600px" 
                                TextMode="MultiLine" Height="80px"></asp:TextBox>                        </td>
                    </tr>
                </table>
                    
                    <div class="divSave">
                    
                    <asp:Button ID="btnOK" runat="server" CommandName="saveclose" CssClass="SkyButtonFocus"
                OnClick="btnOK_Click" Text="注册(O)" />
                    
                    </div>
                </div>
            </div>
        </div>
    </div>

    </form>
</body>
</html>
