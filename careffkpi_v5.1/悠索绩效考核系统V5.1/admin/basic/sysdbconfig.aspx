﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="sysdbconfig.aspx.cs" Inherits="BasicWeb.admin.basic.sysdbconfig" %>



<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>数据库配置</title>
    <asp:Literal ID="litregscript" runat="server" Text=""></asp:Literal>
    <link href="../style/default/style.css" rel="stylesheet" type="text/css" />

    <script language="javascript" src="/admin/js/System.js" type="text/javascript"></script>
    <script  language="javascript" src="/editor/xheditor.js" type="text/javascript"></script>
<style type="text/css">
 #radSQLServer,#radAccess
 {
	width:14px;
	height:14px;!important;
	margin:0px;!important;
	padding:0px;!important;
	border: 0px none #009999;
}
</style>
</head>
<body onload="win_onLoad();">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div id="divLoading">
                <img src="/admin/style/default/images/loading.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div id="tbMain">
        <div class="divTbg">
            <div class="divTbgL">
            </div>
            <div class="divTbgInfo">
                <div class="divTbgTitle">
                    数据库配置管理</div>
            </div>
        </div>
        <div class="divTbgF">
        </div>
        <div class="divTNavBG" id="topURL">
            <ul id="topURLul">
                <li class="divURLed" id="div1">
                    <div>
                        <img src="../style/default/images/mini_icons_046.gif" width="10" height="10" />
                        基本信息</div>
                </li>
            </ul>
        </div>
        <div id="mypanel">
            <div class="divInfoContext">
                <div class="divInfo">
                    <div class="divInfoblack">
                        <div class="divInfoTopList">
                            <div class="listLeft">
                                <div class="navtitle">
                                    数据库配置</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="divInfo">
                   <table width="100%" border="0" cellpadding="0" cellspacing="1" class="tablist" id="mainlist" onMouseOver="changeto()" onmouseout="changeback()">
<tr class="tablisttr" >
<td class="tablisttitle" width="100px">数据库类型</td><td class="tabmanagertr"><asp:RadioButton ID="radAccess" runat="server" AutoPostBack="True" Checked="True"
                                                                                            GroupName="database" OnCheckedChanged="radAccess_CheckedChanged" Text="Access数据库" />
                    <asp:RadioButton ID="radSQLServer" runat="server" AutoPostBack="True" GroupName="database"
                                                                                            OnCheckedChanged="radSQLServer_CheckedChanged" Text="MsSQLServer数据库" /></td>

</tr>
<tbody runat="server" id="TBodyDataBase" visible="false">
<tr class="tablisttr">
<td class="tablisttitle">数据库服务器</td><td class="tabmanagertr"><asp:TextBox ID="txt1" runat="server"  CssClass="1" ToolTip="数据库服务器" Width="300px">localhost</asp:TextBox></td>

</tr>
<tr class="tablisttr">
<td class="tablisttitle">数据库名称</td><td class="tabmanagertr"><asp:TextBox ID="txt2" runat="server" CssClass="1" ToolTip="数据库名称" Width="300px">SchoolDB</asp:TextBox></td>

</tr>
<tr class="tablisttr">
<td class="tablisttitle">登陆用户名</td><td class="tabmanagertr"><asp:TextBox ID="txt3" runat="server"  CssClass="1" ToolTip="登陆用户名" Width="300px">sa</asp:TextBox></td>

</tr>
<tr class="tablisttr">
<td class="tablisttitle">登陆密码</td><td class="tabmanagertr"><asp:TextBox ID="txt4" runat="server"  CssClass="1" ToolTip="登陆密码" Width="300px">00000000</asp:TextBox></td>

</tr>
</tbody>
<tbody runat="server" id="TBodyDataBaseAccess" visible="true">
<tr class="tablisttr">
<td class="tablisttitle">数据库地址</td><td class="tabmanagertr"><asp:TextBox ID="txt5" runat="server" Width="300px"></asp:TextBox></td>
</tr>
</tbody>
</table>
                    <div class="divSave">
                        <asp:Button ID="btnSubmit" runat="server" Text="保存(S)" 
                            CssClass="SkyButtonFocus" onclick="btnSubmit_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </form>
</body>
</html>