﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="oprloglist.aspx.cs" Inherits="BasicWeb.admin.basic.oprloglist" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>操作日志</title>
    <link href="../style/default/style.css" rel="stylesheet" type="text/css" />

    <script language="javascript" src="/admin/js/System.js" type="text/javascript"></script>
    <script language="javascript" src="/admin/My97DatePicker/WdatePicker.js" type="text/jscript"></script>

</head>
<body>
    <form id="Form1" method="post" runat="server">
    <div class="divTbg">
      <div class="divTbgL"></div>
      <div class="divTbgInfo">
        <div class="divTbgTitle">操作日志管理</div>
      </div>
    </div>
    <div class="divTbgF"></div>
    <div class="divSearchBoth">
        <div class="divInfo">
            <div class="divInfoblack">
                <div class="divInfoTopList">
                    <div class="listLeft">
                        <div class="navsearchtitle">
                            操作日志搜索</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="divInfo">
            <div class="divSearch">
                <div id="searchBox" class="clearsearch">
                                        
                   
                    
                    <p>
                        <label for="drpLModel">
                            操作模块：</label>
                        <asp:DropDownList runat="server" ID="drpLModel" ToolTip="操作模块">
                            <asp:ListItem Value="LModel" Text="code"></asp:ListItem>
                          </asp:DropDownList>
                    </p>
                    
                     <p>
                        <label for="drpLType">
                            操作方法：</label>
                        <asp:DropDownList runat="server" ID="drpLType" ToolTip="操作方法">
                            <asp:ListItem Value="LType" Text="code"></asp:ListItem>
                          </asp:DropDownList>
                    </p>
                    
                      <p>
                        <label for="txtUserID">
                            用户编号：</label>
                        <asp:TextBox ID="txtUserID" runat="server"></asp:TextBox>
                    </p>
                    <p>
                        <label for="txtUserName">
                            用户名称：</label>
                        <asp:TextBox ID="txtUserName" runat="server"></asp:TextBox>
                    </p>
                   
                   
                    <p>
                        <label for="txtIP">
                            操作IP：</label>
                        <asp:TextBox ID="txtIP" runat="server"></asp:TextBox>
                    </p>
                   <p>
                       <label for="txtBeginDate">开始时间：</label>
                        <asp:TextBox ID="txtBeginDate" runat="server" onfocus="var txtAContractEnd=$dp.$('txtEndDate');WdatePicker({onpicked:function(){txtEndDate.focus();},maxDate:'#F{$dp.$D(\'txtEndDate\')}',isShowClear:true,readOnly:true,dateFmt:'yyyy-MM-dd'})"></asp:TextBox>
                     </p>
                     <p>
                      <label for="txtEndDate">结束时间：</label>
                       <asp:TextBox ID="txtEndDate" runat="server" onfocus="WdatePicker({minDate:'#F{$dp.$D(\'txtBeginDate\')}',isShowClear:true,readOnly:true,dateFmt:'yyyy-MM-dd'});"></asp:TextBox>
                      </p>

                    <p>
                        <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="确认搜索" CssClass="SkyButtonFocus" />
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="divInfo">
        <div class="divInfoblack">
            <div class="divInfoTopList">
                <div class="listLeft">
                    <div class="navtitle">
                        操作日志详细列表</div>
                </div>
                <div class="listRight">
                    <ul>
                        <li class="navstyle1">
                            <input type="checkbox" name="chkselect" id="chkselect" onclick="skySelectCheckList();"/>
                            <label for="chkselect">全选</label> </li>
                        <li class="navstyle2">
                            <asp:LinkButton ID="LnkBAdd" runat="server" OnClick="SkyLnkBAMD_Click" Enabled="false">添加</asp:LinkButton>
                        </li>
                        <li class="navstyle3">
                            <asp:LinkButton ID="LnkBEdit" runat="server" OnClick="SkyLnkBAMD_Click" Enabled="false">编辑</asp:LinkButton>
                        </li>
                        <li class="navstyle4">
                            <asp:LinkButton ID="LnkBDelete" runat="server" OnClick="SkyLnkBAMD_Click">删除</asp:LinkButton>
                        </li>
                        <li class="navstyle5">
                            <asp:LinkButton ID="LnkBExcel" runat="server" OnClick="SkyLnkBAMD_Click">导出</asp:LinkButton>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="divInfo">
        <div class="divInfoContext">
            <asp:Repeater runat="server" ID="rptInfo">
                <HeaderTemplate>
                    <table width="100%" border="0" cellpadding="0" cellspacing="1" class="tablist" onmouseover="changeto()"  onmouseout="changeback()">
                        <tr class="tablisttitle">
                            <td>
                                序号
                            </td>
                            <td>
                                <%=cHTMLFx%>
                            </td>
                            
                            <td>
                                操作模块
                            </td>
                            <td>
                                操作方法
                            </td>
                            <td>
                                用户编号
                            </td>
                            <td>
                                用户名称
                            </td>
                            <td>
                                角色类型
                            </td>
                            
                            <td>
                                日志标题
                            </td>
                            
                            <td>
                                操作IP
                            </td>
                            <td>
                                操作时间
                            </td>
                            <td>
                                查看
                            </td>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr class="tablisttr">
                        <td>
                            <%#Eval("ID") %>
                        </td>
                        <td>
                            <asp:CheckBox runat="server" ID="chkselect" ToolTip='<%#Eval("ID") %>' />
                        </td>
                        <td>
                            <%#Eval("LModelName")%>
                        </td>
                        <td>
                            <%#Eval("LTypeName")%>
                        </td>
                        <td>
                            <%#Eval("CreateBy")%>
                        </td>
                        <td>
                            <%#Eval("UserName")%>
                        </td>
                        <td>
                            <%#Eval("RoleIDName")%>
                        </td>
                        <td>
                            <%#Eval("LName")%>
                        </td>
               
                        <td>
                            <%#Eval("IP")%>
                        </td>
                        <td>
                            <%#UsToolsDateFormat(Eval("CreateDate"))%>
                        </td>
                        <td>
                            <%#cHTMLUrl(pDefault.sEditUrl +"?id="+Eval("ID").ToString(), "[查看]")%>
                        </td>
                        
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table></FooterTemplate>
            </asp:Repeater>
        </div>
    </div>
    <div class="divInfo">
        <webdiyer:aspnetpager id="Pager" runat="server" AlwaysShow="True" OnPageChanged="Pager_PageChanged"
         ShowCustomInfoSection="Left" ShowPageIndexBox="Always" showpageindex="False"
               CustomInfoHTML="共 <B>%PageCount%</B> 页，当前为第 <B>%CurrentPageIndex%</B> 页，每页 <B>%PageSize%</B> 条" 
               SubmitButtonText="" FirstPageText="首页" LastPageText="尾页" 
               LayoutType="Table" NextPageText="后一页" PrevPageText="前一页" ShowMoreButtons="False" 
               TextAfterPageIndexBox="页" TextBeforePageIndexBox="转到" 
               CustomInfoTextAlign="Left" HorizontalAlign="Right"></webdiyer:aspnetpager>
    </div>
    </form>
</body>
</html>
