﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="menumanager.aspx.cs" Inherits="BasicWeb.admin.basic.menumanager" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>系统菜单</title>
    <asp:Literal ID="litregscript" runat="server" Text=""></asp:Literal>
    <link href="../style/default/style.css" rel="stylesheet" type="text/css" />

    <script language="javascript" src="/admin/js/System.js" type="text/javascript"></script>

</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div id="divLoading">
                <img src="/admin/style/default/images/loading.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div id="tbMain">
        <div class="divTbg">
            <div class="divTbgL">
            </div>
            <div class="divTbgInfo">
                <div class="divTbgTitle">
                    系统菜单管理</div>
            </div>
        </div>
        <div class="divTbgF">
        </div>
        <div class="divTNavBG" id="topURL">
            <ul id="topURLul">
                <li class="divURLed" id="div1">
                    <div>
                        <img src="../style/default/images/mini_icons_046.gif" width="10" height="10" />
                        基本信息</div>
                </li>
            </ul>
        </div>
        <div id="mypanel">
            <div class="divInfoContext">
                <div class="divInfo">
                    <div class="divInfoblack">
                        <div class="divInfoTopList">
                            <div class="listLeft">
                                <div class="navtitle">
                                    系统菜单</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="divInfo">
                    <table width="100%" border="0" cellpadding="0" cellspacing="1" class="tablist" id="mainlist" onmouseover="changeto()"
                        onmouseout="changeback()">
                        <tr class="tablisttr">
                            <td class="tablisttitle">
                                菜单编号</td>
                            <td class="tabmanagertr">
                                <asp:TextBox ID="txtMenuID" runat="server" ToolTip="菜单编号" Text="" CssClass="1"></asp:TextBox>
                            </td>
                            <td class="tablisttitle">
                                父级菜单</td>
                            <td class="tabmanagertr">
                                <asp:DropDownList ID="drpParentID" runat="server" CssClass="1" ToolTip="父级菜单"> </asp:DropDownList></td>
                        </tr>
                        <tr class="tablisttr">
                            <td class="tablisttitle">
                                菜单名称</td>
                            <td class="tabmanagertr">
                                <asp:TextBox ID="txtMName" runat="server" CssClass="1" ToolTip="菜单名称"></asp:TextBox>
                            </td>
                            <td class="tablisttitle">
                                菜单提示</td>
                            <td class="tabmanagertr">
                                <asp:TextBox ID="txtMToolTip" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="tablisttr">
                            <td class="tablisttitle">
                                连接地址</td>
                            <td class="tabmanagertr">
                                <asp:TextBox ID="txtMURL" runat="server" CssClass="1" ToolTip="连接地址"></asp:TextBox>
                            </td>
                            <td class="tablisttitle">
                                显示顺序</td>
                            <td class="tabmanagertr">
                                <asp:TextBox ID="txtShowID" runat="server" CssClass="2" ToolTip="显示顺序"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="tablisttr">
                            <td class="tablisttitle">
                                打开方式</td>
                            <td class="tabmanagertr">
                        <asp:DropDownList runat="server" ID="drpMTarget" CssClass="1" ToolTip="打开方式">
                            <asp:ListItem Value="MTarget" Text="codeno"></asp:ListItem>
                        </asp:DropDownList>
                            </td>
                            <td class="tablisttitle">
                                是否展开</td>
                            <td class="tabmanagertr">
                        <asp:DropDownList runat="server" ID="drpMIsOpen" CssClass="1" ToolTip="是否展开">
                            <asp:ListItem Value="CodeYesNo" Text="codeno"></asp:ListItem>
                        </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="tablisttr">
                            <td class="tablisttitle">
                                打开图片
                            </td>
                            <td class="tabmanagertr">
                                <asp:TextBox ID="txtMOImages" runat="server"></asp:TextBox>
                                <a href="#" onclick="SkyOpen('txtMOImages');">[选择]</a>
                                <a href="#" onclick="SkyView('txtMOImages');">[查看]</a>
                            </td>
                            <td class="tablisttitle">
                                关闭图片
                            </td>
                            <td class="tabmanagertr">
                                <asp:TextBox ID="txtMCImages" runat="server"></asp:TextBox>
                                <a href="#" onclick="SkyOpen('txtMCImages');">[选择]</a>
                                <a href="#" onclick="SkyView('txtMCImages');">[查看]</a>
                            </td>
                        </tr>
                        </table>
                    <div class="divSave">
                        <asp:Button ID="btnSubmit" runat="server" Text="保存(S)" 
                            CssClass="SkyButtonFocus" onclick="btnSubmit_Click" />
                        <asp:Button ID="btnDelete" runat="server" Text="删除(D)" CssClass="SkyButtonFocus" 
                            onclick="btnDelete_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </form>
</body>
</html>
