﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="departmentmanager.aspx.cs" Inherits="BasicWeb.admin.basic.departmentmanager" %>


<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>部门</title>
    <asp:Literal ID="litregscript" runat="server" Text=""></asp:Literal>
    <link href="../style/default/style.css" rel="stylesheet" type="text/css" />

    <script language="javascript" src="/admin/js/System.js" type="text/javascript"></script>
    <script  language="javascript" src="/editor/xheditor.js" type="text/javascript" ></script>
<script language="javascript" type="text/javascript">
    function opJsTree(txt, txtValue) {
        var para = "/admin/basic/departmenttreeselect.aspx?para=1&list=" + $("#" + txtValue).val();
        UsOpenDialogTree(para, txt, txtValue);
    }
   
 </script>
</head>
<body onload="win_onLoad();">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div id="divLoading">
                <img src="/admin/style/default/images/loading.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div id="tbMain">
        <div class="divTbg">
            <div class="divTbgL">
            </div>
            <div class="divTbgInfo">
                <div class="divTbgTitle">
                    部门管理</div>
            </div>
        </div>
        <div class="divTbgF">
        </div>
        <div class="divTNavBG" id="topURL">
            <ul id="topURLul">
                <li class="divURLed" id="div1">
                    <div>
                        <img src="../style/default/images/mini_icons_046.gif" width="10" height="10" />
                        基本信息</div>
                </li>
            </ul>
        </div>
        <div id="mypanel">
            <div class="divInfoContext">
                <div class="divInfo">
                    <div class="divInfoblack">
                        <div class="divInfoTopList">
                            <div class="listLeft">
                                <div class="navtitle">
                                    部门管理</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="divInfo">
                   <table width="100%" border="0" cellpadding="0" cellspacing="1" class="tablist" id="mainlist" onMouseOver="changeto()" onmouseout="changeback()">
<tr class="tablisttr">
<td class="tablisttitle">部门编号</td><td class="tabmanagertr"><asp:TextBox ID="txtDeptID" runat="server" ToolTip="部门编号" Text="" CssClass="1"></asp:TextBox></td>
<td class="tablisttitle">部门名称</td><td class="tabmanagertr"><asp:TextBox ID="txtDeptName" runat="server" ToolTip="部门名称" Text="" CssClass="1"></asp:TextBox></td>
</tr>
<tr class="tablisttr">
<td class="tablisttitle">显示顺序</td><td class="tabmanagertr"><asp:TextBox ID="txtShowID" runat="server" ToolTip="显示顺序" Text="1" CssClass="2"></asp:TextBox></td>
<td class="tablisttitle">上级部门</td><td class="tabmanagertr"><asp:TextBox ID="txtParentDeptName" runat="server" ToolTip="上级部门" Text="『顶级部门』"></asp:TextBox>
<span runat="server" id="Span1"><a href="javascript:opJsTree('txtParentDeptName','txtParentDeptID');">[选择]</a></span>
<div style="display:none"><asp:TextBox ID="txtParentDeptID" runat="server" ToolTip="部门编号" Text="0"></asp:TextBox></div>


</td>
</tr>
<tr class="tablisttr">
<td class="tablisttitle">部门负责人</td><td class="tabmanagertr"><asp:TextBox ID="txtDUser" runat="server" ToolTip="部门负责人" Text=""></asp:TextBox></td>
<td class="tablisttitle">部门电话</td><td class="tabmanagertr"><asp:TextBox ID="txtDTel" runat="server" ToolTip="编部门电话" Text=""></asp:TextBox></td>
</tr>
<tr class="tablisttr">
<td class="tablisttitle">是否可评价</td><td class="tabmanagertr"><asp:DropDownList runat="server" ID="drpDIsPJ" ToolTip="是否可评价">
                                    <asp:ListItem Value="CodeYesNo" Text="codeno"></asp:ListItem>
                                </asp:DropDownList></td>
<td class="tablisttitle">部门状态</td><td class="tabmanagertr"><asp:DropDownList runat="server" ID="drpDState" ToolTip="部门状态">
                                    <asp:ListItem Value="CodeState" Text="codeno"></asp:ListItem>
                                </asp:DropDownList></td>
</tr>
<tr class="tablisttr">
<td class="tablisttitle">部门照片</td><td class="tabmanagertr">
<asp:TextBox ID="txtUserPic" runat="server" ToolTip="部门照片" Text=""></asp:TextBox>
<span runat="server" id="divClose">
<a href="#" onclick="SkyOpen('txtUserPic');">[选择]</a>
<a href="#" onclick="SkyView('txtUserPic');">[查看]</a></span></td>
<td class="tablisttitle">部门类型</td><td class="tabmanagertr"><asp:DropDownList runat="server" ID="drpDeptType" ToolTip="部门类型">
                                    <asp:ListItem Value="DeptType" Text="codeno"></asp:ListItem>
                                </asp:DropDownList></td>
</tr>
<tr class="tablisttr">
<td class="tablisttitle">部门备注</td><td class="tabmanagertr" colspan="3"><asp:TextBox ID="txtDInfo" runat="server" ToolTip="部门备注" Text="" TextMode="MultiLine" Height="100px" Width="100%" class="xheditor {upLinkUrl:s_upLinkUrl,upLinkExt:s_upLinkExt,upImgUrl:s_upLinkUrl,upImgExt:s_upImgExt,upFlashUrl:s_upLinkUrl,upFlashExt:s_upFlashExt,upMediaUrl:s_upLinkUrl,upMediaExt:s_upMediaExt}"></asp:TextBox></td>
</tr>
</table>
                    <div class="divSave">
                        <asp:Button ID="btnSubmit" runat="server" Text="保存(S)" 
                            CssClass="SkyButtonFocus" onclick="btnSubmit_Click" />
                        <asp:Button ID="btnDelete" runat="server" Text="删除(D)" 
                            CssClass="SkyButtonFocus" onclick="btnDelete_Click" 
                             />
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </form>
</body>
</html>