﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="userpostmanager.aspx.cs" Inherits="BasicWeb.admin.basic.userpostmanager" %>


<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>岗位</title>
    <asp:Literal ID="litregscript" runat="server" Text=""></asp:Literal>
    <link href="../style/default/style.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery/jquery.js" type="text/javascript"></script>
    <script language="javascript" src="/admin/js/System.js" type="text/javascript"></script>
<script  language="javascript" src="/editor/xheditor.js" type="text/javascript" ></script>

<style type="text/css">
#cblPUserPostID input {
	width:13px;
	height:13px;!important;
	margin:0px;!important;
	padding:0px;!important;
	border: 0px none #009999;
}
</style>

<script language="javascript" type="text/javascript">
    function opJsTree(txt, txtValue) {
        var para = "/admin/basic/departmenttreeselect.aspx?para=2&list=" + $("#" + txtValue).val();
         UsOpenDialogTree(para, txt, txtValue);
     }
     function opJsTree1(txt, txtValue) {
         var para = "/admin/basic/departmenttreeselect.aspx?para=1&list=" + $("#" + txtValue).val();
         UsOpenDialogTree(para, txt, txtValue);
     }
     function opJsTree2(txt, txtValue) {
         var para = "/admin/basic/departmenttreeselect.aspx?para=2&list=" + $("#" + txtValue).val();
         UsOpenDialogTree(para, txt, txtValue);
         $id("btnchange").click();
     }
 </script>
 
 
  <script language="javascript" type="text/javascript" >
      $(document).ready(function() {
          $("#drpPosType").change(function() {
              var vInfo = $("#drpPosType").val();
              //alert(vInfo);
              if (vInfo == "0") {
                  $("#txtDeptName").val("-")
                  $("#txtDeptID").val("");
                  //$("#aselectdepartment").attr("unable",true);
                  $("#aselectdepartment").hide();

              }
              else {
                  //$("#aselectdepartment").attr("unable", false);
                  $("#aselectdepartment").show();
              }

              //return window.confirm("您确实要提交对自己评价数据吗？");
          });

          if ($("#drpPosType").val() == "0") { $("#aselectdepartment").hide(); }
      });

      
    </script>


</head>
<body onload="win_onLoad();">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div id="divLoading">
                <img src="/admin/style/default/images/loading.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div id="tbMain">
        <div class="divTbg">
            <div class="divTbgL">
            </div>
            <div class="divTbgInfo">
                <div class="divTbgTitle">
                    岗位管理</div>
            </div>
        </div>
        <div class="divTbgF">
        </div>
        <div class="divTNavBG" id="topURL">
            <ul id="topURLul">
                <li class="divURLed" id="div1">
                    <div>
                        <img src="../style/default/images/mini_icons_046.gif" width="10" height="10" />
                        基本信息</div>
                </li>
            </ul>
        </div>
        <div id="mypanel">
            <div class="divInfoContext">
                <div class="divInfo">
                    <div class="divInfoblack">
                        <div class="divInfoTopList">
                            <div class="listLeft">
                                <div class="navtitle">
                                    岗位管理</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="divInfo">
                  <table width="100%" border="0" cellpadding="0" cellspacing="1" class="tablist" id="mainlist" onMouseOver="changeto()" onmouseout="changeback()">
<tr class="tablisttr">
<td class="tablisttitle">岗位编号</td><td class="tabmanagertr"><asp:TextBox ID="txtPostID" runat="server" ToolTip="岗位编号" Text="" CssClass="1"></asp:TextBox></td>
<td class="tablisttitle">岗位名称</td><td class="tabmanagertr"><asp:TextBox ID="txtPostName" runat="server" ToolTip="岗位名称" Text="" CssClass="1"></asp:TextBox></td>
</tr>
<tr class="tablisttr">
<td class="tablisttitle">岗位类型</td><td class="tabmanagertr"><asp:DropDownList runat="server" ID="drpPosType" ToolTip="岗位类型">
                                    <asp:ListItem Value="PosType" Text="codeno"></asp:ListItem>
                                </asp:DropDownList></td>
<td class="tablisttitle">隶属部门</td><td class="tabmanagertr"> <asp:TextBox ID="txtDeptName" runat="server" ToolTip="隶属部门" Text=""></asp:TextBox>
<a href="javascript:opJsTree1('txtDeptName','txtDeptID');" id="aselectdepartment">[选择]</a>
<div style="display:none"><asp:TextBox ID="txtDeptID" runat="server" ToolTip="部门编号" Text=""></asp:TextBox></div>
</td>
</tr>
<tr class="tablisttr">
<td class="tablisttitle">岗位状态</td><td class="tabmanagertr"><asp:DropDownList runat="server" ID="drpPState" ToolTip="岗位状态">
                                    <asp:ListItem Value="CodeState" Text="codeno"></asp:ListItem>
                                </asp:DropDownList></td>
<td class="tablisttitle">自评套包</td><td class="tabmanagertr"><asp:DropDownList runat="server" ID="drpPSelfPack" ToolTip="自评套包"></asp:DropDownList></td>
</tr>
<tr class="tablisttr">
<td class="tablisttitle">可评部门</td><td class="tabmanagertr" colspan="3"><asp:TextBox ID="txtPDepartment" runat="server" ToolTip="可评部门" Text=""  Width="90%"  CssClass="1"></asp:TextBox><a href="javascript:opJsTree('txtPDepartment','txtPDepartmentID');">[选择]</a></td>
</tr>
<tr class="tablisttr">
<td class="tablisttitle">部门套包</td><td class="tabmanagertr"><asp:DropDownList runat="server" ID="drpPDepartmentPack" ToolTip="部门套包"></asp:DropDownList>

    </td>
<td class="tablisttitle">可评本部门</td><td class="tabmanagertr"><asp:DropDownList runat="server" ID="drpPDepartmentSelf" ToolTip="可评本部门">
                                    <asp:ListItem Value="CodeYesNo" Text="codeno"></asp:ListItem>
                                </asp:DropDownList></td>
</tr>
<tr class="tablisttr"   style="display:none">
<td class="tablisttitle">&nbsp;</td><td class="tabmanagertr"><asp:TextBox ID="txtPDepartmentID" runat="server" ToolTip="可评部门编号" Text=""></asp:TextBox>
</td>
<td class="tablisttitle">&nbsp;</td><td class="tabmanagertr">
    <asp:TextBox ID="txtPUserDepartmentID" runat="server" ToolTip="可评人员部门编号" 
        Text="" ></asp:TextBox>
                        <asp:Button ID="btnchange" runat="server" Text="调整" 
                            CssClass="SkyButtonFocus" onclick="btnchange_Click" />
                        </td>
</tr>
<tr class="tablisttr">
<td class="tablisttitle">&nbsp;</td><td class="tabmanagertr" colspan="3">&nbsp;</td>
</tr>
<tr class="tablisttr">
<td class="tablisttitle">可评人员部门</td><td class="tabmanagertr" colspan="3"><asp:TextBox ID="txtPUserDepartment" runat="server" ToolTip="可评人员部门" Text=""  Width="90%" CssClass="1"></asp:TextBox><a href="javascript:opJsTree2('txtPUserDepartment','txtPUserDepartmentID');">[选择]</a></td>
</tr>
<tr class="tablisttr">
<td class="tablisttitle">可评岗位<br /><asp:CheckBox ID="chkSelectRole" runat="server" 
        AutoPostBack="True" oncheckedchanged="chkSelectRole_CheckedChanged" Text="全选" />
    </td><td class="tabmanagertr" colspan="3">
<asp:CheckBoxList runat="server" ID="cblPUserPostID" ToolTip="可评岗位" 
        RepeatColumns="4" RepeatDirection="Horizontal" Width="100%"></asp:CheckBoxList></td>
</tr>
<tr class="tablisttr">
<td class="tablisttitle">用户套包</td><td class="tabmanagertr"><asp:DropDownList runat="server" ID="drpPUserPack" ToolTip="用户套包"></asp:DropDownList>
</td>
<td class="tablisttitle">可评本部门角色</td><td class="tabmanagertr"><asp:DropDownList runat="server" ID="drpPUserSelf" ToolTip="可评本部门角色">
                                    <asp:ListItem Value="CodeYesNo" Text="codeno"></asp:ListItem>
                                </asp:DropDownList></td>
</tr>
<tr class="tablisttr">
<td class="tablisttitle">岗位说明</td><td class="tabmanagertr" colspan="3"><asp:TextBox ID="txtPostInfo" runat="server" ToolTip="岗位说明" Text="" TextMode="MultiLine" Height="100px" Width="100%" class="xheditor {upLinkUrl:s_upLinkUrl,upLinkExt:s_upLinkExt,upImgUrl:s_upLinkUrl,upImgExt:s_upImgExt,upFlashUrl:s_upLinkUrl,upFlashExt:s_upFlashExt,upMediaUrl:s_upLinkUrl,upMediaExt:s_upMediaExt}"></asp:TextBox></td>
</tr>
</table>
                    <div class="divSave">
                        <asp:Button ID="btnSubmit" runat="server" Text="保存(S)" 
                            CssClass="SkyButtonFocus" onclick="btnSubmit_Click" />
                        <asp:Button ID="btnExit" runat="server" Text="取消(C)" CssClass="SkyButtonFocus" 
                            onclick="btnSubmit_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>
  
    </form>
</body>
</html>
