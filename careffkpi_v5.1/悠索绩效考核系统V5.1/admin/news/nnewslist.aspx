﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="nnewslist.aspx.cs" Inherits="BasicWeb.admin.news.nnewslist" %>



<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>新闻</title>
    <link href="../style/default/style.css" rel="stylesheet" type="text/css" />

    <script language="javascript" src="/admin/js/System.js" type="text/javascript"></script>
    <script language="javascript" src="/admin/My97DatePicker/WdatePicker.js" type="text/jscript"></script>
</head>
<body>
    <form id="Form1" method="post" runat="server">
    <div class="divTbg">
      <div class="divTbgL"></div>
      <div class="divTbgInfo">
        <div class="divTbgTitle">新闻管理</div>
      </div>
    </div>
    <div class="divTbgF"></div>
    <div class="divSearchBoth">
        <div class="divInfo">
            <div class="divInfoblack">
                <div class="divInfoTopList">
                    <div class="listLeft">
                        <div class="navsearchtitle">
                            新闻搜索</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="divInfo">
            <div class="divSearch">
                <div id="searchBox" class="clearsearch">
                     <p>
                        <label for="txtNTitle">
                            新闻标题：</label>
                        <asp:TextBox runat="server" ID="txtNTitle"></asp:TextBox>
                    </p>
 
                    
                    
                    
                    <p >
                        <label for="drpNNewsType">
                            新闻分类：</label>
                        <asp:DropDownList runat="server" ID="drpNNewsType">
                            <asp:ListItem Value="NNewsType" Text="code"></asp:ListItem>
                        </asp:DropDownList>
                    </p>
                    
                    
                    <p>
                        <label for="drpNState">
                            新闻状态：</label>
                        <asp:DropDownList runat="server" ID="drpNState">
                            <asp:ListItem Value="NNewsState" Text="code"></asp:ListItem>
                        </asp:DropDownList>
                    </p>
                    
                    
                    <p>
                        <label for="drpNNewsGrade">
                            新闻等级：</label>
                        <asp:DropDownList runat="server" ID="drpNNewsGrade">
                            <asp:ListItem Value="NNewsGrade" Text="code"></asp:ListItem>
                        </asp:DropDownList>
                    </p>
                    
                    <p>
                        <label for="drpNNewsSType">
                            发送类型：</label>
                        <asp:DropDownList runat="server" ID="drpNNewsSType">
                            <asp:ListItem Value="NNewsSType" Text="code"></asp:ListItem>
                        </asp:DropDownList>
                    </p>
                    <p>
                    
                        <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="确认搜索" CssClass="SkyButtonFocus" />
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="divInfo">
        <div class="divInfoblack">
            <div class="divInfoTopList">
                <div class="listLeft">
                    <div class="navtitle">
                        新闻详细列表</div>
                </div>
                <div class="listRight">
                    <ul>
                        <li class="navstyle1">
                            <input type="checkbox" name="chkselect" id="chkselect" onclick="skySelectCheckList();"/>
                            <label for="chkselect">全选</label> </li>
                        <li class="navstyle2">
                            <asp:LinkButton ID="LnkBAdd" runat="server" OnClick="SkyLnkBAMD_Click" >添加</asp:LinkButton>
                        </li>
                        <li class="navstyle3">
                            <asp:LinkButton ID="LnkBEdit" runat="server" OnClick="SkyLnkBAMD_Click" >编辑</asp:LinkButton>
                        </li>
                        <li class="navstyle4">
                            <asp:LinkButton ID="LnkBDelete" runat="server" OnClick="SkyLnkBAMD_Click">删除</asp:LinkButton>
                        </li>
                        <li class="navstyle5">
                            <asp:LinkButton ID="LnkBExcel" runat="server" OnClick="SkyLnkBAMD_Click">导出</asp:LinkButton>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="divInfo">
        <div class="divInfoContext">
            <asp:Repeater runat="server" ID="rptInfo">
                <HeaderTemplate>
                    <table width="100%" border="0" cellpadding="0" cellspacing="1" class="tablist" onmouseover="changeto()"  onmouseout="changeback()">
                       <tr class="tablisttitle">
                                <td>
                                    序号
                                </td>
                                <td>
                                    <%=cHTMLFx%>
                                </td>
                                <td align="center">
                                    新闻标题
                                </td>
                                <td align="center">
                                    新闻状态
                                </td>
                                <td align="center">
                                    新闻分类
                                </td>
                                <td align="center">
                                    新闻等级
                                </td>
                                <td align="center">
                                    发送类型
                                </td>
                                <td align="center">
                                    建立人
                                </td>
                                
                                <td align="center">
                                    建立时间
                                </td>
                                <td>
                                    修改
                                </td>
                            </tr>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr class="tablisttr">
                            <td>
                                <%#Eval("ID") %>
                            </td>
                            <td>
                                <asp:CheckBox runat="server" ID="chkselect" ToolTip='<%#Eval("ID") %>' />
                            </td>
                            <td align="center">
                                <%#Eval("NTitle")%>
                            </td>
                           <td align="center">
                                <%#Eval("NStateName")%>
                            </td>
                             <td align="center">
                                <%#Eval("NTypeName")%>
                            </td>
                            <td align="center">
                                <%#Eval("NGradeName")%>
                            </td>
                            <td align="center">
                                <%#Eval("NSTypeName")%>
                            </td>
                            <td align="center">
                                <%#Eval("CreateBy")%>
                            </td>
                            
                            <td align="center">
                               <%#UsToolsDateFormat(Eval("CreateDate"))%>
                            </td>
                            <td>
                                <%#cHTMLUrl(pDefault.sEditUrl + "?id=" + Eval("ID").ToString(), "[修改]")%>
                            </td>
                        </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table></FooterTemplate>
            </asp:Repeater>
        </div>
    </div>
    <div class="divInfo">
        <webdiyer:aspnetpager id="Pager" runat="server" AlwaysShow="True" OnPageChanged="Pager_PageChanged"
         ShowCustomInfoSection="Left" ShowPageIndexBox="Always" showpageindex="False"
               CustomInfoHTML="共 <B>%PageCount%</B> 页，当前为第 <B>%CurrentPageIndex%</B> 页，每页 <B>%PageSize%</B> 条" 
               SubmitButtonText="" FirstPageText="首页" LastPageText="尾页" 
               LayoutType="Table" NextPageText="后一页" PrevPageText="前一页" ShowMoreButtons="False" 
               TextAfterPageIndexBox="页" TextBeforePageIndexBox="转到" 
               CustomInfoTextAlign="Left" HorizontalAlign="Right"></webdiyer:aspnetpager>
    </div>
    </form>
</body>
</html>
