﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="nnewstreeselect.aspx.cs" Inherits="BasicWeb.admin.news.nnewstreeselect" %>



<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>选择</title>
<link href="../style/default/style.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="/admin/js/System.js" type="text/javascript"></script>
<link rel="StyleSheet" href="/admin/js/tree/dtree.css" type="text/css" />
<script type="text/javascript" src="/admin/js/tree/dtree3.js"></script>
<asp:Literal ID="litScript" runat="server"></asp:Literal>

<style type="text/css"> 
input[type="checkbox"] {
	width:12px;
	height:12px;
	margin:0px;
	padding:0px;
	border: 0px none #009999;
}
</style><script language="javascript" type="text/javascript">
    function SavegetSelectedOne() {
        var sIDlist = "";
        var sNamelist = ""; ;
        var dom = document.getElementsByTagName("*");
        var el = event.srcElement ? event.srcElement : event.target;
        for (i = 0; i < dom.length; i++) {
            if (dom[i].tagName == "INPUT" && dom[i].type.toLowerCase() == "checkbox") {
                nName = dom[i].id;
                if (dom[i].checked) {
                    sIDlist += dom[i].value + ",";
                    sNamelist += dom[i].title + ",";
                }

            }
        }
        if (sIDlist == "") {
            alert("您必须至少选择一项!");
        }
        else {
            sIDlist = sIDlist.substr(0, sIDlist.length - 1);

            var sZ = sIDlist.split(',');
            if (sZ.length > 1) {
                alert("对不起，您只能选择一项!");
                return false;
            }
            
            sNamelist = sNamelist.substr(0, sNamelist.length - 1);
            returnValue = sIDlist + "#" + sNamelist;
            window.close();
        }
        //        alert(sIDlist + sNamelist);
        return false;
    }
    
    function SavegetSelected() {
        var sIDlist = "";
        var sNamelist = ""; ;
        var dom = document.getElementsByTagName("*");
        var el = event.srcElement ? event.srcElement : event.target;
        for (i = 0; i < dom.length; i++) {
            if (dom[i].tagName == "INPUT" && dom[i].type.toLowerCase() == "checkbox") {
                nName = dom[i].id;
                if (dom[i].checked) {
                    sIDlist += dom[i].value + ",";
                    sNamelist += dom[i].title + ",";
                }
                //                if (nName.indexOf("chkselect") != -1) {
                //                    if (dom[i].disabled == "")
                //                        dom[i].checked = !dom[i].checked;
                //                }

            }
        }
        if (sIDlist == "") {
            alert("您必须至少选择一项");
        }
        else {
            sIDlist = sIDlist.substr(0, sIDlist.length - 1);
            sNamelist = sNamelist.substr(0, sNamelist.length - 1);


            returnValue = sIDlist + "#" + sNamelist;
            window.close();
        }
        //        alert(sIDlist + sNamelist);
        return false;
    }
</script>


</head>
<body>
<form id="Form1" method="post" runat="server">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" height="100%">
    <tr>
      <td width="160" valign="top"><div class="divTbg">
          <div class="divTbgL"></div>
          <div class="divTbgInfo">
            <div class="divTbgTitle">选择</div>
          </div>
        </div>
        <div class="divTbgF"></div>
        <div class="divSearchBoth">
          
          <div class="divInfo">
             <div class="divAutoHeight" id="ScroLeft"  style="height:400px"><%=TreeHTML %></div>
          </div>
           <%=SelectHTML %>
        </div>
          <div class="divSave"><asp:Button ID="btnSubmit" runat="server" Text="保存(S)" CssClass="SkyButtonFocus"
                             /></div>
        </td>
        </td>
      
    </tr>
  </table>
</form>
</body>
</html>
