﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="nnewsviewmanager.aspx.cs" Inherits="BasicWeb.admin.news.nnewsviewmanager" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>新闻</title>
    <asp:Literal ID="litregscript" runat="server" Text=""></asp:Literal>
    <link href="../style/default/style.css" rel="stylesheet" type="text/css" />

    <script language="javascript" src="/admin/js/System.js" type="text/javascript"></script>
 <script language="javascript" src="/admin/My97DatePicker/WdatePicker.js" type="text/jscript"></script>
 <script  language="javascript" src="/editor/xheditor.js" type="text/javascript" ></script>
 


    <script src="../js/jquery/jquery.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript">
    function opJsTree1(txt, txtValue) {
        //选择角色
        var NSType = $("#drpNSType").val();
        if (NSType == "0") {
            alert("全部人员无需选择角色");
        }
        else {
            //1=多选 2=单选
            var para = "nnewstreeselect.aspx?para=" + "1" + "&list=" + $("#" + txtValue).val();
            UsOpenDialogTree(para, txt, txtValue);
        }
    }
    function opJsTree2(txt, txtValue) {

        var NRoleID = $("#txtNRoleID").val();
        if (NRoleID == "") {
            alert("请选择一个角色才能选择人员");
        }
        else {
            var para = "nnewstreeselect.aspx?para=" + NRoleID + "&list=" + $("#" + txtValue).val();
            UsOpenDialogTree(para, txt, txtValue);
        }
    }

    function jsNSType() {
        var NSType = $("#drpNSType").val();

        if (NSType == "0") {
            $("#trselectRoleID").hide();
            $("#trselectUserID").hide();

        }
        if (NSType == "1") {
            $("#trselectRoleID").show();
            $("#trselectUserID").hide();

        }
        if (NSType == "2") {
            $("#trselectRoleID").show();
            $("#trselectUserID").show();

        }
    }
    function jsNSTypeClear() {
        $("#txtNRoleID").val("");
        $("#txtNRoleIDName").val("");
        $("#txtNUserID").val("");
        $("#txtNUserIDName").val("");
    }


    $(document).ready(function() {
        jsNSType();
        $("#drpNSType").change(function() {
            jsNSType();
            jsNSTypeClear();
        });
    });

 </script>
 
 

</head>
<body onload="win_onLoad();">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div id="divLoading">
                <img src="/admin/style/default/images/loading.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div id="tbMain">
        <div class="divTbg">
            <div class="divTbgL">
            </div>
            <div class="divTbgInfo">
                <div class="divTbgTitle">
                    新闻管理</div>
            </div>
        </div>
        <div class="divTbgF">
        </div>
        <div class="divTNavBG" id="topURL">
            <ul id="topURLul">
                <li class="divURLed" id="div1">
                    <div>
                        <img src="../style/default/images/mini_icons_046.gif" width="10" height="10" />
                        基本信息</div>
                </li>
            </ul>
        </div>
        <div id="mypanel">
            <div class="divInfoContext">
                <div class="divInfo">
                    <div class="divInfoblack">
                        <div class="divInfoTopList">
                            <div class="listLeft">
                                <div class="navtitle">
                                    新闻</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="divInfo">
                 


<table width="100%" border="0" cellpadding="0" cellspacing="1" class="tablist" id="mainlist" onMouseOver="changeto()" onmouseout="changeback()">
                      
                      
                      
                      
                      
                      
                        <tr class="tablisttr">
                          <td class="tablisttitle">新闻标题 </td>
                          <td align="left" valign="top" class="tabmanagertr">
                            <asp:TextBox ID="txtNTitle" runat="server"  CssClass="1" ToolTip="新闻标题" Width="90%"></asp:TextBox>                      </td>
                        </tr>
                        <tr class="tablisttr">
                          <td class="tablisttitle">新闻副标题</td>
                          <td align="left" valign="top" class="tabmanagertr">
                            <asp:TextBox ID="txtNCTitle" runat="server"   ToolTip="新闻副标题" Width="90%"></asp:TextBox>                      </td>
                        </tr>
                        
                         <tr class="tablisttr">
                          <td class="tablisttitle">新闻附件 </td>
                          <td align="left" valign="top" class="tabmanagertr">
                            <asp:TextBox ID="txtNAddr" runat="server" ToolTip="新闻附件"  Width="90%"></asp:TextBox>
                            &nbsp;<a href="#" onclick="SkyView('txtNAddr');">[查看]</a></td>
                        </tr>
                        <tr class="tablisttr">
                        
                          <td class="tablisttitle">新闻分类</td>
                          <td align="left" valign="top" class="tabmanagertr">
                        <asp:DropDownList runat="server" ID="drpNType">
                            <asp:ListItem Value="NNewsType" Text="codeno"></asp:ListItem>
                        </asp:DropDownList>                            </td>
                        </tr>
                        <tr class="tablisttr">
                        
                          <td class="tablisttitle">新闻状态</td>
                          <td align="left" valign="top" class="tabmanagertr">
                              <asp:DropDownList runat="server" ID="drpNState">
                            <asp:ListItem Value="NNewsState" Text="codeno"></asp:ListItem>
                        </asp:DropDownList></td>
                        </tr>
                        <tr class="tablisttr">
                          <td class="tablisttitle">新闻等级</td>
                          <td align="left" valign="top" class="tabmanagertr">
                        <asp:DropDownList runat="server" ID="drpNGrade">
                            <asp:ListItem Value="NNewsGrade" Text="codeno"></asp:ListItem>
                        </asp:DropDownList>                            </td>
                        </tr>
                       
                       
                        <tr class="tablisttr">
                            <td class="tablisttitle">发送类型 </td>
                            <td align="left" valign="top" class="tabmanagertr">
                        <asp:DropDownList runat="server" ID="drpNSType">
                            <asp:ListItem Value="NNewsSType" Text="codeno"></asp:ListItem>
                        </asp:DropDownList>                            </td>
                      </tr>
                      
                      
                      
                      <tr class="tablisttr" runat="server" id="trselectRoleID">
                            <td class="tablisttitle">选择角色</td>
                            <td align="left" valign="top" class="tabmanagertr">
                                <asp:TextBox ID="txtNRoleIDName" runat="server" ToolTip="角色名称" Text="" Width="90%"></asp:TextBox>
                               </td>
                      </tr>
                      
                      <tr class="tablisttr"  runat="server" id="trselectUserID">
                            <td class="tablisttitle">选择人员</td>
                            <td align="left" valign="top" class="tabmanagertr">
                                <asp:TextBox ID="txtNUserIDName" runat="server" ToolTip="用户名称" Text=""  Width="90%"></asp:TextBox>
                                </td>
                      </tr>
                        
                        <tr class="tablisttr">
                          <td class="tablisttitle">新闻内容 </td>
                          <td align="left" valign="top" class="tabmanagertr"><asp:TextBox ID="txtNInfo" runat="server" ToolTip="新闻内容" Width="100%" TextMode="MultiLine" Height="200px" class="xheditor {upLinkUrl:s_upLinkUrl,upLinkExt:s_upLinkExt,upImgUrl:s_upLinkUrl,upImgExt:s_upImgExt,upFlashUrl:s_upLinkUrl,upFlashExt:s_upFlashExt,upMediaUrl:s_upLinkUrl,upMediaExt:s_upMediaExt}"></asp:TextBox></td>
                        </tr>
                    </table>


                    <div class="divSave">
                        <asp:Button ID="btnSubmit" runat="server" Text="保存(S)" 
                            CssClass="SkyButtonFocus" onclick="btnSubmit_Click" />
                        <asp:Button ID="btnExit" runat="server" Text="取消(C)" CssClass="SkyButtonFocus" 
                            onclick="btnSubmit_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </form>
</body>
</html>
