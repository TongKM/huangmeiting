﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="usright.aspx.cs" Inherits="BasicWeb.admin.usright" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>右侧区域</title>
    <link href="style/default/style.css" rel="stylesheet" type="text/css" />

    <script language="javascript" src="/admin/js/system.js" type="text/javascript"></script>

</head>
<body>
    <form id="Form1" method="post" runat="server">
    <div class="divSearchBoth">
        <div class="divInfo">
            <div class="divInfoblack">
                <div class="divInfoTopList">
                    <div class="listLeft">
                        <div class="navsearchtitle">
                            登录日志</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="divInfo">
            <div class="divSearch">
                <div class="newsearch">
                    <p>
                        <label for="txtID">
                            登录名称：</label>
                        <asp:TextBox ID="txtID" runat="server"></asp:TextBox>
                    </p>
                    <p>
                        <label for="txtName">
                            用户名称：</label>
                        <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                    </p>
                    <p>
                        <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="确认搜索" CssClass="SkyButtonFocus" />
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="divInfo">
        <div class="divInfoblack">
            <div class="divInfoTopList">
                <div class="listLeft">
                    <div class="navtitle">
                        登录日志详细列表</div>
                </div>
                <div class="listRight">
                    <ul>
                        <li class="navstyle1">
                            <input type="checkbox" name="chkselect" id="chkselect" onclick="skySelectCheckList();"/>
                            <label for="chkselect">全选</label> </li>
                        <li class="navstyle2">
                            <asp:LinkButton ID="LnkBAdd" runat="server" OnClick="SkyLnkBAMD_Click">添加</asp:LinkButton>
                        </li>
                        <li class="navstyle3">
                            <asp:LinkButton ID="LnkBEdit" runat="server" OnClick="SkyLnkBAMD_Click">编辑</asp:LinkButton>
                        </li>
                        <li class="navstyle4">
                            <asp:LinkButton ID="LnkBDelete" runat="server" OnClick="SkyLnkBAMD_Click">删除</asp:LinkButton>
                        </li>
                        <li class="navstyle5">
                            <asp:LinkButton ID="LnkBExcel" runat="server" OnClick="SkyLnkBAMD_Click">导出</asp:LinkButton>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="divInfo">
        <div class="divInfoContext">
            <asp:Repeater runat="server" ID="rptInfo">
                <HeaderTemplate>
                    <table width="100%" border="0" cellpadding="0" cellspacing="1" class="tablist" onmouseover="changeto()"  onmouseout="changeback()">
                        <tr class="tablisttitle">
                            <td width="4%">
                                序号
                            </td>
                            <td width="4%">
                                <%=cHTMLFx%>
                            </td>
                            <td>
                                登录名称
                            </td>
                            <td>
                                用户名称
                            </td>
                            <td>
                                用户角色
                            </td>
                            <td>
                                登录系统
                            </td>
                            <td>
                                浏览器
                            </td>
                            <td>
                                登录时间
                            </td>
                            <td>
                                IP地址
                            </td>
                            <td>
                                操作
                            </td>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr class="tablisttr">
                        <td>
                            <%#Eval("ID") %>
                        </td>
                        <td>
                            <asp:CheckBox  runat="server" ID="chkselect" ToolTip='<%#Eval("ID") %>'/>
                        </td>
                        <td>
                            <%#Eval("UserID") %>
                        </td>
                        <td>
                            <%#Eval("UserName") %>
                        </td>
                        <td>
                            <%#Eval("RoleType") %>
                        </td>
                        <td>
                           <%#Eval("UserSys") %>
                        </td>
                        <td>
                           <%#Eval("UserIE") %>
                        </td>
                        <td>
                           <%#UsToolsDateFormat(Eval("CreateDate"))%>
                        </td>
                        <td>
                            <%#Eval("IP") %>
                        </td>
                        <td>
                            <%#cHTMLUrl(pDefault.sEditUrl +"?id="+Eval("ID").ToString(), "修改")%>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table></FooterTemplate>
            </asp:Repeater>
        </div>
    </div>
    <div class="divInfo">
        <webdiyer:aspnetpager id="Pager" runat="server" AlwaysShow="True" OnPageChanged="Pager_PageChanged"
         ShowCustomInfoSection="Left" ShowPageIndexBox="Always" showpageindex="False"
               CustomInfoHTML="共 <B>%PageCount%</B> 页，当前为第 <B>%CurrentPageIndex%</B> 页，每页 <B>%PageSize%</B> 条" 
               SubmitButtonText="" FirstPageText="首页" LastPageText="尾页" 
               LayoutType="Table" NextPageText="后一页" PrevPageText="前一页" ShowMoreButtons="False" 
               TextAfterPageIndexBox="页" TextBeforePageIndexBox="转到" 
               CustomInfoTextAlign="Left" HorizontalAlign="Right"></webdiyer:aspnetpager>
    </div>
    </form>
</body>
</html>
