﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="exportuserscore.aspx.cs" Inherits="BasicWeb.admin.export.exportuserscore" %>


<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>导入导出</title>
    <asp:Literal ID="litregscript" runat="server" Text=""></asp:Literal>
    <link href="../style/default/style.css" rel="stylesheet" type="text/css" />

    <script language="javascript" src="/admin/js/System.js" type="text/javascript"></script>
    
    <script language="javascript" type="text/javascript">
        function AutoStart() {
            var info;
            var bstate ;
            info = "您确实要导出数据吗？导出数据比较浪费系统资源，请尽量在服务器不忙的时候使用！";
            bstate = confirm(info);
            return bstate;
        }
</script>
    
    
    
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div id="divLoading">
                <img src="/admin/style/default/images/loading.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div id="tbMain">
        <div class="divTbg">
            <div class="divTbgL">
            </div>
            <div class="divTbgInfo">
                <div class="divTbgTitle">
                    人员成绩导出</div>
            </div>
        </div>
        <div class="divTbgF">
        </div>
        <div class="divTNavBG" id="topURL">
            <ul id="topURLul">
                <li class="divURLed" id="div1">
                    <div>
                        <img src="../style/default/images/mini_icons_046.gif" width="10" height="10" />
                        基本信息</div>
                </li>
            </ul>
        </div>
        <div id="mypanel">
            <div class="divInfoContext">
                <div class="divInfo">
                    <div class="divInfoblack">
                        <div class="divInfoTopList">
                            <div class="listLeft">
                                <div class="navtitle">
                                    人员成绩导出</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="divInfo">
                    <table width="100%" border="0" cellpadding="0" cellspacing="1" class="tablist" id="mainlist"
                        onmouseover="changeto()" onmouseout="changeback()">
                        <tr class="tablisttr">
                            <td class="tablisttitle">
                                时间段</td>
                            <td class="tabmanagertr">
                                <asp:DropDownList runat="server" ID="drpDateID" ToolTip="时间段"></asp:DropDownList></td>
                        </tr>
                        <tr class="tablisttr">
                            <td class="tablisttitle">
                                隶属部门类型</td>
                            <td class="tabmanagertr">
                                <asp:DropDownList runat="server" ID="drpDeptType" ToolTip="部门类型">
                                    <asp:ListItem Value="DeptType" Text="code"></asp:ListItem>
                                </asp:DropDownList></td>
                        </tr>
                        <tr class="tablisttr">
                            <td class="tablisttitle">
                                岗位类型</td>
                            <td class="tabmanagertr"><asp:DropDownList runat="server" ID="drpPostID" ToolTip="隶属岗位"></asp:DropDownList></td>
                        </tr>
                        <tbody runat="server" id="tbOutput">
                        </tbody>
                        <tbody runat="server" id="tbInput" visible="false">
                        </tbody>
                    </table>
                    <div class="divSave">
                        <asp:Button ID="btnSubmit1" runat="server" Text="提交(S)" CssClass="SkyButtonFocus"
                            OnClick="btnSubmit_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </form>
</body>
</html>
