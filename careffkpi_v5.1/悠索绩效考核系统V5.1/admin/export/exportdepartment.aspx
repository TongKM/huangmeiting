﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="exportdepartment.aspx.cs"
    Inherits="BasicWeb.admin.export.exportdepartment" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>导入导出</title>
    <asp:Literal ID="litregscript" runat="server" Text=""></asp:Literal>
    <link href="../style/default/style.css" rel="stylesheet" type="text/css" />

    <script language="javascript" src="/admin/js/System.js" type="text/javascript"></script>
    
    <script language="javascript" type="text/javascript">
        function AutoStart() {
            var info;
            var bstate ;

            if (document.getElementById("radOutput").checked) {
                info = "您确实要导出数据吗？导出数据比较浪费系统资源，请尽量在服务器不忙的时候使用！";
                bstate = confirm(info);
            }

            else {
                info = "您确实要开始导入信息吗？请注意在没有结束的时候，请不要关闭浏览器，否则系统将可能存在问题！";
                bstate = confirm(info);
                if (bstate) {
                    var txt1 = document.getElementById("btnSubmit1");
                    txt1.style.display = "none";
                }
            }
            return bstate;
        }

        function SetButtonState(state) {
            var btn = document.getElementById("btnSubmit1");
            if (state) {
                btn.style.display = "none";
            }
            else {
                btn.style.display = "";
            }
        }

</script>
    
    
    
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div id="divLoading">
                <img src="/admin/style/default/images/loading.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div id="tbMain">
        <div class="divTbg">
            <div class="divTbgL">
            </div>
            <div class="divTbgInfo">
                <div class="divTbgTitle">
                    部门导入导出</div>
            </div>
        </div>
        <div class="divTbgF">
        </div>
        <div class="divTNavBG" id="topURL">
            <ul id="topURLul">
                <li class="divURLed" id="div1">
                    <div>
                        <img src="../style/default/images/mini_icons_046.gif" width="10" height="10" />
                        基本信息</div>
                </li>
            </ul>
        </div>
        <div id="mypanel">
            <div class="divInfoContext">
                <div class="divInfo">
                    <div class="divInfoblack">
                        <div class="divInfoTopList">
                            <div class="listLeft">
                                <div class="navtitle">
                                    部门导入导出</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="divInfo">
                    <table width="100%" border="0" cellpadding="0" cellspacing="1" class="tablist" id="mainlist"
                        onmouseover="changeto()" onmouseout="changeback()">
                        <tr class="tablisttr">
                            <td class="tablisttitle">
                                导入导出
                            </td>
                            <td class="tabmanagertr">
                            <div class="divRadCheck">
                                <asp:RadioButton ID="radOutput" runat="server" Checked="True"  GroupName="1" 
                                    Text="导出数据" oncheckedchanged="radOutput_CheckedChanged" 
                                    AutoPostBack="True" />&nbsp;&nbsp;
                                      <asp:RadioButton ID="radInput" runat="server" GroupName="1" Text="导入数据" 
                                    oncheckedchanged="radInput_CheckedChanged" AutoPostBack="True" /></div>
                            </td>
                        </tr>
                        <tbody runat="server" id="tbOutput">
                        <tr class="tablisttr">
                            <td class="tablisttitle">
                                导出说明
                            </td>
                            <td class="tabmanagertr">
                                您可以通过本功能导出标准的Excel文件，方便在以后导入到系统使用；
                                <a href="/upload/systemplete/部门信息导出样例.xls" target="_blank">[导出样例]</a> 
                               <div class="divRadCheck" style="display:none">
                                <asp:RadioButton ID="radPDF" runat="server" GroupName="2" Text="PDF格式" Checked="True"/>&nbsp;&nbsp;
                                <asp:RadioButton ID="radRTF" runat="server" GroupName="2" Text="RTF格式"/>&nbsp;&nbsp;
                                <asp:RadioButton ID="radCVS" runat="server" GroupName="2" Text="CVS格式"/></div>
                            </td>
                        </tr>
                        </tbody>
                        <tbody runat="server" id="tbInput" visible="false">
                        <tr class="tablisttr">
                            <td class="tablisttitle">
                                选择文件
                            </td>
                            <td class="tabmanagertr">
                                <asp:TextBox runat="server" ID="txtPath" Width="500">/upload/systemplete/部门信息导入模板.xls</asp:TextBox>
                                <a href="#" onclick="SkyOpen('txtPath');">[选择]</a> 
                                <a href="/upload/systemplete/部门信息导入模板.xls" target="_blank">[模板]</a> 
                            </td>
                        </tr>
                        <tr class="tablisttr">
                            <td class="tablisttitle">
                                处理方式
                            </td>
                            <td class="tabmanagertr">
                                <div class="divRadCheck">
                                <asp:RadioButton ID="radUpdate" runat="server" GroupName="3" Text="更新" Checked="True"/>&nbsp;&nbsp;
                                <asp:RadioButton ID="radNoOpr" runat="server" GroupName="3" Text="跳过"/></div>
                            </td>
                        </tr>
                        <tr class="tablisttr">
                            <td class="tablisttitle">
                                导入提示
                            </td>
                            <td class="tabmanagertr">
                                <div style="height:300px; overflow:auto" id="logInfo"><iframe src="exportmainserver.aspx" id="OutputFrame" allowtransparency="true" frameborder="0"
                width="100%" height="100%" scrolling="auto" marginheight="0"></iframe></div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <div class="divSave">
                        <asp:Button ID="btnSubmit1" runat="server" Text="提交(S)" CssClass="SkyButtonFocus"
                            OnClick="btnSubmit_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </form>
</body>
</html>
