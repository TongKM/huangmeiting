﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ustop.aspx.cs" Inherits="BasicWeb.admin.ustop" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>顶部区域</title>
<link href="style/default/style.css" rel="stylesheet" type="text/css" />

</head>

<body>
<table class="wtable2014" width="100%" border="0" cellpadding="0" cellspacing="0" background="style/default/images/top_bg.jpg">
  <tr>
    <td  height="88" class="toplogo">
        <a href='<%=AppConfig.sSoftWebURL%>' target="_blank">
            <img src='<%=AppConfig.sSoftLogoURL%>' border="0"/></a>
    </td>
    <td >
    
    
    <table border="0" cellspacing="0" cellpadding="0" style="display:none;">
      <tr>
        <td><img src="style/default/images/topd1.jpg"/></td>
        <td><img src="style/default/images/topd2.jpg"/></td>
        <td><img src="style/default/images/topd3.jpg"/></td>
        <td><img src="style/default/images/topd4.jpg"/></td>
        <td><img src="style/default/images/topd5.jpg"/></td>
        <td><img src="style/default/images/topd6.jpg"/></td>
      </tr>
    </table>
    <%=getTopURLHTML()%>
    </td>
    <td width="250" class="topright"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td height="44">
        
      <table border="0" cellspacing="0" cellpadding="0">
  <tr>
  <td width="60" align="center">&nbsp;</td>
    <td width="20" align="center"><img src="style/default/images/top_help.jpg"/></td>
    <td align="left" nowrap="nowrap"><a href="#" class="topurllink">帮助</a></td>
    <td width="20" align="center"><img src="style/default/images/top_dot.jpg"/></td>
    <td align="center"  nowrap="nowrap"><a href="/admin/basic/usermanagerself.aspx" target="main" class="topurllink">用户信息</a></td>
    <td width="20" align="center"><img src="style/default/images/top_dot.jpg"/></td>
    <td align="left"  nowrap="nowrap"><a  href="usexit.aspx" onclick="return confirm('您确实要退出系统吗?');" target="main" class="topurllink">退出</a></td>
    
  </tr>
</table>

        </td>
      </tr>
      <tr>
        <td height="44" valign="top"><span class="topuser">欢迎您：<%=UserInfo.sUserName%></span></td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>
