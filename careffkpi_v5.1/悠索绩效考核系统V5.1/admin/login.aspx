﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="BasicWeb.admin.login" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><%=AppConfig.sSoftName +"-"+ AppConfig.sSoftCompanyName%></title>
<link href="style/login/images/login.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/admin/js/uslogin.js"></script>
</head>
<body>
<form action="login.aspx" onsubmit="return userlogin();" method="post">
  <div id="login">
    <div id="top">
      <div id="top_left"><img src="style/login/images/login_03.gif" /></div>
      <div id="top_center"></div>
    </div>
    <div id="center">
      <div id="center_left"></div>
      <div id="center_middle">
        <div id="user">
          <label for="userid">登录名称</label>
          <input type="text"  id="userid" name="userid" maxlength="20" class="inputt"/>
        </div>
        <div id="password">
          <label for="userpwd">登录密码</label>
          <input type="password"  id="userpwd" name="userpwd" maxlength="20"  class="inputt"/>
        </div>
        <div id="check">
          <label for="usercode">验证码　</label>
          <input type="text" class="inputy"  id="usercode" name="usercode" maxlength="4" />
          <img align="absMiddle" alt="看不清楚,可以点击更新" src="/admin/basic/syscode.aspx" id="imgcode" runat="server" onclick="this.src=this.src+'?'" /> </div>
        <div id="btn">
          <table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td align="center"><input type="submit"  id="btnsubmit" class="inputbtn" value="登录" />
              </td>
              <td align="center"><input type="reset"  id="btnreset" class="inputbtn" value="清空" /></td>
            </tr>
          </table>
        </div>
      </div>
      <div id="center_right"></div>
    </div>
    <div id="down">
      <div id="down_left">
        <div id="inf"> <span class="inf_text">版本信息</span> <span class="copyright"><%=AppConfig.sSoftCopyRight%></span> </div>
      </div>
      <div id="down_center"></div>
    </div>
      
  </div><asp:Literal ID="litscript" runat="server"></asp:Literal>
</form>
</body>
</html>
