﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="evaluationuserselflist.aspx.cs" Inherits="BasicWeb.admin.evaluation.evaluationuserselflist" %>



<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>自我评价</title>
    <link href="../style/default/style.css" rel="stylesheet" type="text/css" />

    <script language="javascript" src="/admin/js/System.js" type="text/javascript"></script>
    <script language="javascript" src="/admin/My97DatePicker/WdatePicker.js" type="text/jscript"></script>
</head>
<body>
    <form id="Form1" method="post" runat="server">
    <div class="divTbg">
      <div class="divTbgL"></div>
      <div class="divTbgInfo">
        <div class="divTbgTitle">自我评价</div>
      </div>
    </div>
    <div class="divTbgF"></div>
    <div class="divSearchBoth">
        <div class="divInfo">
            <div class="divInfoblack">
                <div class="divInfoTopList">
                    <div class="listLeft">
                        <div class="navsearchtitle">
                            自我评价时间段搜索</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="divInfo">
            <div class="divSearch">
                <div id="searchBox" class="clearsearch">
                    <p>
                        <label for="txtCodeID">
                            时间段编号：</label>
                        <asp:TextBox ID="txtDateID" runat="server" ToolTip="时间段编号" Text=""></asp:TextBox>
                    </p>
                    <p>
                        <label for="txtDateName">
                            时间段名称：</label>
                       <asp:TextBox ID="txtDateName" runat="server" ToolTip="时间段名称" Text=""></asp:TextBox>
                    </p>
                    
                    
                    
                    <p>
                        <label for="drpDState">
                            时间段状态：</label>
                        <asp:DropDownList runat="server" ID="drpDState" ToolTip="时间段状态">
                                    <asp:ListItem Value="CodeState" Text="code"></asp:ListItem>
                                </asp:DropDownList>
                   </p>
                   
                    <p>
                       <label for="txtBeginDate">开始时间：</label>
                        <asp:TextBox ID="txtBeginDate" runat="server" onfocus="var txtAContractEnd=$dp.$('txtEndDate');WdatePicker({onpicked:function(){txtEndDate.focus();},maxDate:'#F{$dp.$D(\'txtEndDate\')}',isShowClear:true,readOnly:true,dateFmt:'yyyy-MM-dd'})"></asp:TextBox>
                     </p>
                     
                     <p>
                      <label for="txtEndDate">结束时间：</label>
                       <asp:TextBox ID="txtEndDate" runat="server" onfocus="WdatePicker({minDate:'#F{$dp.$D(\'txtBeginDate\')}',isShowClear:true,readOnly:true,dateFmt:'yyyy-MM-dd'});"></asp:TextBox>
                      </p>
                    <p>
                    
                        <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="确认搜索" CssClass="SkyButtonFocus" />
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="divInfo">
        <div class="divInfoblack">
            <div class="divInfoTopList">
                <div class="listLeft">
                    <div class="navtitle">
                        自我评价时间段列表</div>
                </div>
                <div class="listRight">
                    <ul>
                        <li class="navstyle1">
                            <input type="checkbox" name="chkselect" id="chkselect" onclick="skySelectCheckList();"/>
                            <label for="chkselect">全选</label> </li>
                        <li class="navstyle2">
                            <asp:LinkButton ID="LnkBAdd" runat="server" OnClick="SkyLnkBAMD_Click" Enabled="false" >添加</asp:LinkButton>
                        </li>
                        <li class="navstyle3">
                            <asp:LinkButton ID="LnkBEdit" runat="server" OnClick="SkyLnkBAMD_Click" Enabled="false">编辑</asp:LinkButton>
                        </li>
                        <li class="navstyle4">
                            <asp:LinkButton ID="LnkBDelete" runat="server" OnClick="SkyLnkBAMD_Click" Enabled="false">删除</asp:LinkButton>
                        </li>
                        <li class="navstyle5">
                            <asp:LinkButton ID="LnkBExcel" runat="server" OnClick="SkyLnkBAMD_Click">导出</asp:LinkButton>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="divInfo">
        <div class="divInfoContext">
            <asp:Repeater runat="server" ID="rptInfo">
                <HeaderTemplate>
                    <table width="100%" border="0" cellpadding="0" cellspacing="1" class="tablist" onmouseover="changeto()"  onmouseout="changeback()">
                        <tr class="tablisttitle">
                           <td width="4%">
                                序号
                            </td>
                            <td width="4%">
                                <%=cHTMLFx%>
                            </td>
                            <td>
                                时间段编号
                            </td>
                            <td>
                                时间段名称
                            </td>
                            <td>
                                默认时间段
                            </td>
                           
                            <td>
                                评价时间
                            </td>
                            
                            <td>
                                自评时间
                            </td>
                            
                            <td>
                                计划时间
                            </td>
                            
                           
                            
                            <td>
                                操作
                            </td>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr class="tablisttr">
                        <td>
                            <%#Eval("ID") %>
                        </td>
                        <td>
                            <asp:CheckBox  runat="server" ID="chkselect" ToolTip='<%#Eval("ID") %>' Enabled='<%# Eval("DRunState").ToString() != "0"?false:true%>'/>
                        </td>
                        <td>
                            <%#Eval("DateID")%>
                        </td>
                        <td>
                            <%#Eval("DateName")%>
                        </td>
                        <td>
                            <%#Eval("DateMainName")%>
                        </td>
                       
                        <td>
                           <%#UsToolsDateFormatyyMMdd(Eval("DStart"))%>至<br/><%#UsToolsDateFormatyyMMdd(Eval("DEnd"))%>
                        </td>
                        <td>
                           <%#UsToolsDateFormatyyMMdd(Eval("DSelfStart"))%>至<br/><%#UsToolsDateFormatyyMMdd(Eval("DSelfEnd"))%>
                        </td>
                        <td>
                           <%#UsToolsDateFormatyyMMdd(Eval("DPlanStart"))%>至<br/><%#UsToolsDateFormatyyMMdd(Eval("DPlanEnd"))%>
                        </td>
                       
                        
                        <td>
                        
                        <asp:LinkButton runat="server" ID="lbtMain" Text='自我评价' CommandArgument='<%#Eval("DateID")%>' OnClick="EditState"></asp:LinkButton>
                        
                           
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table></FooterTemplate>
            </asp:Repeater>
        </div>
    </div>
    <div class="divInfo">
        <webdiyer:aspnetpager id="Pager" runat="server" AlwaysShow="True" OnPageChanged="Pager_PageChanged"
         ShowCustomInfoSection="Left" ShowPageIndexBox="Always" showpageindex="False"
               CustomInfoHTML="共 <B>%PageCount%</B> 页，当前为第 <B>%CurrentPageIndex%</B> 页，每页 <B>%PageSize%</B> 条" 
               SubmitButtonText="" FirstPageText="首页" LastPageText="尾页" 
               LayoutType="Table" NextPageText="后一页" PrevPageText="前一页" ShowMoreButtons="False" 
               TextAfterPageIndexBox="页" TextBeforePageIndexBox="转到" 
               CustomInfoTextAlign="Left" HorizontalAlign="Right"></webdiyer:aspnetpager>
    </div>
    </form>
</body>
</html>
