﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="evaluationuserselfhistorymanager.aspx.cs" Inherits="BasicWeb.admin.evaluation.evaluationuserselfhistorymanager" %>


<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>自我评价</title>
    <link href="../style/default/style.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery/jquery.js" type="text/javascript"></script>
    <script language="javascript" src="/admin/js/System.js" type="text/javascript"></script>
    <script language="javascript" src="/admin/My97DatePicker/WdatePicker.js" type="text/jscript"></script>
    <script  language="javascript" src="/editor/xheditor.js" type="text/javascript" ></script>

    
    
    
    <script language="javascript" type="text/javascript" >
        $(document).ready(function() {
            $("#btnSearch").click(function() {

            return window.confirm("您确实要提交对自己评价数据吗？");
            });
        });

      
    </script>
</head>
<body onload="win_onLoad();">
    <form id="Form1" method="post" runat="server"><div id="tbMain">

    <div class="divTbg">
      <div class="divTbgL"></div>
      <div class="divTbgInfo">
        <div class="divTbgTitle">自我评价</div>
      </div>
    </div>
    <div class="divTbgF"></div>
    <div class="divSearchBoth">
        <div class="divInfo">
            <div class="divInfoblack">
                <div class="divInfoTopList">
                    <div class="listLeft">
                        <div class="navsearchtitle">
                            自我评价详细说明</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="divInfo">
            <div class="divSearch1">
                <div id="searchBox1" class="clearsearch1">
                    <table width="100%" border="0" cellpadding="0" cellspacing="1" class="tablist" onMouseOver="changeto()"  onmouseout="changeback()">
    <tr class="tablisttr">
      <td align="center" class="tablisttitle"> 评估时间</td>
      <td align="center" class="tablisttr">暂未上传</td>
      <td align="center" class="tablisttitle">提交评估</td>
      <td align="center" class="tablisttr"> <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="确认评估" CssClass="SkyButtonFocus" /></td>
    </tr>
    <tr class="tablisttr">
      <td align="center" class="tablisttitle"> 文件上传</td>
      <td colspan="3" align="left">
      
      <asp:TextBox ID="txtEAddr" runat="server"  Width="350px"></asp:TextBox>
      
        <input type="button" value="文件上传"  class="SkyButtonFocus" onClick="UsOpenDialog('txtEAddr');" />
        <input type="button" value="查看文件"  class="SkyButtonFocus" onClick="UsView('txtEAddr');" />
        <input type="button" value="删除上传"  class="SkyButtonFocus"  onclick="UsClear('txtEAddr');"/>
        
</td>
    </tr>
    <tr class="tablisttr">
      <td align="center" class="tablisttitle"> 自评说明</td>
      <td colspan="3" align="center">
          <asp:TextBox ID="txtEInfo" runat="server" TextMode="MultiLine" Width="100%"  Height="80px"></asp:TextBox>
        </td>
    </tr>
    </table>
                   
              </div>
            </div>
        </div>
    </div>
    <div class="divInfo">
        <div class="divInfoblack">
            <div class="divInfoTopList">
                <div class="listLeft">
                    <div class="navtitle">
                        自我评价指标体系</div>
                </div>
                <div class="listRight">
                    <ul>
                        <li class="navstyle1">
                            <input type="checkbox" name="chkselect" id="chkselect" onclick="skySelectCheckList();"/>
                            <label for="chkselect">全选</label> </li>
                        <li class="navstyle2">
                            <asp:LinkButton ID="LnkBAdd" runat="server" OnClick="SkyLnkBAMD_Click"  Enabled="false">添加</asp:LinkButton>
                        </li>
                        <li class="navstyle3">
                            <asp:LinkButton ID="LnkBEdit" runat="server" OnClick="SkyLnkBAMD_Click"   Enabled="false">编辑</asp:LinkButton>
                        </li>
                        <li class="navstyle4">
                            <asp:LinkButton ID="LnkBDelete" runat="server" OnClick="SkyLnkBAMD_Click"  Enabled="false">删除</asp:LinkButton>
                        </li>
                        <li class="navstyle5">
                            <asp:LinkButton ID="LnkBExcel" runat="server" OnClick="SkyLnkBAMD_Click"  Enabled="false">导出</asp:LinkButton>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="divInfo">
        <div class="divInfoContext">
            <asp:Repeater runat="server" ID="rptInfo">
                <HeaderTemplate>
                    <table width="100%" border="0" cellpadding="0" cellspacing="1" class="tablist" onmouseover="changeto()"  onmouseout="changeback()">
                        <tr class="tablisttitle">
                           <td width="4%">
                                序号
                            </td>
                            
                            <td>
                                指标大类
                            </td>
                            <td>
                                指标小类
                            </td>
                           
                            <td>
                                指标名称
                            </td>
                            
                            
                             <td>
                                考评细则
                            </td>
                       
                            <td>
                                计划分值
                            </td>

                            <td>
                                完成分值
                            </td>
                            
                            <td>
                                完成情况
                            </td>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr class="tablisttr">
                        <td>
                           
                            <asp:Literal runat="server" ID="LiteralID" Text=' <%#Eval("ID1") %>'></asp:Literal>
                            <input type="hidden" id='txtIDType' runat="server" value='<%#Eval("IsDel")%>' />
                        </td>
                       
                         <td>
                            <%#Eval("BigIDName")%>
                        </td>
                        <td>
                            <%#Eval("SmallIDName")%>
                        </td>
                       
                        <td>
                            <%#UsToolsStringLengthTitle(Eval("EvalName").ToString(), 30)%>
                            <input type="hidden" id='txtName' runat="server" value='<%#Eval("EvalName")%>' />
                            
                        </td>
                       
                        <td>
                            <%#cHTMLUrlPJ("../advanced/evaluationdetailview.aspx?id=" + Eval("EvalID").ToString(), "考评细则")%>
                        </td>
                        
                        <td>
                            <%#KPISelfType(Eval("EIType"), Eval("EIMax"))%>
                        </td>
                       
                        <td>
                            <%#KPISelfTypeValue(Eval("EIType"), Eval("ID1"), Eval("Evalue1"))%>                       
                        </td>
                        
                        <td>
                            <asp:TextBox runat="server" ID="txtItemNote" CssClass="pjSelfTextBox" TextMode="MultiLine" Text=' <%#Eval("EInfo1")%>' onkeyup="this.value = this.value.substring(0, 2000)" 
                            title="系系统最多支持2000字符,超出将自动截断,因为有限制,在字符超过指定数量的时候,您将无法使用ctrl+a进行全选,这个时候,您可以使用点击ctrl键+鼠标左键文本框或者双击即可全选"></asp:TextBox>
                            
                           
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                
                    </table></FooterTemplate>
            </asp:Repeater>
        </div>
    </div>
    <div class="divInfo">
        <webdiyer:aspnetpager id="Pager" runat="server" AlwaysShow="True" OnPageChanged="Pager_PageChanged"
         ShowCustomInfoSection="Left" ShowPageIndexBox="Always" showpageindex="False"
               CustomInfoHTML="共 <B>%PageCount%</B> 页，当前为第 <B>%CurrentPageIndex%</B> 页，每页 <B>%PageSize%</B> 条" 
               SubmitButtonText="" FirstPageText="首页" LastPageText="尾页" 
               LayoutType="Table" NextPageText="后一页" PrevPageText="前一页" ShowMoreButtons="False" 
               TextAfterPageIndexBox="页" TextBeforePageIndexBox="转到" 
               CustomInfoTextAlign="Left" HorizontalAlign="Right"></webdiyer:aspnetpager>
    </div></div>
    </form>
</body>
</html>