﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="evaluationuserlist.aspx.cs" Inherits="BasicWeb.admin.evaluation.evaluationuserlist" %>



<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>人员互评</title>
    <link href="../style/default/style.css" rel="stylesheet" type="text/css" />

    <script language="javascript" src="/admin/js/System.js" type="text/javascript"></script>
    <script language="javascript" src="/admin/My97DatePicker/WdatePicker.js" type="text/jscript"></script>
</head>
<body>
    <form id="Form1" method="post" runat="server">
    <div class="divTbg">
      <div class="divTbgL"></div>
      <div class="divTbgInfo">
        <div class="divTbgTitle">人员互评</div>
      </div>
    </div>
    <div class="divTbgF"></div>
    <div class="divSearchBoth">
        <div class="divInfo">
            <div class="divInfoblack">
                <div class="divInfoTopList">
                    <div class="listLeft">
                        <div class="navsearchtitle">
                            人员互评</div>
                    </div>
                    <div class="listRight">
                    <div class="pjdatestrstyle">(本次可评价时间:<%=DateConfig.sDStart + " 至 " + DateConfig.sDEnd%>)</div>
                    
                    </div>
                </div>
            </div>
        </div>
        <div class="divInfo">
            <div class="divSearch">
                <div id="searchBox" class="clearsearch">
                    <p>
                        <label for="drpDateID">
                            时间段：</label>
                        <asp:DropDownList runat="server" ID="drpDateID" ToolTip="时间段" Enabled="false"></asp:DropDownList>
                    </p>
                    <p>
                        <label for="txtDeptID">
                            部门编号：</label>
                        <asp:TextBox ID="txtDeptID" runat="server" ToolTip="部门编号" Text=""></asp:TextBox>
                    </p>
                    <p>
                        <label for="txtDeptName">
                            部门名称：</label>
                        <asp:TextBox ID="txtDeptName" runat="server" ToolTip="部门名称" Text=""></asp:TextBox>
                    </p>
                     <p>
                        <label for="txtUserID">
                            用户编号：</label>
                        <asp:TextBox ID="txtUserID" runat="server" ToolTip="用户编号" Text=""></asp:TextBox>
                    </p>
                    <p>
                        <label for="txtUserName">
                            人员名称：</label>
                        <asp:TextBox ID="txtUserName" runat="server" ToolTip="人员名称" Text=""></asp:TextBox>
                    </p>
                    <p>
                        <label for="txtUserPost">
                            用户岗位：</label>
                        <asp:TextBox ID="txtUserPost" runat="server" ToolTip="用户岗位" Text=""></asp:TextBox>
                    </p>
                     <p>
                        <label for="drpDIsPJ">
                            评价状态：</label>
                        <asp:DropDownList runat="server" ID="drpDIsPJ" ToolTip="评价状态">
                                    <asp:ListItem Value="CodeYesNo" Text="code"></asp:ListItem>
                                </asp:DropDownList>
                    </p>
                    <div style="display:none;">
                    <p>
                       <label for="txtBeginDate">开始时间：</label>
                        <asp:TextBox ID="txtBeginDate" runat="server" onfocus="var txtAContractEnd=$dp.$('txtEndDate');WdatePicker({onpicked:function(){txtEndDate.focus();},maxDate:'#F{$dp.$D(\'txtEndDate\')}',isShowClear:true,readOnly:true,dateFmt:'yyyy-MM-dd'})"></asp:TextBox>
                     </p>
                     
                     <p>
                      <label for="txtEndDate">结束时间：</label>
                       <asp:TextBox ID="txtEndDate" runat="server" onfocus="WdatePicker({minDate:'#F{$dp.$D(\'txtBeginDate\')}',isShowClear:true,readOnly:true,dateFmt:'yyyy-MM-dd'});"></asp:TextBox>
                      </p>
                      </div>
                    <p>
                    
                        <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="确认搜索" CssClass="SkyButtonFocus" />
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="divInfo">
        <div class="divInfoblack">
            <div class="divInfoTopList">
                <div class="listLeft">
                    <div class="navtitle">
                        人员详细列表</div>
                </div>
                <div class="listRight">
                    <ul>
                        <li class="navstyle1">
                            <input type="checkbox" name="chkselect" id="chkselect" onclick="skySelectCheckList();"/>
                            <label for="chkselect">全选</label> </li>
                        
                        <li class="navstyle6">
                            <asp:LinkButton ID="LnkBEvaluation" runat="server" OnClick="SkyLnkBAMD_Click">进入评价</asp:LinkButton>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="divInfo">
        <div class="divInfoContext">
            <asp:Repeater runat="server" ID="rptInfo">
                <HeaderTemplate>
                    <table width="100%" border="0" cellpadding="0" cellspacing="1" class="tablist" onmouseover="changeto()"  onmouseout="changeback()">
                        <tr class="tablisttr">
                        <td>
                        <div class="showallwidth">
                </HeaderTemplate>
                <ItemTemplate>
                    
                           
                           <div  class="showuserone">
                           
                           <table border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td><table border="0" cellpadding="0" cellspacing="0" class="showuserbg">
                                    <tr>
                                
                                      <td><img src='<%# Eval("UserPic").ToString() %>' class='showuser'title='<%# Eval("DeptName").ToString() %>-<%# Eval("PostName").ToString() %>-<%#Eval("UserName")%>'
                                      onerror="javascript:this.src='/upload/user/no.jpg'"/></td>
                                    </tr>
                                  </table></td>
                              </tr>
                              <tr>
                                <td align="center" class="tablisttr" style="height:30px; line-height:30px;">
                                    <asp:CheckBox  runat="server" ID="chkselect" ToolTip='<%#Eval("UserID") %>' Text='<%#Eval("UserName")%>'
                                    Enabled='<%# Eval("ACount").ToString() != "0"?false:true%>'
                                    />
                                </td>
                              </tr>
                            </table>

                           </div>
                           
                           
                      
                </ItemTemplate>
                <FooterTemplate>
                </div>
                  </td>
                        
                    </tr>
                    </table></FooterTemplate>
            </asp:Repeater>
        </div>
    </div>
    <div class="divInfo">
        <webdiyer:aspnetpager id="Pager" runat="server" AlwaysShow="True" OnPageChanged="Pager_PageChanged"
         ShowCustomInfoSection="Left" ShowPageIndexBox="Always" showpageindex="False"
               CustomInfoHTML="共 <B>%PageCount%</B> 页，当前为第 <B>%CurrentPageIndex%</B> 页，每页 <B>%PageSize%</B> 条" 
               SubmitButtonText="" FirstPageText="首页" LastPageText="尾页" 
               LayoutType="Table" NextPageText="后一页" PrevPageText="前一页" ShowMoreButtons="False" 
               TextAfterPageIndexBox="页" TextBeforePageIndexBox="转到" 
               CustomInfoTextAlign="Left" HorizontalAlign="Right"></webdiyer:aspnetpager>
    </div>
    </form>
</body>
</html>