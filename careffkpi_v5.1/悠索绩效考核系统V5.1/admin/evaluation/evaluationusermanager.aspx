﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="evaluationusermanager.aspx.cs" Inherits="BasicWeb.admin.evaluation.evaluationusermanager" %>


<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>人员互评</title>
    <link href="../style/default/style.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery/jquery.js" type="text/javascript"></script>
    <script language="javascript" src="/admin/js/System.js" type="text/javascript"></script>
    <script language="javascript" src="/admin/My97DatePicker/WdatePicker.js" type="text/jscript"></script>
    <script  language="javascript" src="/editor/xheditor.js" type="text/javascript" ></script>

    
    
    
    <script language="javascript" type="text/javascript" >
        $(document).ready(function() {
            $("[name^='Careff_']").click(function() {
                //alert($(this).attr("id"));
                getAllDateCount();
            });
            $("[name^='Careff_']").blur(function() {
                //alert($(this).attr("id"));
                getAllDateCount();
            });
            getAllDateCount();
            $("#btnSearch").click(function() {
            var UserID = $("#userid a").html();
            var aSum = $("#divsum").html();
                return window.confirm("您确实要提交对[" + UserID + "]的评价数据吗？\n当前您的评价总分为["+aSum+"]分！");
            });
        });

        function getAllDateCount() {
            var Ctype="";
            var aSum = 0;
            var aOne = 0;
            $("[name^='Careff_']").each(function(index, domEle) {
                Ctype = $(domEle).attr("type");
                //alert(Ctype + " " + $(domEle).attr("id")+" " + $(domEle).val());
                if (Ctype == "radio") {
                    if ($(domEle).attr("checked") == true) {
                        aOne = toFloat($(domEle).val());
                        aSum = aSum + aOne;
                    }
                }
                else {
                    aOne = toFloat($(domEle).val());
                    if (aOne == 0) {
                        $(domEle).val(0);
                    }
 

                    //判断超过了最大值吗
                    if ("text" == Ctype) {
                        var aOneMax = toFloat($(domEle).attr("title"));
                        if (aOneMax < aOne) {
                            aOne = aOneMax
                            $(domEle).val(aOne);
                            alert("本项最大可输入数为[" + aOneMax + "]");
                        }
                    }


                    aSum = aSum + aOne;
                }
            });
            //alert(aSum);
            $("#divsum").html(aSum.toFixed(2));
        }
        function toFloat(src) {
            var ret = parseFloat(src);
            if (isNaN(ret)) {
                return 0;
            }
            return ret;
        } 
    </script>
</head>
<body>
    <form id="Form1" method="post" runat="server">
    <div class="divTbg">
      <div class="divTbgL"></div>
      <div class="divTbgInfo">
        <div class="divTbgTitle">人员互评</div>
      </div>
    </div>
    <div class="divTbgF"></div>
    <div class="divSearchBoth">
        <div class="divInfo">
            <div class="divInfoblack">
                <div class="divInfoTopList">
                    <div class="listLeft">
                        <div class="navsearchtitle">
                            人员互评详细说明</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="divInfo">
            <div class="divSearch1">
                <div id="searchBox1" class="clearsearch1">
                    <table width="100%" border="0" cellpadding="0" cellspacing="1" class="tablist" onMouseOver="changeto()"  onmouseout="changeback()">
    <tr class="tablisttitle">
      <td width="100" align="center" class="tablisttitle"> 选择数量 </td>
      <td width="40%" align="center" class="tablisttr"> 共[<%=iAllCount.ToString()%>]个</td>
      <td width="100" align="center" class="tablisttitle">  当前评估</td>
      <td align="center" class="tablisttr">  第[<%=(iCurrentNumber+1).ToString()%>]个</td>
    </tr>
    <tr class="tablisttr">
      <td align="center" class="tablisttitle"> 当前评估</td>
      <td align="center" class="tablisttr" id="userid">[<%=sCurrentUserName.ToString()%>]</td>
      <td align="center" class="tablisttitle">下个评估</td>
      <td align="center" class="tablisttr">[<%=sNextUserName.ToString()%>]</td>
    </tr>
    <tr class="tablisttr">
      <td align="center" class="tablisttitle">文件上传</td>
      <td align="center" class="tablisttr"><%=sShow %></td>
      <td align="center" class="tablisttitle">提交评估</td>
      <td align="center" class="tablisttr"><asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="确认评估" CssClass="SkyButtonFocus" /></td>
    </tr>
    <tr class="tablisttr">
      <td align="center" class="tablisttitle">自评说明</td>
      <td align="center">
          <asp:TextBox ID="txtInfo1" runat="server" TextMode="MultiLine" Width="100%"  
              Height="80px" Enabled="False"></asp:TextBox>
        </td>
         <td align="center" class="tablisttitle">评价留言</td>
          <td align="center">
          <asp:TextBox ID="txtInfo" runat="server" TextMode="MultiLine" Width="100%"  Height="80px"></asp:TextBox>
        </td>
    </tr>
    <tr class="tablisttr">
      <td align="center" class="tablisttitle"> 考评提示</td>
      <td colspan="3" align="center"><%=sUserPJInfo%></td>
    </tr>
    <tr class="tablisttr">
      <td align="center" class="tablisttitle">完成情况</td>
      <td colspan="3" align="center"><span class="usHelpMessage">[<%=sPlan.ToString()%>]</span></td>
    </tr>
  </table>
                   
              </div>
            </div>
        </div>
    </div>
    <div class="divInfo">
        <div class="divInfoblack">
            <div class="divInfoTopList">
                <div class="listLeft">
                    <div class="navtitle">
                        人员互评指标体系</div>
                </div>
                <div class="listRight">
                    <ul>
                        <li class="navstyle1">
                            <input type="checkbox" name="chkselect" id="chkselect" onclick="skySelectCheckList();"/>
                            <label for="chkselect">全选</label> </li>
                        <li class="navstyle2">
                            <asp:LinkButton ID="LnkBAdd" runat="server" OnClick="SkyLnkBAMD_Click"  Enabled="false">添加</asp:LinkButton>
                        </li>
                        <li class="navstyle3">
                            <asp:LinkButton ID="LnkBEdit" runat="server" OnClick="SkyLnkBAMD_Click"   Enabled="false">编辑</asp:LinkButton>
                        </li>
                        <li class="navstyle4">
                            <asp:LinkButton ID="LnkBDelete" runat="server" OnClick="SkyLnkBAMD_Click"  Enabled="false">删除</asp:LinkButton>
                        </li>
                        <li class="navstyle5">
                            <asp:LinkButton ID="LnkBExcel" runat="server" OnClick="SkyLnkBAMD_Click"  Enabled="false">导出</asp:LinkButton>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="divInfo">
        <div class="divInfoContext">
            <asp:Repeater runat="server" ID="rptInfo">
                <HeaderTemplate>
                    <table width="100%" border="0" cellpadding="0" cellspacing="1" class="tablist" onmouseover="changeto()"  onmouseout="changeback()">
                        <tr class="tablisttitle">
                           <td width="4%">
                                序号
                            </td>
                            
                            <td>
                                指标大类
                            </td>
                            <td>
                                指标小类
                            </td>
                           
                            <td>
                                指标名称
                            </td>
                            
                            
                             <td>
                                考评细则
                            </td>
                       
                            <td>
                                最大分值
                            </td>

                            <td>
                                单项分
                            </td>
                            
                            <td>
                                备注说明
                            </td>
                           <%--  <td>
                                实现状态
                            </td>
                             <td>
                                完成情况
                            </td>--%>
                            
                            
                            
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr class="tablisttr">
                        <td>
                           
                            <asp:Literal runat="server" ID="LiteralID" Text=' <%#Eval("EvalID") %>'></asp:Literal>
                        </td>
                       
                         <td>
                            <%#Eval("BigIDName")%>
                        </td>
                        <td>
                            <%#Eval("SmallIDName")%>
                        </td>
                       
                        <td>
                            <%#UsToolsStringLengthTitle(Eval("EvalName").ToString(), 30)%>
                            <input type="hidden" id='txtName' runat="server" value='<%#Eval("EvalName")%>' />
                            
                        </td>
                       
                        <td>
                            <%#cHTMLUrlPJ("../advanced/evaluationdetailview.aspx?id=" + Eval("EvalID").ToString(), "考评细则")%>
                        </td>
                        
                        <td>
                            <%#Eval("EMaxValue")%>
                        </td>
                       
                        <td>
                           <%#KPIShowDetail(Eval("EvalID"), Eval("EType"), Eval("EValue"), Eval("EMaxValue"))%>
                        </td>
                        
                        <td>
                            <asp:TextBox runat="server" ID="txtItemNote" TextMode="MultiLine" Text="无" onkeyup="this.value = this.value.substring(0, 2000)" 
                            title="系系统最多支持2000字符,超出将自动截断,因为有限制,在字符超过指定数量的时候,您将无法使用ctrl+a进行全选,这个时候,您可以使用点击ctrl键+鼠标左键文本框或者双击即可全选"></asp:TextBox>
                        </td>
                       <%--  <td>
                            <%# getLinePercent(Eval("EIType"), Eval("EIMax"), Eval("Evalue1"))%>
                        </td>
                         <td>
                          <%# KPISelfInfo(Eval("EIType"), Eval("ID1"), Eval("EInfo1"))%>
                        </td>--%>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                 <tr class="tablisttitle">
                           <td width="4%">
                                
                            </td>
                            
                            <td>
                            </td>
                            <td>
                                
                            </td>
                           
                            <td>
                                
                            </td>
                            
                            
                             <td>
                                
                            </td>
                       
                            <td>
                                <% =siEMaxValue.ToString()%>
                            </td>
                                
                            <td>
                                <div id="divsum">0</div>
                            </td>
                            
                            <td>
                                
                            </td>
                             <td>
                                
                            </td>
                             <td>
                                
                            </td>
                        </tr>
                    </table></FooterTemplate>
            </asp:Repeater>
        </div>
    </div>
    <div class="divInfo">
        <webdiyer:aspnetpager id="Pager" runat="server" AlwaysShow="True" OnPageChanged="Pager_PageChanged"
         ShowCustomInfoSection="Left" ShowPageIndexBox="Always" showpageindex="False"
               CustomInfoHTML="共 <B>%PageCount%</B> 页，当前为第 <B>%CurrentPageIndex%</B> 页，每页 <B>%PageSize%</B> 条" 
               SubmitButtonText="" FirstPageText="首页" LastPageText="尾页" 
               LayoutType="Table" NextPageText="后一页" PrevPageText="前一页" ShowMoreButtons="False" 
               TextAfterPageIndexBox="页" TextBeforePageIndexBox="转到" 
               CustomInfoTextAlign="Left" HorizontalAlign="Right"></webdiyer:aspnetpager>
    </div>
    </form>
</body>
</html>