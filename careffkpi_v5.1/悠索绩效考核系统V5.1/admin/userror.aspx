﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="userror.aspx.cs" Inherits="BasicWeb.admin.userror" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>系统错误</title>
    <asp:Literal ID="litregscript" runat="server" Text=""></asp:Literal>
    <link href="style/default/style.css" rel="stylesheet" type="text/css" />

    <script language="javascript" src="/admin/js/System.js" type="text/javascript"></script>

</head>
<body>
    <form id="form1" runat="server">
    
    <div id="tbMain">
        <div class="divTbg">
            <div class="divTbgL">
            </div>
            <div class="divTbgInfo">
                <div class="divTbgTitle">
                    系统错误</div>
            </div>
        </div>
        <div class="divTbgF">
        </div>
        <div class="divTNavBG" id="topURL">
            <ul id="topURLul">
                <li class="divURLed" id="div1">
                    <div>
                        <img src="style/default/images/mini_icons_046.gif" width="10" height="10" />
                        系统错误</div>
                </li>
            </ul>
        </div>
        <div id="mypanel">
            <div class="divInfoContext">
                
                <div class="divInfo">
                   <table width="100%" border="0" cellpadding="0" cellspacing="1" class="tablist" id="mainlist" onMouseOver="changeto()" onmouseout="changeback()">
<tr class="tablisttr">
<td class="tablisttitle">系统错误</td><td class="tabmanagertr"><span style="color:Red"><asp:Literal ID="LiteralInfo" runat="server"></asp:Literal></span></td>
</tr>
</table>
                   
                </div>
            </div>
        </div>
    </div>

    </form>
</body>
</html>
