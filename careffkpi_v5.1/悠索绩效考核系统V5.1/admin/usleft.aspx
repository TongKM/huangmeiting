<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="usleft.aspx.cs" Inherits="BasicWeb.admin.usleft" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
    <title>侧部导航区</title>
    
    <link href="style/default/style.css" rel="stylesheet" type="text/css" />
<link rel="StyleSheet" href="/admin/js/tree/dtree.css" type="text/css" />
<script type="text/javascript" src="/admin/js/tree/dtree.js"></script>

 <script src="../admin/js/jquery/jquery.js" type="text/javascript"></script>

 <script language="javascript" type="text/javascript" >
     function AutoHeight() {
         var he = document.body.clientHeight - 45;
         var s = document.getElementById("ScroLeft");
         s.style.height = he;
		 f1();
     }
     $(window).resize(function() {
         AutoHeight();
     });

     $(document).ready(function() {
         AutoHeight();
     });
</script>   
  
<script>
var Scrolling=false;
function $ID(o){return document.getElementById(o)}
function ScroMove(){Scrolling=true}
document.onmousemove=function(e){if(Scrolling==false)return;ScroNow(e)}
document.onmouseup=function(e){Scrolling=false}
function ScroNow(event){
 var event=event?event:(window.event?window.event:null);
 var Y=event.clientY-$ID("Scroll").getBoundingClientRect().top-$ID("ScroLine").clientHeight/2;
 var H=$ID("ScroRight").clientHeight-$ID("ScroLine").clientHeight;
 var SH=Y/H*($ID("ScroLeft").scrollHeight-$ID("ScroLeft").clientHeight);
 if (Y<0)Y=0;if (Y>H)Y=H;
 $ID("ScroLine").style.top=Y+"px";
 $ID("ScroLeft").scrollTop=SH;
 }
function ScrollWheel(){
 var Y=$ID("ScroLeft").scrollTop;
 var H=$ID("ScroLeft").scrollHeight-$ID("ScroLeft").clientHeight;
 if (event.wheelDelta >=120){Y=Y-80}else{Y=Y+80}
 if(Y<0)Y=0;if(Y>H)Y=H;
 $ID("ScroLeft").scrollTop=Y;
 var SH=Y/H*$ID("ScroRight").clientHeight-$ID("ScroLine").clientHeight;
 if(SH<0)SH=0;
 $ID("ScroLine").style.top=SH+"px";
}
</script>
    <link href="style/green/skin.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        BODY
        {
            margin: 0px;overflow-x:hidden; overflow-y:hidden;
            font-family: Arial, Helvetica, sans-serif,宋体;
            font-size: 12px;
            line-height: 25px;
        }
        .STYLE1
        {
            font-size: 12px;
            color: #ffffff;
        }
        .STYLE3
        {
            font-size: 12px;
            color: #033d61;
        }
    </style>
    <style type="text/css">
        .menu_title SPAN
        {
            font-weight: bold;
            left: 3px;
            color: #ffffff;
            position: relative;
            top: 2px;
        }
        .menu_title2 SPAN
        {
            font-weight: bold;
            left: 3px;
            color: #ffcc00;
            position: relative;
            top: 2px;
        }
    </style>
    <style>
/*主窗*/
#Scroll {
	width:165px;
	height:400px;
	
}
/*左边内容区*/
#ScroLeft {
	float:left;
	height:400px;
	width:145px;
	overflow:hidden; padding-top:5px;
}
/*滚动条背景*/
#ScroRight {
	position:relative;
	float:right;
	height:100%;
	width:4px;
	background:#D5EEFB;
	background-image: url(style/default/images/scollbg.gif);
	overflow:hidden;
}
/*滚动条*/
#ScroLine {
	position:absolute;
	z-index:1;
	top:0px;
	left:0px;
	width:100%;
	height:47px;
	overflow:hidden;
	background-image: url(style/default/images/scoll.gif);
	background-repeat: no-repeat;
	background-position: center center;
}
</style>
</head>
<body>
    <table width="166" height="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td align="center" valign="top" bgcolor="#F0F9FD"><table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
    <td class="left1"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="40"><span class="leftgndh">功能导航</span></td>
  </tr>
</table>
</td>
  </tr>
                        <td valign="top" style="padding-left:5px;">
                            <div id="Scroll" onselectstart="return false" onmousewheel="ScrollWheel()">
                                <div id="ScroLeft">
                                    <%=TreeHTML %>
                                </div>
                                <div id="ScroRight" onClick="ScroNow(event)">
                                    <div id="ScroLine" onMouseDown="ScroMove()" title="滚动条,可以上下拖动">
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
    <td height="0" background="style/default/images/main_69.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0" style=" display:none">
        <tr>
          <td width="24%">&nbsp;</td>
          <td width="76%" valign="bottom"><span class="TopStyle4">版本：<%=sVersion%></span> </td>
        </tr>
      </table></td>
  </tr>
            </table></td>
      </tr>
    </table>

    <script>
        function showsubmenu(sid)
        {
        whichEl = eval("submenu" + sid);
        imgmenu = eval("imgmenu" + sid);
        if (whichEl.style.display == "none")
        {
        eval("submenu" + sid + ".style.display=\"\";");
        imgmenu.background="style/default/main_47.gif";
        }
        else
        {
        eval("submenu" + sid + ".style.display=\"none\";");
        imgmenu.background="style/default/main_48.gif";
        }
        }
		
		//document.getElementById("divw").style.height=he;

    </script>

    <script>
		function f1()
		{
        var he=document.body.clientHeight-40;
        var s=document.getElementById("ScroLeft");
        s.style.height=he;
        var s1=document.getElementById("Scroll");
        s1.style.height=he;
        //document.write("<div id=tt style=height:"+he+";overflow:hidden>")
		}
		f1();
    </script>

</body>
</html>